USE [master]
GO

/****** Object:  Database [IN.2014.LY.PTIT_G5_DB]    Script Date: 08/10/2014 16:36:31 ******/
IF  EXISTS (SELECT name FROM sys.databases WHERE name = N'IN.2014.LY.PTIT_G5_DB')
DROP DATABASE [IN.2014.LY.PTIT_G5_DB]
GO




CREATE DATABASE [IN.2014.LY.PTIT_G5_DB]
GO

USE [IN.2014.LY.PTIT_G5_DB]
GO

CREATE TABLE [User] (
	UserID int identity(1,1),
	Account nvarchar(255),
	Email nvarchar(255),
	Role int,
	Password nvarchar(255),
	PRIMARY KEY (UserID)
)
GO

CREATE TABLE Contact (
	ContactID int identity(1,1),
	FirstName nvarchar(255),
	ManagerID int,
	PRIMARY KEY (ContactID),
	FOREIGN KEY (ManagerID) REFERENCES Contact(ContactID)
)
GO

CREATE TABLE Organisation (
	OrgID int identity(1,1),
	OrgName nvarchar(255),
	ContactID int,
	PRIMARY KEY (OrgID),
	FOREIGN KEY (ContactID) REFERENCES Contact(ContactID)
)
GO

CREATE TABLE Programme (
	ProgrammeID int identity(1,1),
	ContactID int,
	PRIMARY KEY (ProgrammeID),
	FOREIGN KEY (ContactID) REFERENCES Contact(ContactID)
)
GO 

CREATE TABLE OrganisationProgramme (
	OrganisationProgrammeID int identity(1,1),
	OrgID int,
	ProgrammeID int,
	PRIMARY KEY (OrganisationProgrammeID),
	FOREIGN KEY (OrgID) REFERENCES Organisation(OrgID),
	FOREIGN KEY (ProgrammeID) REFERENCES Programme(ProgrammeID)
)
GO

CREATE TABLE Service (
	ServiceID int identity(1,1),
	ContactID int,
	PRIMARY KEY (ServiceID),
	FOREIGN KEY (ContactID) REFERENCES Contact(ContactID)
)
GO

CREATE TABLE OrganisationService (
	OrganisationServiceID int identity(1,1),
	OrgID int,
	ServiceID int,
	PRIMARY KEY (OrganisationServiceID),
	FOREIGN KEY (OrgID) REFERENCES Organisation(OrgID),
	FOREIGN KEY (ServiceID) REFERENCES Service(ServiceID)
)
GO

CREATE TABLE Premise (
	PremiseID int identity(1,1),
	ServiceID int,
	PRIMARY KEY (PremiseID),
	FOREIGN KEY (ServiceID) REFERENCES Service(ServiceID)
)
GO

CREATE TABLE Directorate (
	DirectorateID int identity(1,1),
	OrgID int,
	ContactID int,
	PRIMARY KEY (DirectorateID),
	FOREIGN KEY (OrgID) REFERENCES Organisation(OrgID),
	FOREIGN KEY (ContactID) REFERENCES Contact(ContactID)
)
GO

CREATE TABLE Department (
	DepartmentID int identity(1,1),
	DirectorateID int,
	ContactID int,
	PRIMARY KEY (DepartmentID),
	FOREIGN KEY (DirectorateID) REFERENCES Directorate(DirectorateID),
	FOREIGN KEY (ContactID) REFERENCES Contact(ContactID)
)
GO

CREATE TABLE Team (
	TeamID int identity(1,1),
	DepartmentID int,
	ContactID int,
	PRIMARY KEY (TeamID),
	FOREIGN KEY (DepartmentID) REFERENCES Department(DepartmentID),
	FOREIGN KEY (ContactID) REFERENCES Contact(ContactID)
)
GO
CREATE TABLE SupportingMaterial (
	SupportingMaterialID int identity(1,1),
	UserID int,
	OrgID int,
	PRIMARY KEY (SupportingMaterialID),
	FOREIGN KEY (OrgID) REFERENCES Organisation(OrgID),
	FOREIGN KEY (UserID) REFERENCES [User](UserID),
)
GO

CREATE TABLE Reference (
	RefID int identity(1,1),
	RefCode nvarchar(255),
	RefValue nvarchar(255),
	PRIMARY KEY (RefID)
)
GO

CREATE TABLE Country (
	CountryID int identity(1,1),
	CountryName nvarchar(255),
	PRIMARY KEY (CountryID)
)
GO

CREATE TABLE County (
	CountyID int identity(1,1),
	CountryID int,
	CountyName nvarchar(255),
	PRIMARY KEY (CountyID),
	FOREIGN KEY (CountryID) REFERENCES Country(CountryID)
)
GO

CREATE TABLE GovOfficeRegion (
	GovOfficeRegionID int identity(1,1),
	CountyID int,
	Name nvarchar(255),
	PRIMARY KEY (GovOfficeRegionID),
	FOREIGN KEY (CountyID) REFERENCES County(CountyID)
)
GO

CREATE TABLE Town (
	TownID int identity(1,1),
	CountyID int,
	PRIMARY KEY (TownID),
	FOREIGN KEY (CountyID) REFERENCES County(CountyID)
)
GO

CREATE TABLE Address (
	AddressID int identity(1,1),
	TownID int,
	PostCode int,
	Street nvarchar(500),
	CountryID int,
	CountyID int,
	PRIMARY KEY (AddressID),
	FOREIGN KEY (TownID) REFERENCES Town(TownID),
	FOREIGN KEY (CountryID) REFERENCES Country(CountryID),
	FOREIGN KEY (CountyID) REFERENCES County(CountyID)

)
GO

CREATE TABLE TrustRegion (
	TrustRegionID int identity(1,1),
	Name nvarchar(255),
	Description text,
	CountryID int,
	IsActive int,
	PRIMARY KEY (TrustRegionID),
	FOREIGN KEY (CountryID) REFERENCES Country(CountryID)
)
GO

CREATE TABLE TrustDistrict (
	TrustDistrictID int identity(1,1),
	TrustRegionID int,
	Name nvarchar(255),
	Description text,
	PRIMARY KEY (TrustDistrictID),
	FOREIGN KEY (TrustRegionID) REFERENCES TrustRegion(TrustRegionID)
)
GO

CREATE TABLE BusinessType (
	BusinessTypeID int identity(1,1),
	SIC int,
	PRIMARY KEY (BusinessTypeID)
)
GO

/*insert column GovOfficeRegion*/
ALTER TABLE GovOfficeRegion
ADD Desctiption text
GO

ALTER TABLE GovOfficeRegion
ADD IsActive int
GO
/*Town column insert*/
ALTER TABLE dbo.Town
ADD TownName nvarchar(255)
GO
/*Contact column insert*/
ALTER TABLE dbo.Contact
ADD SurName nvarchar(255)
GO

ALTER TABLE dbo.Contact
ADD KnownAs nvarchar(255)
GO

ALTER TABLE dbo.Contact
ADD OfficePhone nvarchar(25)
GO

ALTER TABLE dbo.Contact
ADD MobilePhone nvarchar(255)
GO

ALTER TABLE dbo.Contact
ADD STHomePhone nvarchar(255)
GO

ALTER TABLE dbo.Contact
ADD Email nvarchar(255)
GO

ALTER TABLE dbo.Contact
ADD JobRole nvarchar(255)
GO

ALTER TABLE dbo.Contact
ADD WorkBase nvarchar(255)
GO

ALTER TABLE dbo.Contact
ADD JobTitle nvarchar(255)
GO

ALTER TABLE dbo.Contact
ADD IsActive int
GO
/*create 2 depended table for contact table*/
CREATE TABLE ContactType (
	ContactTypeID int identity(1,1),
	Value nvarchar(255),
	PRIMARY KEY (ContactTypeID)
)
GO

IF (SELECT COUNT(*) FROM dbo.ContactType) = 0
BEGIN
	INSERT INTO dbo.ContactType(Value)
	VALUES ('Contact type 1')
	INSERT INTO dbo.ContactType(Value)
	VALUES ('Contact type 2')
	INSERT INTO dbo.ContactType(Value)
	VALUES ('Contact type 3')
	INSERT INTO dbo.ContactType(Value)
	VALUES ('Contact type 4')	
END
GO

CREATE TABLE BestContactMethod (
	BestContactMethodID int identity(1,1),
	Value nvarchar(255),
	PRIMARY KEY (BestContactMethodID)
)
GO

IF (SELECT COUNT(*) FROM dbo.BestContactMethod) = 0
BEGIN
	INSERT INTO dbo.BestContactMethod(Value)
	VALUES ('BestContactMethod  1')
	INSERT INTO dbo.BestContactMethod(Value)
	VALUES ('BestContactMethod 2')
	INSERT INTO dbo.BestContactMethod(Value)
	VALUES ('BestContactMethod 3')
	INSERT INTO dbo.BestContactMethod(Value)
	VALUES ('BestContactMethod 4')	
END
GO

ALTER TABLE dbo.Contact
ADD ContactTypeID int
GO

ALTER TABLE dbo.Contact
ADD CONSTRAINT fk_Contact_ContactType
FOREIGN KEY (ContactTypeID)
REFERENCES ContactType(ContactTypeID)
GO

ALTER TABLE dbo.Contact
ADD BestContactMethodID int
GO

ALTER TABLE dbo.Contact
ADD CONSTRAINT fk_Contact_BestContactMethod
FOREIGN KEY (BestContactMethodID)
REFERENCES BestContactMethod(BestContactMethodID)
GO
/*insert column Organisation*/

ALTER TABLE dbo.Organisation
ADD ShortDescription nvarchar(255)
GO

ALTER TABLE dbo.Organisation
ADD FullDescription nvarchar(255)
GO
ALTER TABLE dbo.Organisation
ADD AddressLine1 nvarchar(255)
GO

ALTER TABLE dbo.Organisation
ADD AddressLine2 nvarchar(255)
GO

ALTER TABLE dbo.Organisation
ADD AddressLine3 nvarchar(255)
GO

ALTER TABLE dbo.Organisation
ADD CountryName nvarchar(255)
GO

ALTER TABLE dbo.Organisation
ADD BusinessTypeID int
GO

ALTER TABLE dbo.Organisation
ADD CONSTRAINT fk_organisation_business
FOREIGN KEY (BusinessTypeID)
REFERENCES BusinessType(BusinessTypeID)
GO

ALTER TABLE dbo.Organisation
ADD Postcode int
GO

ALTER TABLE dbo.Organisation
ADD PhoneNumber nvarchar(25)
GO

ALTER TABLE dbo.Organisation
ADD Fax nvarchar(255)
GO

ALTER TABLE dbo.Organisation
ADD Email nvarchar(255)
GO

ALTER TABLE dbo.Organisation
ADD WebAddress nvarchar(255)
GO

ALTER TABLE dbo.Organisation
ADD CharityNumber nvarchar(255)
GO

ALTER TABLE dbo.Organisation
ADD CompanyNumber nvarchar(255)
GO

ALTER TABLE dbo.Organisation
ADD CountyID int
GO

ALTER TABLE dbo.Organisation
ADD CONSTRAINT fk_organisation_county
FOREIGN KEY (CountyID)
REFERENCES County(CountyID)
GO

ALTER TABLE dbo.Organisation
ADD TownName nvarchar(255)
GO

ALTER TABLE dbo.Organisation
ADD TrustRegionID int
GO

ALTER TABLE dbo.Organisation
ADD CONSTRAINT fk_organisation_trustregion
FOREIGN KEY (TrustRegionID)
REFERENCES dbo.TrustRegion(TrustRegionID)
GO

ALTER TABLE dbo.Organisation
ADD TrustDistrictID int
GO

ALTER TABLE dbo.Organisation
ADD CONSTRAINT fk_organisation_trustdistrict
FOREIGN KEY (TrustDistrictID)
REFERENCES dbo.TrustDistrict(TrustDistrictID)
GO

ALTER TABLE dbo.Organisation
ADD IsActive int
GO

/*Create tables for tab2 organisation*/
CREATE TABLE OrganisationSpecicalism (
	OrganisationSpecicalismID int identity(1,1),
	Value nvarchar(255)
)
GO

IF (SELECT COUNT(*) FROM dbo.OrganisationSpecicalism) = 0
BEGIN
	INSERT INTO dbo.OrganisationSpecicalism(Value)
	VALUES ('Blind/Partical Sighted')
	INSERT INTO dbo.OrganisationSpecicalism(Value)
	VALUES ('Deaf/Hard Of Hearing')
	INSERT INTO dbo.OrganisationSpecicalism(Value)
	VALUES ('Dyslexia')
	INSERT INTO dbo.OrganisationSpecicalism(Value)
	VALUES ('Learning Disability')
	INSERT INTO dbo.OrganisationSpecicalism(Value)
	VALUES ('Metal Heath')
END
GO

CREATE TABLE ServiceDisabilitiesCapabilities (
	ServiceDisabilitiesCapabilitiesID int identity(1,1),
	Value nvarchar(255)
)
GO

IF (SELECT COUNT(*) FROM dbo.ServiceDisabilitiesCapabilities) = 0
BEGIN
	INSERT INTO dbo.ServiceDisabilitiesCapabilities(Value)
	VALUES ('Chest, Breathing problems')
	INSERT INTO dbo.ServiceDisabilitiesCapabilities(Value)
	VALUES ('Condition restricting mobility')
	INSERT INTO dbo.ServiceDisabilitiesCapabilities(Value)
	VALUES ('Diabetes')
	INSERT INTO dbo.ServiceDisabilitiesCapabilities(Value)
	VALUES ('Difficulty in hearing')	
END
GO

CREATE TABLE ServiceBarriersCapabilities (
	ServiceBarriersCapabilitiesID int identity(1,1),
	Value nvarchar(255)
)
GO

IF (SELECT COUNT(*) FROM dbo.ServiceBarriersCapabilities) = 0
BEGIN
	INSERT INTO dbo.ServiceBarriersCapabilities(Value)
	VALUES ('Lone Parent')
	INSERT INTO dbo.ServiceBarriersCapabilities(Value)
	VALUES ('ESOL')
	INSERT INTO dbo.ServiceBarriersCapabilities(Value)
	VALUES ('Refugee')
	INSERT INTO dbo.ServiceBarriersCapabilities(Value)
	VALUES ('Basic Skills')	
END
GO

CREATE TABLE ServicePersonalCircumstancesCapabilities (
	ServicePersonalCircumstancesCapabilitiesID int identity(1,1),
	Value nvarchar(255)
)
GO

IF (SELECT COUNT(*) FROM dbo.ServicePersonalCircumstancesCapabilities) = 0
BEGIN
	INSERT INTO dbo.ServicePersonalCircumstancesCapabilities(Value)
	VALUES ('Carer Respsibilities')
	INSERT INTO dbo.ServicePersonalCircumstancesCapabilities(Value)
	VALUES ('Lone Parent')
END
GO

CREATE TABLE ServiceEthnicityCapabilities (
	ServiceEthnicityCapabilitiesID int identity(1,1),
	Value nvarchar(255)
)
GO

IF (SELECT COUNT(*) FROM dbo.ServiceEthnicityCapabilities) = 0
BEGIN
	INSERT INTO dbo.ServiceEthnicityCapabilities(Value)
	VALUES ('White British')
	INSERT INTO dbo.ServiceEthnicityCapabilities(Value)
	VALUES ('White Irish')
	INSERT INTO dbo.ServiceEthnicityCapabilities(Value)
	VALUES ('Other White')
	INSERT INTO dbo.ServiceEthnicityCapabilities(Value)
	VALUES ('White & Black Caribean')	
END
GO

CREATE TABLE Accreditation (
	AccreditationID int identity(1,1),
	Value nvarchar(255)
)
GO

IF (SELECT COUNT(*) FROM dbo.Accreditation) = 0
BEGIN
	INSERT INTO dbo.Accreditation(Value)
	VALUES ('Two Ticks')
	INSERT INTO dbo.Accreditation(Value)
	VALUES ('Investor In People')
	INSERT INTO dbo.Accreditation(Value)
	VALUES ('ISO 9001')
	INSERT INTO dbo.Accreditation(Value)
	VALUES ('ISO 27001')	
END
GO
/* create organisation criterias*/
CREATE TABLE OrganisationCriteria (
	OrganisationCriteriaID int identity(1,1),
	OrgID int,
	CriteriaName nvarchar(255),
	criteriaValue nvarchar(255),
	PRIMARY KEY (OrganisationCriteriaID),
	FOREIGN KEY (OrgID) REFERENCES dbo.Organisation(OrgID)
)
GO

/*Create organisation-office region*/


/*Simulate POSTZON System*/
CREATE TABLE PostZon (
	PostCode int identity(1,1),
	Ward nvarchar(255),
	Borough nvarchar(255),
	LocalAuthority nvarchar(255),
	UnitaryAuthority nvarchar(255),
	NHSAuthority nvarchar(255),
	PRIMARY KEY (PostCode)
)
GO
/*Create Supporting Materials Maintenance*/
CREATE TABLE SupportingMaterialsMaintenance (
	SupportingMaterialsMaintenanceID int identity(1,1),
	URL text,
	Description text,
	Type nvarchar(10),
	AddedBy nvarchar(255),
	AddedDate datetime,
	OrgID int,
	PRIMARY KEY (SupportingMaterialsMaintenanceID),
	FOREIGN KEY (OrgID) REFERENCES Organisation(OrgID)
)
GO

/*Insert column to directory*/
ALTER TABLE dbo.Directorate
ADD Name nvarchar(255)
GO
ALTER TABLE dbo.Directorate
ADD ShortDescription nvarchar(255)
GO

ALTER TABLE dbo.Directorate
ADD FullDescription nvarchar(255)
GO
ALTER TABLE dbo.Directorate
ADD AddressLine1 nvarchar(255)
GO

ALTER TABLE dbo.Directorate
ADD AddressLine2 nvarchar(255)
GO

ALTER TABLE dbo.Directorate
ADD AddressLine3 nvarchar(255)
GO

ALTER TABLE dbo.Directorate
ADD CountryName nvarchar(255)
GO

ALTER TABLE dbo.Directorate
ADD BusinessTypeID int
GO

ALTER TABLE dbo.Directorate
ADD CONSTRAINT fk_Directorate_business
FOREIGN KEY (BusinessTypeID)
REFERENCES BusinessType(BusinessTypeID)
GO

ALTER TABLE dbo.Directorate
ADD Postcode int
GO

ALTER TABLE dbo.Directorate
ADD PhoneNumber nvarchar(25)
GO

ALTER TABLE dbo.Directorate
ADD Fax nvarchar(255)
GO

ALTER TABLE dbo.Directorate
ADD WebAddress nvarchar(255)
GO

ALTER TABLE dbo.Directorate
ADD CharityNumber nvarchar(255)
GO

ALTER TABLE dbo.Directorate
ADD CompanyNumber nvarchar(255)
GO

ALTER TABLE dbo.Directorate
ADD CountyID int
GO

ALTER TABLE dbo.Directorate
ADD CONSTRAINT fk_Directorate_county
FOREIGN KEY (CountyID)
REFERENCES County(CountyID)
GO

ALTER TABLE dbo.Directorate
ADD TownName nvarchar(255)
GO

ALTER TABLE dbo.Directorate
ADD Email nvarchar(255)
GO

ALTER TABLE dbo.Directorate
ADD IsActive int
GO

/*Insert column to Department*/
ALTER TABLE dbo.Department
ADD Name nvarchar(255)
GO
ALTER TABLE dbo.Department
ADD ShortDescription nvarchar(255)
GO

ALTER TABLE dbo.Department
ADD FullDescription nvarchar(255)
GO
ALTER TABLE dbo.Department
ADD AddressLine1 nvarchar(255)
GO

ALTER TABLE dbo.Department
ADD AddressLine2 nvarchar(255)
GO

ALTER TABLE dbo.Department
ADD AddressLine3 nvarchar(255)
GO

ALTER TABLE dbo.Department
ADD CountryName nvarchar(255)
GO

ALTER TABLE dbo.Department
ADD BusinessTypeID int
GO

ALTER TABLE dbo.Department
ADD CONSTRAINT fk_Department_business
FOREIGN KEY (BusinessTypeID)
REFERENCES BusinessType(BusinessTypeID)
GO

ALTER TABLE dbo.Department
ADD Postcode int
GO

ALTER TABLE dbo.Department
ADD PhoneNumber nvarchar(25)
GO

ALTER TABLE dbo.Department
ADD Fax nvarchar(255)
GO

ALTER TABLE dbo.Department
ADD Email nvarchar(255)
GO

ALTER TABLE dbo.Department
ADD WebAddress nvarchar(255)
GO

ALTER TABLE dbo.Department
ADD CharityNumber nvarchar(255)
GO

ALTER TABLE dbo.Department
ADD CompanyNumber nvarchar(255)
GO

ALTER TABLE dbo.Department
ADD CountyID int
GO

ALTER TABLE dbo.Department
ADD CONSTRAINT fk_Department_county
FOREIGN KEY (CountyID)
REFERENCES County(CountyID)
GO

ALTER TABLE dbo.Department
ADD TownName nvarchar(255)
GO

ALTER TABLE dbo.Department
ADD IsActive int
GO

/*Insert column to Team*/
ALTER TABLE dbo.Team
ADD Name nvarchar(255)
GO
ALTER TABLE dbo.Team
ADD ShortDescription nvarchar(255)
GO

ALTER TABLE dbo.Team
ADD FullDescription nvarchar(255)
GO
ALTER TABLE dbo.Team
ADD AddressLine1 nvarchar(255)
GO

ALTER TABLE dbo.Team
ADD AddressLine2 nvarchar(255)
GO

ALTER TABLE dbo.Team
ADD AddressLine3 nvarchar(255)
GO

ALTER TABLE dbo.Team
ADD CountryName nvarchar(255)
GO

ALTER TABLE dbo.Team
ADD BusinessTypeID int
GO

ALTER TABLE dbo.Team
ADD CONSTRAINT fk_Team_business
FOREIGN KEY (BusinessTypeID)
REFERENCES BusinessType(BusinessTypeID)
GO

ALTER TABLE dbo.Team
ADD Postcode int
GO

ALTER TABLE dbo.Team
ADD PhoneNumber nvarchar(25)
GO

ALTER TABLE dbo.Team
ADD Fax nvarchar(255)
GO

ALTER TABLE dbo.Team
ADD Email nvarchar(255)
GO

ALTER TABLE dbo.Team
ADD WebAddress nvarchar(255)
GO

ALTER TABLE dbo.Team
ADD CharityNumber nvarchar(255)
GO

ALTER TABLE dbo.Team
ADD CompanyNumber nvarchar(255)
GO

ALTER TABLE dbo.Team
ADD CountyID int
GO

ALTER TABLE dbo.Team
ADD CONSTRAINT fk_Team_county
FOREIGN KEY (CountyID)
REFERENCES County(CountyID)
GO

ALTER TABLE dbo.Team
ADD TownName nvarchar(255)
GO

ALTER TABLE dbo.Team
ADD IsActive int
GO
/*Add to Programme*/
ALTER TABLE Programme
ADD  Name nvarchar(255)
GO
ALTER TABLE Programme
ADD  Description nvarchar(255)
GO
ALTER TABLE Programme
ADD  IsActive int
GO
/*Add to business type*/
ALTER TABLE dbo.BusinessType
ADD Name nvarchar(255)
GO


