package mock.appcode.web.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mock.appcode.business.OrganisationBO;
import mock.appcode.business.OrganisationBOImpl;
import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.Constant;
import mock.appcode.common.valueobjects.OrganisationVO;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * @author do duc trung
 * @version Aug 5, 2014
 */
public class OrganisationAction extends ActionSupport implements Preparable,
        SessionAware {

    /**
     * 
     */
    private List<OrganisationVO> listOrganisation;
    private OrganisationBO organisationBo;
    private static final long serialVersionUID = 1L;
    private String filterStr;
    private boolean includeInactive;
    private int selectedId;
    private OrganisationVO selectedOrganisation;
    private Map<String, Object> session;
    private List<String> countryName;
    static String FILTER_LIST = "filter_list_Organisation";

    /*
     * load all Organisation
     */
    public String doList() {
        try {
            if (listOrganisation == null) {
                listOrganisation = organisationBo.listAllOrganisation();
            }
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return SUCCESS;
    }

    /**
     * do filter
     */
    public String doFilter() {
        String option = "";
        System.out.println("Filter: " + filterStr);
        if (filterStr.equals("all")) {
            option = OrganisationBOImpl.OPTION_ALL;
        } else if (filterStr.equals("0-9")) {
            option = OrganisationBOImpl.OPTION_0_9;
        } else if (filterStr.equals("A-E")) {
            option = OrganisationBOImpl.OPTION_A_E;
        } else if (filterStr.equals("F-J")) {
            option = OrganisationBOImpl.OPTION_F_J;
        } else if (filterStr.equals("K-N")) {
            option = OrganisationBOImpl.OPTION_K_N;
        } else if (filterStr.equals("O-R")) {
            option = OrganisationBOImpl.OPTION_O_R;
        } else if (filterStr.equals("S-V")) {
            option = OrganisationBOImpl.OPTION_S_V;
        } else if (filterStr.equals("W-Z")) {
            option = OrganisationBOImpl.OPTION_W_Z;
        }
        try {
            listOrganisation = organisationBo
                    .listAllOrganisationWithOption(option);
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (listOrganisation == null) {
            listOrganisation = new ArrayList<OrganisationVO>();
        } else {
            includeInactive = true;
            session.put(FILTER_LIST, listOrganisation);
        }
        return SUCCESS;
    }

    public String doShowInactive() {
        List<OrganisationVO> listTmp = (List<OrganisationVO>) session
                .get(FILTER_LIST);
        if (listTmp != null) {
            listOrganisation = new ArrayList<OrganisationVO>();
            listOrganisation.addAll(listTmp);
        }
        if (listOrganisation == null) {
            try {
                listOrganisation = organisationBo
                        .listAllOrganisationIncludeInactive(includeInactive);
            } catch (DataAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (FunctionalException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            if (!includeInactive) {
                for (int i = 0; i < listOrganisation.size(); i++) {
                    OrganisationVO Organisation = listOrganisation.get(i);
                    if (!Organisation.getIsActive()) {
                        listOrganisation.remove(i);
                        i--;
                    }
                }
            }
            System.out.println("list after: " + listOrganisation.size());
        }
        return SUCCESS;
    }

    public String showOrganisationDetail() {
        try {
            selectedOrganisation = organisationBo
                    .findOrganisationById(selectedId);
            countryName = organisationBo.returnAllNation();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return SUCCESS;
    }

    public String createNewOrganisation() {

        return SUCCESS;
    }

    public String inactiveOrganisation() {

        return SUCCESS;
    }

    public String activeOrganisation() {
        try {
            organisationBo.changeActiveStatus(selectedId, 1);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        }
        return SUCCESS;
    }

    public String showCreatePage() {

        return SUCCESS;
    }

    public String copyAddressesFromDirectorate() {
        return SUCCESS;
    }

    public List<OrganisationVO> getListOrganisation() {
        return listOrganisation;
    }

    public void setListOrganisation(List<OrganisationVO> listOrganisation) {
        this.listOrganisation = listOrganisation;
    }

    public OrganisationBO getOrganisationBo() {
        return organisationBo;
    }

    public void setOrganisationBo(OrganisationBO organisationBo) {
        this.organisationBo = organisationBo;
    }

    public String getFilterStr() {
        return filterStr;
    }

    public void setFilterStr(String filterStr) {
        this.filterStr = filterStr;
    }

    public boolean isIncludeInactive() {
        return includeInactive;
    }

    public void setIncludeInactive(boolean includeInactive) {
        this.includeInactive = includeInactive;
    }

    public int getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(int selectedId) {
        this.selectedId = selectedId;
    }

    public OrganisationVO getSelectedOrganisation() {
        return selectedOrganisation;
    }

    public void setSelectedOrganisation(OrganisationVO selectedOrganisation) {
        this.selectedOrganisation = selectedOrganisation;
    }

    public Map<String, Object> getSession() {
        return session;
    }

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    public List<String> getCountryName() {
        return countryName;
    }

    public void setCountryName(List<String> countryName) {
        this.countryName = countryName;
    }

    @Override
    public void prepare() throws Exception {
        // TODO Auto-generated method stub
        organisationBo = new OrganisationBOImpl();
        countryName = organisationBo.returnAllNation();
    }
}
