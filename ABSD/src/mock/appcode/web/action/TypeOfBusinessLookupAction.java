package mock.appcode.web.action;

import java.sql.SQLException;
import java.util.Vector;

import mock.appcode.business.TypeOfBusinessBOImpl;
import mock.appcode.common.valueobjects.TypeOfBusinessVO;

import com.opensymphony.xwork2.ActionSupport;

public class TypeOfBusinessLookupAction extends ActionSupport {
	private String businessName;
	private String sicCode;
	private Vector<TypeOfBusinessVO> typeOfBusinessList;

	public String searchTypeOfBusiness() {

		try {
			typeOfBusinessList = new TypeOfBusinessBOImpl()
					.searchTypeOfBusinessByOption(businessName, sicCode);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public Vector<TypeOfBusinessVO> getTypeOfBusinessList() {
		return typeOfBusinessList;
	}

	public void setTypeOfBusinessList(
			Vector<TypeOfBusinessVO> typeOfBusinessList) {
		this.typeOfBusinessList = typeOfBusinessList;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getSicCode() {
		return sicCode;
	}

	public void setSicCode(String sicCode) {
		this.sicCode = sicCode;
	}

}
