package mock.appcode.web.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mock.appcode.business.TeamBO;
import mock.appcode.business.TeamBOImpl;
import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.Constant;
import mock.appcode.common.valueobjects.TeamVO;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

public class TeamAction extends ActionSupport implements Preparable, SessionAware{
	private List<TeamVO> listTeam;
    private TeamBO teamBo;
    private static final long serialVersionUID = 1L;
    private String filterStr;
    private boolean includeInactive;
    private int selectedId;
    private TeamVO selectedTeam;
    private Map<String, Object> session;
    private List<String> countryName;
    static String FILTER_LIST = "filter_list_team";

    /*
     * load all team
     */
    public String doList() {
        try {
            if (listTeam == null) {
                listTeam = teamBo.listAllTeam();
            }
        } catch (DataAccessException ex) {

        } catch (FunctionalException ex) {

        } catch (ClassNotFoundException ex) {

        } catch (SQLException ex) {

        }
        return SUCCESS;
    }

    /**
     * do filter
     */
    public String doFilter() {
        String option = "";
        System.out.println("Filter: " + filterStr);
        if (filterStr.equals("all")) {
            option = TeamBOImpl.OPTION_ALL;
        } else if (filterStr.equals("0-9")) {
            option = TeamBOImpl.OPTION_0_9;
        } else if (filterStr.equals("A-E")) {
            option = TeamBOImpl.OPTION_A_E;
        } else if (filterStr.equals("F-J")) {
            option = TeamBOImpl.OPTION_F_J;
        } else if (filterStr.equals("K-N")) {
            option = TeamBOImpl.OPTION_K_N;
        } else if (filterStr.equals("O-R")) {
            option = TeamBOImpl.OPTION_O_R;
        } else if (filterStr.equals("S-V")) {
            option = TeamBOImpl.OPTION_S_V;
        } else if (filterStr.equals("W-Z")) {
            option = TeamBOImpl.OPTION_W_Z;
        }
        try {
            listTeam = teamBo.listAllTeamWithOption(option);
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (listTeam == null) {
            listTeam = new ArrayList<TeamVO>();
        } else {
            includeInactive = true;
            session.put(FILTER_LIST, listTeam);
        }
        return SUCCESS;
    }

    public String doShowInactive() {
        List<TeamVO> listTmp = (List<TeamVO>) session.get(FILTER_LIST);
        if (listTmp != null) {
            listTeam = new ArrayList<TeamVO>();
            listTeam.addAll(listTmp);
        }
        if (listTeam == null) {
            try {
                listTeam = teamBo
                        .listAllTeamIncludeInactive(includeInactive);
            } catch (DataAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (FunctionalException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            if (!includeInactive) {
                for (int i = 0; i < listTeam.size(); i++) {
                    TeamVO team = listTeam.get(i);
                    if (!team.getIsActive()) {
                        listTeam.remove(i);
                        i--;
                    }
                }
            }
            System.out.println("list after: " + listTeam.size());
        }
        return SUCCESS;
    }

    public String showTeamDetail() {
        try {
            selectedTeam = teamBo.findTeamByID(selectedId);
            countryName = teamBo.returnAllNation();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return SUCCESS;
    }

    public String createNewTeam() {
        try {
            boolean b = false;
            if (selectedTeam.getId() > 0) {
                b = teamBo.updateTeam(selectedTeam);
            } else {
                b = teamBo.createNewTeam(selectedTeam);
            }
            if (!b) {
                return ERROR;
            }
        } catch (DataValidationException e) {
            // TODO Auto-generated catch block
            if (e.getMessage().equals(Constant.POSTCODE_ERROR)) {
                addActionError(Constant.POSTCODE_ERROR);
                return ERROR;
            }
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        }
        return SUCCESS;
    }

    public String inactiveTeam() {
        try {
            teamBo.inActiveTeamWithID(selectedTeam.getId());
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        }
        return SUCCESS;
    }
    
    public String activeTeam() {
        try {
            teamBo.activeTeamWithID(selectedId);
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        }
        return SUCCESS;
    }
    
    public String showCreatePage() {
        try {
            countryName = teamBo.returnAllNation();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return SUCCESS;
    }

    public String copyAddressesFromDepartment() {
        return SUCCESS;
    }

    public List<TeamVO> getListTeam() {
        return listTeam;
    }

    public void setListTeam(List<TeamVO> listTeam) {
        this.listTeam = listTeam;
    }

    public TeamBO getTeamBo() {
        return teamBo;
    }

    public void setTeamBo(TeamBO teamBo) {
        this.teamBo = teamBo;
    }

    public String getFilterStr() {
        return filterStr;
    }

    public void setFilterStr(String filterStr) {
        this.filterStr = filterStr;
    }

    public boolean isIncludeInactive() {
        return includeInactive;
    }

    public void setIncludeInactive(boolean includeInactive) {
        this.includeInactive = includeInactive;
    }

    public int getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(int selectedId) {
        this.selectedId = selectedId;
    }

    public TeamVO getSelectedTeam() {
        return selectedTeam;
    }

    public void setSelectedTeam(TeamVO selectedTeam) {
        this.selectedTeam = selectedTeam;
    }
    
    

    public List<String> getCountryName() {
        return countryName;
    }

    public void setCountryName(List<String> countryName) {
        this.countryName = countryName;
    }

    public Map<String, Object> getSession() {
        return session;
    }

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    @Override
    public void prepare() throws Exception {
        // TODO Auto-generated method stub
        teamBo = new TeamBOImpl();
        countryName = teamBo.returnAllNation();
    }

}
