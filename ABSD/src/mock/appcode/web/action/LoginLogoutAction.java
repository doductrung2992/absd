package mock.appcode.web.action;

import java.sql.SQLException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import mock.appcode.business.UserBOImpl;
import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.valueobjects.UserVO;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

public class LoginLogoutAction extends ActionSupport implements SessionAware{
	/**
     * 
     */
    private static final long serialVersionUID = 1L;
    private UserVO user;
    private Map<String, Object> session;
    public static String USER_SESSION="user_absd";
    private String email;
    
	public String login() {
		try {
			if (new UserBOImpl().checkLogin(user)) {
			    session.put(USER_SESSION, user);
				return SUCCESS;
			} else {
			    addActionError("Account or Password is not match!");
				return ERROR;
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DataValidationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
		return ERROR;
	}
	
	public String checkLogin() {
	    user = (UserVO) session.get(USER_SESSION);
	    if(user == null) {
	        return ERROR;
	    }
	    return SUCCESS;
	}
	
	public String sendToEmail() {
	    String email_patter = 
	            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
	            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	    Pattern pattern = Pattern.compile(email_patter);
	    Matcher matcher = pattern.matcher(email);
	    if(matcher.matches()) {
	        return SUCCESS;
	    }
	    addActionError("Email is invalid!");
	    return ERROR;
	}

	public String logout() {

		return SUCCESS;
	}

    public UserVO getUser() {
        return user;
    }

    public void setUser(UserVO user) {
        this.user = user;
    }
    
    

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Map<String, Object> getSession() {
        return session;
    }

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
    
    
	

}
