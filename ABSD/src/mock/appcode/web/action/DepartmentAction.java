package mock.appcode.web.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mock.appcode.business.DepartmentBO;
import mock.appcode.business.DepartmentBOImpl;
import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.Constant;
import mock.appcode.common.valueobjects.DepartmentVO;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * @author do duc trung
 * @version Aug 5, 2014
 */
public class DepartmentAction extends ActionSupport implements Preparable,
        SessionAware {

    /**
     * 
     */
    private List<DepartmentVO> listDepartment;
    private DepartmentBO departmentBo;
    private static final long serialVersionUID = 1L;
    private String filterStr;
    private boolean includeInactive;
    private int selectedId;
    private DepartmentVO selectedDepartment;
    private Map<String, Object> session;
    private List<String> countryName;
    static String FILTER_LIST = "filter_list_department";
    private int dirID;

    /*
     * load all department
     */
    public String doList() {
        try {
            if (listDepartment == null) {
                listDepartment = departmentBo.listAllDepartment(dirID);                
            }
        } catch (DataAccessException ex) {

        } catch (FunctionalException ex) {

        } catch (ClassNotFoundException ex) {

        } catch (SQLException ex) {

        }
        return SUCCESS;
    }

    /**
     * do filter
     */
    public String doFilter() {
        String option = "";
        System.out.println("Filter: " + filterStr);
        if (filterStr.equals("all")) {
            option = DepartmentBOImpl.OPTION_ALL;
        } else if (filterStr.equals("0-9")) {
            option = DepartmentBOImpl.OPTION_0_9;
        } else if (filterStr.equals("A-E")) {
            option = DepartmentBOImpl.OPTION_A_E;
        } else if (filterStr.equals("F-J")) {
            option = DepartmentBOImpl.OPTION_F_J;
        } else if (filterStr.equals("K-N")) {
            option = DepartmentBOImpl.OPTION_K_N;
        } else if (filterStr.equals("O-R")) {
            option = DepartmentBOImpl.OPTION_O_R;
        } else if (filterStr.equals("S-V")) {
            option = DepartmentBOImpl.OPTION_S_V;
        } else if (filterStr.equals("W-Z")) {
            option = DepartmentBOImpl.OPTION_W_Z;
        }
        try {
            listDepartment = departmentBo.listAllDepartmentWithOption(dirID,option);            
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (listDepartment == null) {
            listDepartment = new ArrayList<DepartmentVO>();
        } else {
            includeInactive = true;
            session.put(FILTER_LIST, listDepartment);
        }
        return SUCCESS;
    }

    public String doShowInactive() {
        List<DepartmentVO> listTmp = (List<DepartmentVO>) session
                .get(FILTER_LIST);
        if (listTmp != null) {
            listDepartment = new ArrayList<DepartmentVO>();
            listDepartment.addAll(listTmp);
        }
        if (listDepartment == null) {
            try {
                listDepartment = departmentBo
                        .listAllDepartmentIncludeInactive(dirID,includeInactive);
            } catch (DataAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (FunctionalException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            if (!includeInactive) {
                for (int i = 0; i < listDepartment.size(); i++) {
                    DepartmentVO department = listDepartment.get(i);
                    if (!department.getIsActive()) {
                        listDepartment.remove(i);
                        i--;
                    }
                }
            }
            System.out.println("list after: " + listDepartment.size());
        }
        return SUCCESS;
    }

    public String showDepartmentDetail() {
        try {
            selectedDepartment = departmentBo.findDepartmentByID(selectedId);
            countryName = departmentBo.returnAllNation();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return SUCCESS;
    }

    public String createNewDepartment() {
        try {
            String fieldEmptyStr = "Madatory field";
            if(selectedDepartment.getName().length() == 0) {
                addFieldError("selectedDepartment.name", fieldEmptyStr);
                return ERROR;
            }
            if(selectedDepartment.getShortDescription().length() == 0) {
                addFieldError("selectedDepartment.shortDescription", fieldEmptyStr);
                return ERROR;
            }
            boolean b = false;
            if (selectedDepartment.getId() > 0) {
                b = departmentBo.updateDepartment(selectedDepartment);
            } else {
                b = departmentBo.createNewDepartment(dirID,selectedDepartment);
            }
            if (!b) {
                return ERROR;
            }
        } catch (DataValidationException e) {
            // TODO Auto-generated catch block
            if (e.getMessage().equals(Constant.POSTCODE_ERROR)) {
                addFieldError("selectedDepartment.postcode", Constant.POSTCODE_ERROR);
                addActionError(Constant.POSTCODE_ERROR);
                return ERROR;
            }
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        }
        return SUCCESS;
    }

    public String inactiveDepartment() {
        try {
            departmentBo.inActiveDepartmentWithID(selectedDepartment.getId());
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        }
        return SUCCESS;
    }
    
    public String activeDepartment() {
        try {
            departmentBo.activeDepartmentWithID(selectedId);
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            addActionError(e.toString());
            return ERROR;
        }
        return SUCCESS;
    }
    
    public String showCreatePage() {
        try {
            countryName = departmentBo.returnAllNation();
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return SUCCESS;
    }    
    
    public String copyAddressesFromDirectorate() {
        return SUCCESS;
    }

    public List<DepartmentVO> getListDepartment() {
        return listDepartment;
    }

    public void setListDepartment(List<DepartmentVO> listDepartment) {
        this.listDepartment = listDepartment;
    }

    public DepartmentBO getDepartmentBo() {
        return departmentBo;
    }

    public void setDepartmentBo(DepartmentBO departmentBo) {
        this.departmentBo = departmentBo;
    }

    public String getFilterStr() {
        return filterStr;
    }

    public void setFilterStr(String filterStr) {
        this.filterStr = filterStr;
    }

    public boolean isIncludeInactive() {
        return includeInactive;
    }

    public void setIncludeInactive(boolean includeInactive) {
        this.includeInactive = includeInactive;
    }

    public int getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(int selectedId) {
        this.selectedId = selectedId;
    }

    public DepartmentVO getSelectedDepartment() {
        return selectedDepartment;
    }

    public void setSelectedDepartment(DepartmentVO selectedDepartment) {
        this.selectedDepartment = selectedDepartment;
    }

    public Map<String, Object> getSession() {
        return session;
    }

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }
    

    public List<String> getCountryName() {
        return countryName;
    }

    public void setCountryName(List<String> countryName) {
        this.countryName = countryName;
    }
    

    public int getDirID() {
        return dirID;
    }

    public void setDirID(int dirID) {
        this.dirID = dirID;
    }

    @Override
    public void prepare() throws Exception {
        // TODO Auto-generated method stub
        departmentBo = new DepartmentBOImpl();
        countryName = departmentBo.returnAllNation();
    }
}
