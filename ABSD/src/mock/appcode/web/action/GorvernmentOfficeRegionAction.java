package mock.appcode.web.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mock.appcode.business.GovernmentOfficeRegionBO;
import mock.appcode.business.GovernmentOfficeRegionBOImpl;
import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.GovernmentOfficeRegionVO;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * @author do duc trung
 * @version Aug 5, 2014
 */
public class GorvernmentOfficeRegionAction extends ActionSupport implements
        Preparable, SessionAware {

    /**
     * 
     */
    private List<GovernmentOfficeRegionVO> listRegion;
    private GovernmentOfficeRegionBO regionBo;
    private static final long serialVersionUID = 1L;
    private String filterStr;
    private boolean includeInactive;
    private int selectedId;
    private GovernmentOfficeRegionVO selectedRegion;
    private Map<String, Object> session;
    static String FILTER_LIST = "filter_list";

    /*
     * load all region
     */
    public String doList() {
        try {
            if (listRegion == null) {
                listRegion = regionBo.listAllRegion();
            }
        } catch (DataAccessException ex) {

        } catch (FunctionalException ex) {

        } catch (ClassNotFoundException ex) {

        } catch (SQLException ex) {

        }
        return SUCCESS;
    }

    /**
     * do filter
     */
    public String doFilter() {
        String option = "";
        System.out.println("Filter: "+filterStr);
        if (filterStr.equals("all")) {
            option = GovernmentOfficeRegionBOImpl.OPTION_ALL;
        } else if (filterStr.equals("0-9")) {
            option = GovernmentOfficeRegionBOImpl.OPTION_0_9;
        } else if (filterStr.equals("A-E")) {
            option = GovernmentOfficeRegionBOImpl.OPTION_A_E;
        } else if (filterStr.equals("F-J")) {
            option = GovernmentOfficeRegionBOImpl.OPTION_F_J;
        } else if (filterStr.equals("K-N")) {
            option = GovernmentOfficeRegionBOImpl.OPTION_K_N;
        } else if (filterStr.equals("O-R")) {
            option = GovernmentOfficeRegionBOImpl.OPTION_O_R;
        } else if (filterStr.equals("S-V")) {
            option = GovernmentOfficeRegionBOImpl.OPTION_S_V;
        } else if (filterStr.equals("W-Z")) {
            option = GovernmentOfficeRegionBOImpl.OPTION_W_Z;
        }
        try {
            listRegion = regionBo.listAllRegionWithOption(option);
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (listRegion == null) {
            listRegion = new ArrayList<GovernmentOfficeRegionVO>();
        } else {
            includeInactive = true;
            session.put(FILTER_LIST, listRegion);
        }
        return SUCCESS;
    }

    public String doShowInactive() {                
        List<GovernmentOfficeRegionVO> listTmp = (List<GovernmentOfficeRegionVO>) session.get(FILTER_LIST);
        if(listTmp != null) {
            listRegion = new ArrayList<GovernmentOfficeRegionVO>();
            listRegion.addAll(listTmp);
        }
        if (listRegion == null) {
            try {
                listRegion = regionBo
                        .listAllRegionIncludeInactive(includeInactive);
            } catch (DataAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (FunctionalException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            if (!includeInactive) {                
                for(int i = 0; i<listRegion.size(); i++){
                    GovernmentOfficeRegionVO region = listRegion.get(i);
                    if (region.getIsActive() == 0) {
                        listRegion.remove(i);
                        i--;                        
                    }
                }
            }
            System.out.println("list after: "+listRegion.size());
        }
        return SUCCESS;
    }

    public String showRegionDetail() {
        try {
            selectedRegion = regionBo.findRegionById(selectedId);
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return SUCCESS;
    }

    public boolean isIncludeInactive() {
        return includeInactive;
    }

    public void setIncludeInactive(boolean includeInactive) {
        this.includeInactive = includeInactive;
    }

    public List<GovernmentOfficeRegionVO> getListRegion() {
        return listRegion;
    }

    public void setListRegion(List<GovernmentOfficeRegionVO> listRegion) {
        this.listRegion = listRegion;
    }

    public String getFilterStr() {
        return filterStr;
    }

    public void setFilterStr(String filterStr) {
        this.filterStr = filterStr;
    }

    public int getSelectedId() {
        return selectedId;
    }

    public void setSelectedId(int selectedId) {
        this.selectedId = selectedId;
    }

    public GovernmentOfficeRegionVO getSelectedRegion() {
        return selectedRegion;
    }

    public void setSelectedRegion(GovernmentOfficeRegionVO selectedRegion) {
        this.selectedRegion = selectedRegion;
    }

    public Map<String, Object> getSession() {
        return session;
    }

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    @Override
    public void prepare() throws Exception {
        // TODO Auto-generated method stub
        regionBo = new GovernmentOfficeRegionBOImpl();
    }
}
