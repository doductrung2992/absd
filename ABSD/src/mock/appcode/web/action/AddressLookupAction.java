package mock.appcode.web.action;

import java.sql.SQLException;
import java.util.Vector;

import mock.appcode.business.AddressBOImpl;
import mock.appcode.common.valueobjects.AddressVO;

import com.opensymphony.xwork2.ActionSupport;

public class AddressLookupAction extends ActionSupport {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String postcode, street, town;
	private Vector<AddressVO> addList;

	public String searchAddress() throws ClassNotFoundException, SQLException {
		AddressBOImpl abo = new AddressBOImpl();
		addList = abo.findAddressByOption(postcode, street, town);
		return SUCCESS;
	}

	public String getPostcode() {
		return postcode;
	}

	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getTown() {
		return town;
	}

	public void setTown(String town) {
		this.town = town;
	}

	public Vector<AddressVO> getAddList() {
		return addList;
	}

	public void setAddList(Vector<AddressVO> addList) {
		this.addList = addList;
	}

}
