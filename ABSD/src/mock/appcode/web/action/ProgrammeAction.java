package mock.appcode.web.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mock.appcode.business.ProgrammeBOImpl;
import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.ProgrammeVO;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

/**
 * @author do duc trung
 * @version Aug 5, 2014
 */
public class ProgrammeAction extends ActionSupport implements
        Preparable, SessionAware {

    /**
     * 
     */
    private List<ProgrammeVO> listProgramme;
    private ProgrammeBOImpl programmeBo;
    private static final long serialVersionUID = 1L;
    private String filterStr;
    private boolean includeInactive;
    private int selectedId;
    private ProgrammeVO selectedProgramme;
    private Map<String, Object> session;
    static String FILTER_LIST = "filter_list";
    private String programmeName;
    private String description;
    private String contactName;
    private int isActive;

    /*
     * load all prog
     */
    public String doList() {
        try {
            if (listProgramme == null) {
                listProgramme = programmeBo.listAllProgramme();
            }
        } catch (DataAccessException ex) {

        } catch (FunctionalException ex) {

        } catch (ClassNotFoundException ex) {

        } catch (SQLException ex) {

        }
        return SUCCESS;
    }

    /**
     * do filter
     */
    public String doFilter() {
        String option = "";
//        System.out.println("Filter: "+filterStr);
        if (filterStr.equals("all")) {
            option = ProgrammeBOImpl.OPTION_ALL;
        } else if (filterStr.equals("0-9")) {
            option = ProgrammeBOImpl.OPTION_0_9;
        } else if (filterStr.equals("A-E")) {
            option = ProgrammeBOImpl.OPTION_A_E;
        } else if (filterStr.equals("F-J")) {
            option = ProgrammeBOImpl.OPTION_F_J;
        } else if (filterStr.equals("K-N")) {
            option = ProgrammeBOImpl.OPTION_K_N;
        } else if (filterStr.equals("O-R")) {
            option = ProgrammeBOImpl.OPTION_O_R;
        } else if (filterStr.equals("S-V")) {
            option = ProgrammeBOImpl.OPTION_S_V;
        } else if (filterStr.equals("W-Z")) {
            option = ProgrammeBOImpl.OPTION_W_Z;
        }
        try {
            listProgramme = programmeBo.listAllProgrammeWithOption(option);
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (listProgramme == null) {
            listProgramme = new ArrayList<ProgrammeVO>();
        } else {
            includeInactive = true;
            session.put(FILTER_LIST, listProgramme);
        }
        System.out.println("After: "+ listProgramme.size());
        return SUCCESS;
    }

    public String doShowInactive() {                
        List<ProgrammeVO> listTmp = (List<ProgrammeVO>) session.get(FILTER_LIST);
        if(listTmp != null) {
            listProgramme = new ArrayList<ProgrammeVO>();
            listProgramme.addAll(listTmp);
        }
        if (listProgramme == null) {
            try {
                listProgramme = programmeBo.listAllProgrammeIncludeInactive(includeInactive);
            } catch (DataAccessException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (FunctionalException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            if (!includeInactive) {                
                for(int i = 0; i<listProgramme.size(); i++){
                    ProgrammeVO prog = listProgramme.get(i);
                    if (prog.getIsActive() == 0) {
                        listProgramme.remove(i);
                        i--;                        
                    }
                }
            }
            System.out.println("list after: "+listProgramme.size());
        }
        return SUCCESS;
    }

    public String showProgrammeDetail() {
        try {
            selectedProgramme = programmeBo.findProgrammeById(selectedId);
        } catch (DataAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (FunctionalException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return SUCCESS;
    }

    public String inactiveProgramme(){

				try {
					programmeBo.setInactive(selectedId);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println("done set inactive!");
//				try {
//					selectedProgramme = programmeBo.findProgrammeById(selectedId);
//				} catch (DataAccessException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (FunctionalException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (ClassNotFoundException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				} catch (SQLException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}

         return SUCCESS;
    }
    public String saveProgrammeDetail(){
    	ProgrammeVO prog = new ProgrammeVO();
    	System.out.println("PROGRAMME Name: "+programmeName);
    	prog.setName(programmeName);
    	prog.setDescription(description);
    	prog.setContactName(contactName);
//		lookup contactId
    	int contactId = 1;
    	prog.setContactId(contactId);
    	ProgrammeBOImpl programmeBo = new ProgrammeBOImpl();
    	if(selectedId==0){	//add new programme
    		prog.setIsActive(1);	//default is Active
    		try {
				programmeBo.addProgramme(prog);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    		System.out.println("Add Programme - ID: "+ selectedId);
    	}
    	else{	// amend programme
    		prog.setId(selectedId);
    		prog.setIsActive(isActive);
    		try {
				programmeBo.amendProgramme(prog);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    		System.out.println("Update Programme, ID= "+ selectedId + " isActive= "+ isActive);
    	}
    	return SUCCESS;
    }
    public List<ProgrammeVO> getListProgramme() {
		return listProgramme;
	}

	public void setListProgramme(List<ProgrammeVO> listProgramme) {
		this.listProgramme = listProgramme;
	}

	public ProgrammeBOImpl getProgrammeBo() {
		return programmeBo;
	}

	public void setProgrammeBo(ProgrammeBOImpl programmeBo) {
		this.programmeBo = programmeBo;
	}

	public String getFilterStr() {
		return filterStr;
	}

	public void setFilterStr(String filterStr) {
		this.filterStr = filterStr;
	}

	public boolean isIncludeInactive() {
		return includeInactive;
	}

	public void setIncludeInactive(boolean includeInactive) {
		this.includeInactive = includeInactive;
	}

	public int getSelectedId() {
		return selectedId;
	}

	public void setSelectedId(int selectedId) {
		this.selectedId = selectedId;
	}

	public ProgrammeVO getSelectedProgramme() {
		return selectedProgramme;
	}

	public void setSelectedProgramme(ProgrammeVO selectedProgramme) {
		this.selectedProgramme = selectedProgramme;
	}
	
	public String getProgrammeName() {
		return programmeName;
	}

	public void setProgrammeName(String programmeName) {
		this.programmeName = programmeName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public Map<String, Object> getSession() {
        return session;
    }

    public void setSession(Map<String, Object> session) {
        this.session = session;
    }

    @Override
    public void prepare() throws Exception {
        // TODO Auto-generated method stub
        programmeBo = new ProgrammeBOImpl();
    }
}
