package mock.appcode.web.action;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import mock.appcode.business.BestContactMethodBO;
import mock.appcode.business.BestContactMethodBOImpl;
import mock.appcode.business.ContactBO;
import mock.appcode.business.ContactBOImpl;
import mock.appcode.business.ContactTypeBO;
import mock.appcode.business.ContactTypeBOImpl;
import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.BestContactMethodVO;
import mock.appcode.common.valueobjects.ContactTypeVO;
import mock.appcode.common.valueobjects.ContactVO;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

public class ContactAction extends ActionSupport implements Preparable,
		SessionAware {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;
	private List<ContactVO> listContact;
	private ContactBO contactBO;
	private ContactTypeBO contactTypeBO;
	private BestContactMethodBO bestContactMethodBO;
	private boolean includeInactive;
	private int selectedId;
	private ContactVO selectedContact;
	private String firstName;
	private String surName;
	private String filterStr;
	private Map<String, Object> session;
	static String FILTER_LIST = "filter_list";
	private List<ContactTypeVO> listContactType;
	private List<BestContactMethodVO> listBestContactMethod;
	private String selectedType;
	private String selectedBestMethod;

	public String searchContact() throws DataAccessException,
			FunctionalException, ClassNotFoundException, SQLException {
		contactBO = new ContactBOImpl();
		if ("".equals(firstName) && "".equals(surName))
			listContact = contactBO.listAllContact();
		else
			listContact = contactBO.findContactByName(firstName, surName);
		return SUCCESS;
	}

	public String doList() {
		try {
			if (listContact == null) {
				listContact = contactBO.listAllContact();
			}
		} catch (DataAccessException ex) {

		} catch (FunctionalException ex) {

		} catch (ClassNotFoundException ex) {

		} catch (SQLException ex) {

		}
		return SUCCESS;
	}

	public String doShowInactive() {
		try {
			listContact = contactBO
					.listAllContactIncludeInactive(includeInactive);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FunctionalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (!includeInactive) {
			for (int i = 0; i < listContact.size(); i++) {
				ContactVO contact = listContact.get(i);
				if (!contact.getIsActive()) {
					listContact.remove(i);
					i--;
				}
			}

			System.out.println("list after: " + listContact.size());
		}
		return SUCCESS;
	}

	public String getContactTypeAndMethod() {

		try {
			listContactType = contactTypeBO.listContactType();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			listBestContactMethod = bestContactMethodBO.listBestContactMethod();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return SUCCESS;

	}

	public String showContactDetail() {
		try {
			selectedContact = contactBO.findContactById(selectedId);
			selectedType = Integer.toString(selectedContact.getContactTypeID());
			selectedBestMethod = Integer.toString(selectedContact.getBestContactMethodID());
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FunctionalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String inactiveContact() {

		try {
			contactBO.setInactive(selectedId);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("done set inactive!");

		return SUCCESS;
	}

	public String saveContactDetail() {
	    selectedContact.setContactTypeID(Integer.parseInt(selectedType));
	    selectedContact.setBestContactMethodID(Integer.parseInt(selectedBestMethod));
		ContactBO contactBo = new ContactBOImpl();
		if (selectedContact.getId() == 0) { // add new contact
			try {
				contactBo.addNewContact(selectedContact);
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FunctionalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("Add Contact - ID: " + selectedId);
		} else { // amend contact		    
			try {
				contactBo.amendContact(selectedContact);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return SUCCESS;
	}

	public List<ContactVO> getListContact() {
		return listContact;
	}

	public void setListContact(List<ContactVO> listContact) {
		this.listContact = listContact;
	}

	public boolean isIncludeInactive() {
		return includeInactive;
	}

	public void setIncludeInactive(boolean includeInactive) {
		this.includeInactive = includeInactive;
	}

	public int getSelectedId() {
		return selectedId;
	}

	public void setSelectedId(int selectedId) {
		this.selectedId = selectedId;
	}

	public ContactVO getSelectedContact() {
		return selectedContact;
	}

	public void setSelectedContact(ContactVO selectedContact) {
		this.selectedContact = selectedContact;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		contactBO = new ContactBOImpl();
		contactTypeBO = new ContactTypeBOImpl();
		bestContactMethodBO = new BestContactMethodBOImpl();
	}

	public ContactBO getContactBO() {
		return contactBO;
	}

	public void setContactBO(ContactBO contactBO) {
		this.contactBO = contactBO;
	}

	public ContactTypeBO getContactTypeBO() {
		return contactTypeBO;
	}

	public void setContactTypeBO(ContactTypeBO contactTypeBO) {
		this.contactTypeBO = contactTypeBO;
	}

	public BestContactMethodBO getBestContactMethodBO() {
		return bestContactMethodBO;
	}

	public void setBestContactMethodBO(BestContactMethodBO bestContactMethodBO) {
		this.bestContactMethodBO = bestContactMethodBO;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public String getFilterStr() {
		return filterStr;
	}

	public void setFilterStr(String filterStr) {
		this.filterStr = filterStr;
	}


    public List<ContactTypeVO> getListContactType() {
        return listContactType;
    }

    public void setListContactType(List<ContactTypeVO> listContactType) {
        this.listContactType = listContactType;
    }

    public List<BestContactMethodVO> getListBestContactMethod() {
        return listBestContactMethod;
    }

    public void setListBestContactMethod(
            List<BestContactMethodVO> listBestContactMethod) {
        this.listBestContactMethod = listBestContactMethod;
    }

    public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

    public String getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(String selectedType) {
        this.selectedType = selectedType;
    }

    public String getSelectedBestMethod() {
        return selectedBestMethod;
    }

    public void setSelectedBestMethod(String selectedBestMethod) {
        this.selectedBestMethod = selectedBestMethod;
    }
    
    
	
	

}
