package mock.appcode.web.action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.Preparable;

import mock.appcode.business.DirectorateBO;
import mock.appcode.business.DirectorateBOImpl;
import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.Constant;
import mock.appcode.common.valueobjects.DirectorateVO;

public class DirectorateAction extends ActionSupport implements Preparable,
		SessionAware {
	private List<DirectorateVO> listDirectorate;
	private DirectorateBO directorateBo;
	private static final long serialVersionUID = 1L;
	private String filterStr;
	private boolean includeInactive;
	private int selectedId;
	private DirectorateVO selectedDirectorate;
	private Map<String, Object> session;
	private List<String> countryName;
	static String FILTER_LIST = "filter_list_directorate";

	/*
	 * load all directorate
	 */
	public String doList() {
		try {
			if (listDirectorate == null) {
				listDirectorate = directorateBo.listAllDirectorate();
			}
		} catch (DataAccessException ex) {

		} catch (FunctionalException ex) {

		} catch (ClassNotFoundException ex) {

		} catch (SQLException ex) {

		}
		return SUCCESS;
	}

	/**
	 * do filter
	 */
	public String doFilter() {
		String option = "";
		System.out.println("Filter: " + filterStr);
		if (filterStr.equals("all")) {
			option = DirectorateBOImpl.OPTION_ALL;
		} else if (filterStr.equals("0-9")) {
			option = DirectorateBOImpl.OPTION_0_9;
		} else if (filterStr.equals("A-E")) {
			option = DirectorateBOImpl.OPTION_A_E;
		} else if (filterStr.equals("F-J")) {
			option = DirectorateBOImpl.OPTION_F_J;
		} else if (filterStr.equals("K-N")) {
			option = DirectorateBOImpl.OPTION_K_N;
		} else if (filterStr.equals("O-R")) {
			option = DirectorateBOImpl.OPTION_O_R;
		} else if (filterStr.equals("S-V")) {
			option = DirectorateBOImpl.OPTION_S_V;
		} else if (filterStr.equals("W-Z")) {
			option = DirectorateBOImpl.OPTION_W_Z;
		}
		try {
			listDirectorate = directorateBo.listAllDirectorateWithOption(option);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FunctionalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (listDirectorate == null) {
			listDirectorate = new ArrayList<DirectorateVO>();
		} else {
			includeInactive = true;
			session.put(FILTER_LIST, listDirectorate);
		}
		return SUCCESS;
	}

	public String doShowInactive() {
		List<DirectorateVO> listTmp = (List<DirectorateVO>) session.get(FILTER_LIST);
		if (listTmp != null) {
			listDirectorate = new ArrayList<DirectorateVO>();
			listDirectorate.addAll(listTmp);
		}
		if (listDirectorate == null) {
			try {
				listDirectorate = directorateBo.listAllDirectorateIncludeInactive(includeInactive);
			} catch (DataAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FunctionalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			if (!includeInactive) {
				for (int i = 0; i < listDirectorate.size(); i++) {
					DirectorateVO directorate = listDirectorate.get(i);
					if (!directorate.getIsActive()) {
						listDirectorate.remove(i);
						i--;
					}
				}
			}
			System.out.println("list after: " + listDirectorate.size());
		}
		return SUCCESS;
	}

	public String showDirectorateDetail() {
		try {
			selectedDirectorate = directorateBo.findDirectorateByID(selectedId);
			countryName = directorateBo.returnAllNation();
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FunctionalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String createNewDirectorate() {
		try {
			boolean b = false;
			if (selectedDirectorate.getId() > 0) {
				b = directorateBo.updateDirectorate(selectedDirectorate);
			} else {
				b = directorateBo.createNewDirectorate(selectedDirectorate);
			}
			if (!b) {
				return ERROR;
			}
		} catch (DataValidationException e) {
			// TODO Auto-generated catch block
			if (e.getMessage().equals(Constant.POSTCODE_ERROR)) {
				addActionError(Constant.POSTCODE_ERROR);
				return ERROR;
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			addActionError(e.toString());
			return ERROR;
		} catch (FunctionalException e) {
			// TODO Auto-generated catch block
			addActionError(e.toString());
			return ERROR;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			addActionError(e.toString());
			return ERROR;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			addActionError(e.toString());
			return ERROR;
		}
		return SUCCESS;
	}

	public String inactiveDirectorate() {
		try {
			directorateBo.inActiveDirectorateWithID(selectedDirectorate.getId());
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			addActionError(e.toString());
			return ERROR;
		} catch (FunctionalException e) {
			// TODO Auto-generated catch block
			addActionError(e.toString());
			return ERROR;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			addActionError(e.toString());
			return ERROR;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			addActionError(e.toString());
			return ERROR;
		}
		return SUCCESS;
	}

	public String activeDirectorate() {
		try {
			directorateBo.activeDirectorateWithID(selectedId);
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			addActionError(e.toString());
			return ERROR;
		} catch (FunctionalException e) {
			// TODO Auto-generated catch block
			addActionError(e.toString());
			return ERROR;
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			addActionError(e.toString());
			return ERROR;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			addActionError(e.toString());
			return ERROR;
		}
		return SUCCESS;
	}

	public String showCreatePage() {
		try {
			countryName = directorateBo.returnAllNation();
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FunctionalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return SUCCESS;
	}

	public String copyAddressesFromDepartment() {
		return SUCCESS;
	}

	public List<DirectorateVO> getListDirectorate() {
		return listDirectorate;
	}

	public void setListDirectorate(List<DirectorateVO> listDirectorate) {
		this.listDirectorate = listDirectorate;
	}

	public DirectorateBO getDirectorateBo() {
		return directorateBo;
	}

	public void setDirectorateBo(DirectorateBO directorateBo) {
		this.directorateBo = directorateBo;
	}

	public String getFilterStr() {
		return filterStr;
	}

	public void setFilterStr(String filterStr) {
		this.filterStr = filterStr;
	}

	public boolean isIncludeInactive() {
		return includeInactive;
	}

	public void setIncludeInactive(boolean includeInactive) {
		this.includeInactive = includeInactive;
	}

	public int getSelectedId() {
		return selectedId;
	}

	public void setSelectedId(int selectedId) {
		this.selectedId = selectedId;
	}

	public DirectorateVO getSelectedDirectorate() {
		return selectedDirectorate;
	}

	public void setSelectedDirectorate(DirectorateVO selectedDirectorate) {
		this.selectedDirectorate = selectedDirectorate;
	}

	public List<String> getCountryName() {
		return countryName;
	}

	public void setCountryName(List<String> countryName) {
		this.countryName = countryName;
	}

	public Map<String, Object> getSession() {
		return session;
	}

	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		directorateBo = new DirectorateBOImpl();
		countryName = directorateBo.returnAllNation();
	}

}
