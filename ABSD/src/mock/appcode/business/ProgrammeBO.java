package mock.appcode.business;

import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.ProgrammeVO;

public interface ProgrammeBO {
	public ArrayList<ProgrammeVO> listAllRegion() throws DataAccessException, FunctionalException, ClassNotFoundException, SQLException;
	public ArrayList<ProgrammeVO> listAllRegionIncludeInactive(boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;
	public ArrayList<ProgrammeVO> listAllRegionWithOption(
            String option) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;
	public void setInactive(int id);
}
