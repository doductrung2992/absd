package mock.appcode.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.TeamVO;

public interface TeamBO {
    public ArrayList<TeamVO> listAllTeam() throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public ArrayList<TeamVO> listAllTeamWithOption(String option)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<TeamVO> listAllTeamIncludeInactive(boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public TeamVO findTeamByID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public boolean createNewTeam(TeamVO d) throws DataAccessException,
            FunctionalException, ClassNotFoundException,
            DataValidationException, SQLException;

    public boolean updateTeam(TeamVO d) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException,
            DataValidationException;

    public void inActiveTeamWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public String[] copyAddressFromDepartmentWithTeamID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public String[] copyAddressFromOrganisationWithTeamID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public void activeTeamWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public List<String> returnAllNation() throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

}
