package mock.appcode.business;

import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.ProgrammeVO;
import mock.appcode.dao.daointerface.ProgrammeDAO;
import mock.appcode.dao.daointerface.ProgrammeDAOImpl;

public class ProgrammeBOImpl {
	public static String OPTION_ALL = "All";
    public static String OPTION_0_9 = "0_9";
    public static String OPTION_A_E = "A_E";
    public static String OPTION_F_J = "F_J";
    public static String OPTION_K_N = "K_N";
    public static String OPTION_O_R = "O_R";
    public static String OPTION_S_V = "S_V";
    public static String OPTION_W_Z = "W_Z";

    public ArrayList<ProgrammeVO> listAllProgramme()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ProgrammeDAOImpl dao = new ProgrammeDAOImpl();
        ArrayList<ProgrammeVO> list = dao.listAllProgramme();
        return list;
    }

    public ArrayList<ProgrammeVO> listAllProgrammeWithOption(
            String option) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ProgrammeDAOImpl dao = new ProgrammeDAOImpl();
        ArrayList<ProgrammeVO> list = new ArrayList<ProgrammeVO>();
        ArrayList<ProgrammeVO> listTmp = null;
        if (option == OPTION_ALL) {
            list = dao.listAllProgramme();
            return list;
        }
        char charBegin = '0';
        char charEnd = '9';
        if (option == OPTION_0_9) {
            charBegin = '0';
            charEnd = '9';
        } else if (option == OPTION_A_E) {
            charBegin = 'A';
            charEnd = 'E';
        } else if (option == OPTION_F_J) {
            charBegin = 'F';
            charEnd = 'J';
        } else if (option == OPTION_K_N) {
            charBegin = 'K';
            charEnd = 'N';
        } else if (option == OPTION_O_R) {
            charBegin = 'O';
            charEnd = 'R';
        } else if (option == OPTION_S_V) {
            charBegin = 'S';
            charEnd = 'V';
        } else if (option == OPTION_W_Z) {
            charBegin = 'W';
            charEnd = 'Z';
        }
        for (char c = charBegin; c <= charEnd; c++) {
            System.out.println(c + "");
            listTmp = dao.listAllProgrammeByFirstCharacter(c + "");
            list.addAll(listTmp);
        }
        return list;
    }

    public ArrayList<ProgrammeVO> listAllProgrammeIncludeInactive(
            boolean b) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ProgrammeDAOImpl dao = new ProgrammeDAOImpl();
        ArrayList<ProgrammeVO> list = dao.listAllProgrammeIncludeInactive(b);
        return list;
    }

    public ProgrammeVO findProgrammeById(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ProgrammeDAOImpl dao = new ProgrammeDAOImpl();
        ProgrammeVO programme = dao.findProgrammeByID(id);
        return programme;
    }

    public void setInactive(int id) throws ClassNotFoundException, SQLException{
    	ProgrammeDAO dao = new ProgrammeDAOImpl();
    	dao.changeActiveStatus(id, 0);
    }
        
    
    public void addProgramme(ProgrammeVO prog) throws ClassNotFoundException, SQLException{
    	ProgrammeDAOImpl dao = new ProgrammeDAOImpl();
    	dao.AddProgramme(prog);
    }
    
    public void amendProgramme(ProgrammeVO prog) throws ClassNotFoundException, SQLException{
    	ProgrammeDAOImpl dao = new ProgrammeDAOImpl();
    	dao.updateProgramme(prog);
    }
    
    public void saveProgrammeDetail(ProgrammeVO prog) throws ClassNotFoundException, SQLException{
    	ProgrammeDAOImpl dao = new ProgrammeDAOImpl();
    	dao.saveProgrammeDetail(prog);
    }
}
