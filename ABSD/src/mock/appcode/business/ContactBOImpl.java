package mock.appcode.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.ContactVO;
import mock.appcode.dao.daointerface.ContactDAO;
import mock.appcode.dao.daointerface.ContactDAOImpl;

public class ContactBOImpl implements ContactBO{
    public ArrayList<ContactVO> listAllContactIncludeInactive(
            boolean b) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ContactDAO dao = new ContactDAOImpl();
        ArrayList<ContactVO> list = dao.listAllContactIncludeInactive(b);
        return list;
    }

	@Override
	public ArrayList<ContactVO> listAllContact() throws DataAccessException,
			FunctionalException, ClassNotFoundException, SQLException {
		ContactDAO dao = new ContactDAOImpl();
        ArrayList<ContactVO> list = dao.listAllContact();
        return list;
	}

	@Override
	public ContactVO findContactById(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ContactDAO dao = new ContactDAOImpl();
        ContactVO contact = dao.findContactById(id);
        return contact;
    }

	@Override
	public ArrayList<ContactVO> findContactByName(String firstName,
			String surName) throws ClassNotFoundException, SQLException {
		ContactDAO dao = new ContactDAOImpl();
		ArrayList<ContactVO> list = dao.findContactByName(firstName, surName);
		return list;
	}

	@Override
	public void setInactive(int id) throws ClassNotFoundException, SQLException {
		ContactDAO dao = new ContactDAOImpl();
    	dao.changeActiveStatus(id, 0);
	}

	@Override
	public List<ContactVO> listAllContactWithOption(String option) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean addNewContact(ContactVO contact) throws DataAccessException, FunctionalException, ClassNotFoundException, SQLException {
		ContactDAO dao = new ContactDAOImpl();
		return dao.addNewContact(contact);
	}

	@Override
	public boolean amendContact(ContactVO contact) throws ClassNotFoundException, SQLException {
		ContactDAO dao = new ContactDAOImpl();
		return dao.amendContact(contact);
	}
	
	
}
