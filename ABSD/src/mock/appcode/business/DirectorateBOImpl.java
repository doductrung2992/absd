package mock.appcode.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.Constant;
import mock.appcode.common.valueobjects.DirectorateVO;
import mock.appcode.dao.daointerface.AddressDAO;
import mock.appcode.dao.daointerface.AddressDAOImpl;
import mock.appcode.dao.daointerface.CountryDAO;
import mock.appcode.dao.daointerface.CountryDAOImpl;
import mock.appcode.dao.daointerface.DirectorateDAO;
import mock.appcode.dao.daointerface.DirectorateDAOImpl;

public class DirectorateBOImpl implements DirectorateBO {
	public static String OPTION_ALL = "All";
    public static String OPTION_0_9 = "0_9";
    public static String OPTION_A_E = "A_E";
    public static String OPTION_F_J = "F_J";
    public static String OPTION_K_N = "K_N";
    public static String OPTION_O_R = "O_R";
    public static String OPTION_S_V = "S_V";
    public static String OPTION_W_Z = "W_Z";

    public ArrayList<DirectorateVO> listAllDirectorate()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DirectorateDAO dao = new DirectorateDAOImpl();
        ArrayList<DirectorateVO> list = dao.listAllDirectorate();
        return list;
    }

    public ArrayList<DirectorateVO> listAllDirectorateWithOption(
            String option) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DirectorateDAO dao = new DirectorateDAOImpl();
        ArrayList<DirectorateVO> list = new ArrayList<DirectorateVO>();
        ArrayList<DirectorateVO> listTmp = null;
        if (option == OPTION_ALL) {
            list = dao.listAllDirectorate();
            return list;
        }
        char charBegin = '0';
        char charEnd = '9';
        if (option == OPTION_0_9) {
            charBegin = '0';
            charEnd = '9';
        } else if (option == OPTION_A_E) {
            charBegin = 'A';
            charEnd = 'E';
        } else if (option == OPTION_F_J) {
            charBegin = 'F';
            charEnd = 'J';
        } else if (option == OPTION_K_N) {
            charBegin = 'K';
            charEnd = 'N';
        } else if (option == OPTION_O_R) {
            charBegin = 'O';
            charEnd = 'R';
        } else if (option == OPTION_S_V) {
            charBegin = 'S';
            charEnd = 'V';
        } else if (option == OPTION_W_Z) {
            charBegin = 'W';
            charEnd = 'Z';
        }
        for (char c = charBegin; c <= charEnd; c++) {
            System.out.println(c + "");
            listTmp = dao.listAllDirectorateByFirstCharacter(c + "");
            list.addAll(listTmp);
        }
        return list;
    }

    public ArrayList<DirectorateVO> listAllDirectorateIncludeInactive(
            boolean b) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DirectorateDAO dao = new DirectorateDAOImpl();
        ArrayList<DirectorateVO> list = dao
                .listAllDirectorateIncludeInactive(b);
        return list;
    }

    public DirectorateVO findDirectorateByID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DirectorateDAO dao = new DirectorateDAOImpl();
        DirectorateVO directorate = dao.findDirectorateByID(id);
        return directorate;
    }
    
    public boolean createNewDirectorate(DirectorateVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, DataValidationException,SQLException {
        AddressDAO aDAO = new AddressDAOImpl();
        boolean postcodeCheck = aDAO.checkPostcode(d.getPostcode());
        if(!postcodeCheck) {
            throw new DataValidationException(Constant.POSTCODE_ERROR);            
        }
        DirectorateDAO dao = new DirectorateDAOImpl();
        boolean b = dao.createNewDirectorate(d);
        return b;
    }
    
    public boolean updateDirectorate(DirectorateVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException, DataValidationException {
        AddressDAO aDAO = new AddressDAOImpl();
        boolean postcodeCheck = aDAO.checkPostcode(d.getPostcode());
        if(!postcodeCheck) {
            throw new DataValidationException(Constant.POSTCODE_ERROR);            
        }
        DirectorateDAO dao = new DirectorateDAOImpl();
        boolean b = dao.updateDirectorate(d);
        return b;
        
    }
    
    public void inActiveDirectorateWithID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DirectorateDAO dao = new DirectorateDAOImpl();
        dao.inActiveDirectorateWithID(id);
    }
    
    public void activeDirectorateWithID(int id) throws DataAccessException,
    FunctionalException, ClassNotFoundException, SQLException {
        DirectorateDAO dao = new DirectorateDAOImpl();
        dao.activeDirectorateWithID(id);
    }
       
    public String[] copyAddressFromOrganisationWithDirectorateID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DirectorateDAO dao = new DirectorateDAOImpl();
        String[] addresses = new String[3];
        addresses = dao.copyAddressFromOrganisationWithDirectorateID(id);
        return addresses;
    }
    
    public List<String> returnAllNation() throws DataAccessException,
    FunctionalException, ClassNotFoundException, SQLException {
        List<String> countryName = new ArrayList<String>();
        CountryDAO countryDAO = new CountryDAOImpl();
        countryName = countryDAO.returnAllNation();
        return countryName;
    }


}
