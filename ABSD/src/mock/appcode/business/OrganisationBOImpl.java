package mock.appcode.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.OrganisationVO;
import mock.appcode.dao.daointerface.CountryDAO;
import mock.appcode.dao.daointerface.CountryDAOImpl;
import mock.appcode.dao.daointerface.OrganisationDAO;
import mock.appcode.dao.daointerface.OrganisationDAOImpl;

public class OrganisationBOImpl implements OrganisationBO {
    public static String OPTION_ALL = "All";
    public static String OPTION_0_9 = "0_9";
    public static String OPTION_A_E = "A_E";
    public static String OPTION_F_J = "F_J";
    public static String OPTION_K_N = "K_N";
    public static String OPTION_O_R = "O_R";
    public static String OPTION_S_V = "S_V";
    public static String OPTION_W_Z = "W_Z";

    public ArrayList<OrganisationVO> listAllOrganisation()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        OrganisationDAOImpl dao = new OrganisationDAOImpl();
        ArrayList<OrganisationVO> list = dao.listAllOrganisation();
        return list;
    }

    public ArrayList<OrganisationVO> listAllOrganisationWithOption(String option)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        OrganisationDAO dao = new OrganisationDAOImpl();
        ArrayList<OrganisationVO> list = new ArrayList<OrganisationVO>();
        ArrayList<OrganisationVO> listTmp = null;
        if (option == OPTION_ALL) {
            list = dao.listAllOrganisation();
            return list;
        }
        char charBegin = '0';
        char charEnd = '9';
        if (option == OPTION_0_9) {
            charBegin = '0';
            charEnd = '9';
        } else if (option == OPTION_A_E) {
            charBegin = 'A';
            charEnd = 'E';
        } else if (option == OPTION_F_J) {
            charBegin = 'F';
            charEnd = 'J';
        } else if (option == OPTION_K_N) {
            charBegin = 'K';
            charEnd = 'N';
        } else if (option == OPTION_O_R) {
            charBegin = 'O';
            charEnd = 'R';
        } else if (option == OPTION_S_V) {
            charBegin = 'S';
            charEnd = 'V';
        } else if (option == OPTION_W_Z) {
            charBegin = 'W';
            charEnd = 'Z';
        }
        for (char c = charBegin; c <= charEnd; c++) {
            System.out.println(c + "");
            listTmp = dao.listAllOrganisationByFirstCharacter(c + "");
            list.addAll(listTmp);
        }
        return list;
    }

    public ArrayList<OrganisationVO> listAllOrganisationIncludeInactive(
            boolean b) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        OrganisationDAOImpl dao = new OrganisationDAOImpl();
        ArrayList<OrganisationVO> list = dao
                .listAllOrganisationIncludeInactive(b);
        return list;
    }

    public OrganisationVO findOrganisationById(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        OrganisationDAOImpl dao = new OrganisationDAOImpl();
        OrganisationVO organisation = dao.findorganisationByID(id);
        return organisation;
    }

    public void setInactive(int id) throws ClassNotFoundException, SQLException {
        OrganisationDAOImpl dao = new OrganisationDAOImpl();
        dao.changeActiveStatus(id, 0);
    }

    public void addOrganisation(OrganisationVO prog)
            throws ClassNotFoundException, SQLException, DataAccessException,
            FunctionalException {
        OrganisationDAO dao = new OrganisationDAOImpl();
        dao.createNeworganisation(prog);
    }

    public void amendOrganisation(OrganisationVO prog)
            throws ClassNotFoundException, SQLException, DataAccessException,
            FunctionalException {
        OrganisationDAOImpl dao = new OrganisationDAOImpl();
        dao.updateorganisation(prog);
    }

    public void changeActiveStatus(int id, int status)
            throws ClassNotFoundException, SQLException {
        OrganisationDAO dao = new OrganisationDAOImpl();
        dao.changeActiveStatus(id, status);
    }

    public List<String> returnAllNation() throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        List<String> countryName = new ArrayList<String>();
        CountryDAO countryDAO = new CountryDAOImpl();
        countryName = countryDAO.returnAllNation();
        return countryName;
    }

}
