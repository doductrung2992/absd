package mock.appcode.business;

import java.sql.SQLException;

import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.valueobjects.UserVO;

public interface UserBO {
    public boolean checkLogin(UserVO user) throws ClassNotFoundException,
    SQLException, DataValidationException;
}
