package mock.appcode.business;

import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.GovernmentOfficeRegionVO;
import mock.appcode.dao.daointerface.GovernmentOfficeRegionDAOImpl;

public class GovernmentOfficeRegionBOImpl implements GovernmentOfficeRegionBO{
    public static String OPTION_ALL = "All";
    public static String OPTION_0_9 = "0_9";
    public static String OPTION_A_E = "A_E";
    public static String OPTION_F_J = "F_J";
    public static String OPTION_K_N = "K_N";
    public static String OPTION_O_R = "O_R";
    public static String OPTION_S_V = "S_V";
    public static String OPTION_W_Z = "W_Z";

    public ArrayList<GovernmentOfficeRegionVO> listAllRegion()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        GovernmentOfficeRegionDAOImpl dao = new GovernmentOfficeRegionDAOImpl();
        ArrayList<GovernmentOfficeRegionVO> list = dao.listAllRegion();
        return list;
    }

    public ArrayList<GovernmentOfficeRegionVO> listAllRegionWithOption(
            String option) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        GovernmentOfficeRegionDAOImpl dao = new GovernmentOfficeRegionDAOImpl();
        ArrayList<GovernmentOfficeRegionVO> list = new ArrayList<GovernmentOfficeRegionVO>();
        ArrayList<GovernmentOfficeRegionVO> listTmp = null;
        if (option == OPTION_ALL) {
            list = dao.listAllRegion();
            return list;
        }
        char charBegin = '0';
        char charEnd = '9';
        if (option == OPTION_0_9) {
            charBegin = '0';
            charEnd = '9';
        } else if (option == OPTION_A_E) {
            charBegin = 'A';
            charEnd = 'E';
        } else if (option == OPTION_F_J) {
            charBegin = 'F';
            charEnd = 'J';
        } else if (option == OPTION_K_N) {
            charBegin = 'K';
            charEnd = 'N';
        } else if (option == OPTION_O_R) {
            charBegin = 'O';
            charEnd = 'R';
        } else if (option == OPTION_S_V) {
            charBegin = 'S';
            charEnd = 'V';
        } else if (option == OPTION_W_Z) {
            charBegin = 'W';
            charEnd = 'Z';
        }
        for (char c = charBegin; c <= charEnd; c++) {
            System.out.println(c + "");
            listTmp = dao.listAllRegionByFirstCharacter(c + "");
            list.addAll(listTmp);
        }
        return list;
    }

    public ArrayList<GovernmentOfficeRegionVO> listAllRegionIncludeInactive(
            boolean b) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        GovernmentOfficeRegionDAOImpl dao = new GovernmentOfficeRegionDAOImpl();
        ArrayList<GovernmentOfficeRegionVO> list = dao
                .listAllRegionIncludeInactive(b);
        return list;
    }

    public GovernmentOfficeRegionVO findRegionById(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        GovernmentOfficeRegionDAOImpl dao = new GovernmentOfficeRegionDAOImpl();
        GovernmentOfficeRegionVO region = dao.findRegionByID(id);
        return region;
    }

}
