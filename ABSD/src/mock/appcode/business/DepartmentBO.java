package mock.appcode.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.DepartmentVO;
import mock.appcode.common.valueobjects.GovernmentOfficeRegionVO;

public interface DepartmentBO {
    public ArrayList<DepartmentVO> listAllDepartment(int dirID)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<DepartmentVO> listAllDepartmentWithOption(int dirID,
            String option) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<DepartmentVO> listAllDepartmentIncludeInactive(int dirID, boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public DepartmentVO findDepartmentByID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public boolean createNewDepartment(int dirID, DepartmentVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, DataValidationException,SQLException;

    public boolean updateDepartment(DepartmentVO d) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException,
            DataValidationException;

    public void inActiveDepartmentWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public String[] copyAddressFromDirectorateWithDepartmentID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public String[] copyAddressFromOrganisationWithDepartmentID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public void activeDepartmentWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public List<String> returnAllNation() throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;
}
