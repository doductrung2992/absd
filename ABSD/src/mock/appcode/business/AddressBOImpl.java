package mock.appcode.business;

import java.sql.SQLException;
import java.util.Vector;

import mock.appcode.common.valueobjects.AddressVO;
import mock.appcode.dao.daointerface.AddressDAOImpl;

public class AddressBOImpl implements AddressBO {
	public Vector<AddressVO> findAddressByOption(String postcode, String street,
			String town) throws ClassNotFoundException, SQLException {
		AddressDAOImpl ada = new AddressDAOImpl();
		return ada.getAddressByOption("" + postcode, street, town);
	}
}
