package mock.appcode.business;

import java.sql.SQLException;
import java.util.List;

import mock.appcode.common.valueobjects.BestContactMethodVO;

public interface BestContactMethodBO {
	public List<BestContactMethodVO> listBestContactMethod() throws ClassNotFoundException, SQLException;
}
