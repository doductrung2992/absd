package mock.appcode.business;

import java.sql.SQLException;
import java.util.List;

import mock.appcode.common.valueobjects.ContactTypeVO;

public interface ContactTypeBO {
	public List<ContactTypeVO> listContactType() throws ClassNotFoundException, SQLException;
}
