package mock.appcode.business;

import java.sql.SQLException;

import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.valueobjects.UserVO;
import mock.appcode.dao.daointerface.UserDAOImpl;

public class UserBOImpl implements UserBO {
    public boolean checkLogin(UserVO user) throws ClassNotFoundException,
            SQLException, DataValidationException {
        return new UserDAOImpl().checkLogin(user);
    }
}
