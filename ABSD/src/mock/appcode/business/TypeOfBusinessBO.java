package mock.appcode.business;

import java.sql.SQLException;
import java.util.Vector;

import mock.appcode.common.valueobjects.TypeOfBusinessVO;

public interface TypeOfBusinessBO {
	public Vector<TypeOfBusinessVO> searchTypeOfBusinessByOption(String name,
			String siccode) throws ClassNotFoundException, SQLException;
}
