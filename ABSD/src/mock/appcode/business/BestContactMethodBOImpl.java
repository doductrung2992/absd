package mock.appcode.business;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import mock.appcode.common.valueobjects.BestContactMethodVO;
import mock.appcode.dao.daointerface.BestContactMethodDAO;
import mock.appcode.dao.daointerface.BestContactMethodDAOImpl;

public class BestContactMethodBOImpl implements BestContactMethodBO {

	@Override
	public List<BestContactMethodVO> listBestContactMethod()
			throws ClassNotFoundException, SQLException {
		BestContactMethodDAO dao = new BestContactMethodDAOImpl();
		List<BestContactMethodVO> map = dao.listBestContactMethod();
		return map;
	}

}
