package mock.appcode.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.Constant;
import mock.appcode.common.valueobjects.DepartmentVO;
import mock.appcode.dao.daointerface.AddressDAO;
import mock.appcode.dao.daointerface.AddressDAOImpl;
import mock.appcode.dao.daointerface.CountryDAO;
import mock.appcode.dao.daointerface.CountryDAOImpl;
import mock.appcode.dao.daointerface.CountyDAO;
import mock.appcode.dao.daointerface.CountyDAOImpl;
import mock.appcode.dao.daointerface.DepartmentDAO;
import mock.appcode.dao.daointerface.DepartmentDAOImpl;

public class DepartmentBOImpl implements DepartmentBO{
    public static String OPTION_ALL = "All";
    public static String OPTION_0_9 = "0_9";
    public static String OPTION_A_E = "A_E";
    public static String OPTION_F_J = "F_J";
    public static String OPTION_K_N = "K_N";
    public static String OPTION_O_R = "O_R";
    public static String OPTION_S_V = "S_V";
    public static String OPTION_W_Z = "W_Z";

    public ArrayList<DepartmentVO> listAllDepartment(int dirID)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DepartmentDAO dao = new DepartmentDAOImpl();
        ArrayList<DepartmentVO> list = dao.listAllDepartment(dirID);
        return list;
    }

    public ArrayList<DepartmentVO> listAllDepartmentWithOption(int dirID,
            String option) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DepartmentDAO dao = new DepartmentDAOImpl();
        ArrayList<DepartmentVO> list = new ArrayList<DepartmentVO>();
        ArrayList<DepartmentVO> listTmp = null;
        if (option == OPTION_ALL) {
            list = dao.listAllDepartment(dirID);
            return list;
        }
        char charBegin = '0';
        char charEnd = '9';
        if (option == OPTION_0_9) {
            charBegin = '0';
            charEnd = '9';
        } else if (option == OPTION_A_E) {
            charBegin = 'A';
            charEnd = 'E';
        } else if (option == OPTION_F_J) {
            charBegin = 'F';
            charEnd = 'J';
        } else if (option == OPTION_K_N) {
            charBegin = 'K';
            charEnd = 'N';
        } else if (option == OPTION_O_R) {
            charBegin = 'O';
            charEnd = 'R';
        } else if (option == OPTION_S_V) {
            charBegin = 'S';
            charEnd = 'V';
        } else if (option == OPTION_W_Z) {
            charBegin = 'W';
            charEnd = 'Z';
        }
        for (char c = charBegin; c <= charEnd; c++) {
            System.out.println(c + "");
            listTmp = dao.listAllDepartmentByFirstCharacter(dirID,c + "");
            list.addAll(listTmp);
        }
        return list;
    }

    public ArrayList<DepartmentVO> listAllDepartmentIncludeInactive(int dirID, boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DepartmentDAO dao = new DepartmentDAOImpl();
        ArrayList<DepartmentVO> list = dao
                .listAllDepartmentIncludeInactive(dirID,b);
        return list;
    }

    public DepartmentVO findDepartmentByID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DepartmentDAO dao = new DepartmentDAOImpl();
        DepartmentVO department = dao.findDepartmentByID(id);
        return department;
    }
    
    public boolean createNewDepartment(int dirID, DepartmentVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, DataValidationException,SQLException {
        AddressDAO aDAO = new AddressDAOImpl();
        boolean postcodeCheck = aDAO.checkPostcode(d.getPostcode());
        if(!postcodeCheck) {
            throw new DataValidationException(Constant.POSTCODE_ERROR);            
        }
        CountyDAO countyDAO = new CountyDAOImpl(); 
        int countyID = countyDAO.checkCountyNameForId(d.getCounty().getName());
        if(countyID == 0) {
            throw new DataValidationException(Constant.POSTCODE_ERROR);
        }
        d.getCounty().setId(countyID);
        DepartmentDAO dao = new DepartmentDAOImpl();
        boolean b = dao.createNewDepartment(dirID,d);
        return b;
    }
    
    public boolean updateDepartment(DepartmentVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException, DataValidationException {
        AddressDAO aDAO = new AddressDAOImpl();
        boolean postcodeCheck = aDAO.checkPostcode(d.getPostcode());
        if(!postcodeCheck) {
            throw new DataValidationException(Constant.POSTCODE_ERROR);            
        }
        DepartmentDAO dao = new DepartmentDAOImpl();
        boolean b = dao.updateDepartment(d);
        return b;
        
    }
    
    public void inActiveDepartmentWithID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DepartmentDAO dao = new DepartmentDAOImpl();
        dao.inActiveDepartmentWithID(id);
    }
    
    public void activeDepartmentWithID(int id) throws DataAccessException,
    FunctionalException, ClassNotFoundException, SQLException {
        DepartmentDAO dao = new DepartmentDAOImpl();
        dao.activeDepartmentWithID(id);
    }
    
    public String[] copyAddressFromDirectorateWithDepartmentID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DepartmentDAO dao = new DepartmentDAOImpl();
        String[] addresses = new String[3];
        addresses = dao.copyAddressFromDirectorateWithDepartmentID(id);
        return addresses;
        
    }
    
    public String[] copyAddressFromOrganisationWithDepartmentID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        DepartmentDAO dao = new DepartmentDAOImpl();
        String[] addresses = new String[3];
        addresses = dao.copyAddressFromOrganisationWithDepartmentID(id);
        return addresses;
    }
    
    public List<String> returnAllNation() throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        List<String> countryName = new ArrayList<String>();
        CountryDAO countryDAO = new CountryDAOImpl();
        countryName = countryDAO.returnAllNation();
        return countryName;
    }
}
