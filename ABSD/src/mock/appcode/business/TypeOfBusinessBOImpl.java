package mock.appcode.business;

import java.sql.SQLException;
import java.util.Vector;

import mock.appcode.common.valueobjects.TypeOfBusinessVO;
import mock.appcode.dao.daointerface.TypeOfBusinessDAOImplm;

public class TypeOfBusinessBOImpl implements TypeOfBusinessBO {
	public Vector<TypeOfBusinessVO> searchTypeOfBusinessByOption(String name,
			String siccode) throws ClassNotFoundException, SQLException {
		return new TypeOfBusinessDAOImplm().searchTypeOfBusinessByOption(name,
				siccode);
	}
}
