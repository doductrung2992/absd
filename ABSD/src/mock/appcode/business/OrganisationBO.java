package mock.appcode.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.OrganisationVO;
import mock.appcode.common.valueobjects.ProgrammeVO;
import mock.appcode.dao.daointerface.CountryDAO;
import mock.appcode.dao.daointerface.CountryDAOImpl;

public interface OrganisationBO {
    public ArrayList<OrganisationVO> listAllOrganisation()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<OrganisationVO> listAllOrganisationWithOption(String option)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<OrganisationVO> listAllOrganisationIncludeInactive(
            boolean b) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public OrganisationVO findOrganisationById(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public void setInactive(int id) throws ClassNotFoundException, SQLException;

    public void changeActiveStatus(int id, int status)
            throws ClassNotFoundException, SQLException;

    public List<String> returnAllNation() throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;
}
