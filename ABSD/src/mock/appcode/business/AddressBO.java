package mock.appcode.business;

import java.sql.SQLException;
import java.util.Vector;

import mock.appcode.common.valueobjects.AddressVO;

public interface AddressBO {
	public Vector<AddressVO> findAddressByOption(String postcode, String street, String town) throws ClassNotFoundException, SQLException;
}
