package mock.appcode.business;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import mock.appcode.common.valueobjects.ContactTypeVO;
import mock.appcode.dao.daointerface.ContactTypeDAO;
import mock.appcode.dao.daointerface.ContactTypeDAOImpl;

public class ContactTypeBOImpl implements ContactTypeBO {

	@Override
	public List<ContactTypeVO> listContactType() throws ClassNotFoundException,
			SQLException {
		ContactTypeDAO dao = new ContactTypeDAOImpl();
		List<ContactTypeVO> map = dao.listContactType();
		return map;
	}

}
