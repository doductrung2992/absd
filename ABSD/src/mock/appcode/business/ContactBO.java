package mock.appcode.business;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.ContactVO;

public interface ContactBO {
	public ArrayList<ContactVO> listAllContact() throws DataAccessException,
			FunctionalException, ClassNotFoundException, SQLException;

	public ArrayList<ContactVO> listAllContactIncludeInactive(boolean b)
			throws DataAccessException, FunctionalException,
			ClassNotFoundException, SQLException;
	
	public ContactVO findContactById(int id) throws DataAccessException,
			FunctionalException, ClassNotFoundException, SQLException;
	
	public ArrayList<ContactVO> findContactByName(String firstName, String surName) throws ClassNotFoundException, SQLException;

	public void setInactive(int selectedId) throws ClassNotFoundException, SQLException;

	public List<ContactVO> listAllContactWithOption(String option);
	
	public boolean addNewContact(ContactVO contact) throws DataAccessException, FunctionalException, ClassNotFoundException, SQLException;
	
	public boolean amendContact(ContactVO contact) throws ClassNotFoundException, SQLException;
}
