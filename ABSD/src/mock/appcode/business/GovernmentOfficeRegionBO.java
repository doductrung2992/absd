package mock.appcode.business;

import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.GovernmentOfficeRegionVO;

public interface GovernmentOfficeRegionBO {
	public ArrayList<GovernmentOfficeRegionVO> listAllRegion() throws DataAccessException, FunctionalException, ClassNotFoundException, SQLException;
	public ArrayList<GovernmentOfficeRegionVO> listAllRegionIncludeInactive(boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;
	public ArrayList<GovernmentOfficeRegionVO> listAllRegionWithOption(
            String option) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;
	 public GovernmentOfficeRegionVO findRegionById(int id)
	            throws DataAccessException, FunctionalException,
	            ClassNotFoundException, SQLException;
}
