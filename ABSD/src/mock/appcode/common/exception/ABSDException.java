package mock.appcode.common.exception;

public class ABSDException extends Exception {
	public ABSDException(String message) {
		super(message);
	}
	
	public ABSDException(String message, Throwable cause) {
		super(message, cause);
	}
	
}
