package mock.appcode.common.valueobjects;

import java.io.Serializable;

public class AddressVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private int postcode;
	private TownVO town;
	private String street;
	private CountryVO country;
	private CountyVO county;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPostcode() {
		return postcode;
	}

	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}

	public TownVO getTown() {
		return town;
	}

	public void setTown(TownVO town) {
		this.town = town;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public AddressVO() {
		super();
	}

	
	public AddressVO(int id, int postcode, TownVO town, String street,
			CountryVO country, CountyVO county) {
		super();
		this.id = id;
		this.postcode = postcode;
		this.town = town;
		this.street = street;
		this.country = country;
		this.county = county;
	}

	public CountryVO getCountry() {
		return country;
	}

	public void setCountry(CountryVO country) {
		this.country = country;
	}

	public CountyVO getCounty() {
		return county;
	}

	public void setCounty(CountyVO county) {
		this.county = county;
	}
	
}
