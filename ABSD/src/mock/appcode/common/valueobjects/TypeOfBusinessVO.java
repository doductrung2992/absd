package mock.appcode.common.valueobjects;

import java.io.Serializable;

/**
 * @author do duc trung
 * @version Aug 8, 2014
 */
public class TypeOfBusinessVO implements Serializable {

	/**
     * 
     */
	private static final long serialVersionUID = 1L;
	private String name;
	private int sicCode;
	private int id;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getSicCode() {
		return sicCode;
	}

	public void setSicCode(int sicCode) {
		this.sicCode = sicCode;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public TypeOfBusinessVO() {
		super();
	}

	public TypeOfBusinessVO(String name, int sicCode, int id) {
		super();
		this.name = name;
		this.sicCode = sicCode;
		this.id = id;
	}

	
	
}
