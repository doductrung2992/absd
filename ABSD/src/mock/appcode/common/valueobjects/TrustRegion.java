package mock.appcode.common.valueobjects;

import java.io.Serializable;
import java.util.List;

/**
 *@author do duc trung
 *@version Aug 8, 2014
 */
public class TrustRegion implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private int id;
    private String name;
    private String description;
    private List<TrustDistrict> listTrustDistrict;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public List<TrustDistrict> getListTrustDistrict() {
        return listTrustDistrict;
    }
    public void setListTrustDistrict(List<TrustDistrict> listTrustDistrict) {
        this.listTrustDistrict = listTrustDistrict;
    }
    public TrustRegion() {
        super();
    }
    
    

}
