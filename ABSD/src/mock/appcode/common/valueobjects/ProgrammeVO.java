package mock.appcode.common.valueobjects;

import java.io.Serializable;

public class ProgrammeVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private String description;
	private int isActive;
	private String isActiveStr;
	private int contactId;
	private String contactName;
	public ProgrammeVO(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getContactId() {
		return contactId;
	}

	public void setContactId(int contactId) {
		this.contactId = contactId;
	}

	public String getIsActiveStr() {
		return isActiveStr;
	}

	public void setIsActiveStr(String isActiveStr) {
		this.isActiveStr = isActiveStr;
	}
	
	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		 this.isActive = isActive;
	     this.isActiveStr = ((this.isActive == 1)?"Yes":"No");
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}	
	
	
}
