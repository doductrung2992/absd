package mock.appcode.common.valueobjects;

import java.io.Serializable;

public class ContactTypeVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String value;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public ContactTypeVO() {
		super();
		// TODO Auto-generated constructor stub
	}

}
