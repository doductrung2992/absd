package mock.appcode.common.valueobjects;

import java.io.Serializable;

/**
 *@author do duc trung
 *@version Aug 11, 2014
 */
public class UserVO implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private String account;
    private String password;
    private String email;
    private int role;
    public String getAccount() {
        return account;
    }
    public void setAccount(String account) {
        this.account = account;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public int getRole() {
        return role;
    }
    public void setRole(int role) {
        this.role = role;
    }
    public UserVO() {
        super();
    }
    
    
    
}
