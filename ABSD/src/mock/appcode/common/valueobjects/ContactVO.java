package mock.appcode.common.valueobjects;

public class ContactVO {
	int id;
	String firstName;
	String surname;
	String knownAs;
	String officePhone;
	String mobilePhone;
	String stHomePhone;
	String email;
	int managerID;
	String managerName;
	int contactTypeID;
	String contactTypeStr;
	int bestContactMethodID;
	String jobRole;
	String workBase;
	String jobTitle;
	boolean isActive;
	String isActiveStr;
	String fullName;
		
	public ContactVO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getKnownAs() {
		return knownAs;
	}

	public void setKnownAs(String knownAs) {
		this.knownAs = knownAs;
	}

	public String getOfficePhone() {
		return officePhone;
	}

	public void setOfficePhone(String officePhone) {
		this.officePhone = officePhone;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getStHomePhone() {
		return stHomePhone;
	}

	public void setStHomePhone(String stHomePhone) {
		this.stHomePhone = stHomePhone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getManagerID() {
		return managerID;
	}

	public void setManagerID(int managerID) {
		this.managerID = managerID;
	}

	public String getManagerName() {
		return managerName;
	}

	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	public int getContactTypeID() {
		return contactTypeID;
	}

	public void setContactTypeID(int contactTypeID) {
		this.contactTypeID = contactTypeID;
	}

	public String getContactTypeStr() {
		return contactTypeStr;
	}

	public void setContactTypeStr(String contactTypeStr) {
		this.contactTypeStr = contactTypeStr;
	}

	public int getBestContactMethodID() {
		return bestContactMethodID;
	}

	public void setBestContactMethodID(int bestContactMethodID) {
		this.bestContactMethodID = bestContactMethodID;
	}

	public String getJobRole() {
		return jobRole;
	}

	public void setJobRole(String jobRole) {
		this.jobRole = jobRole;
	}

	public String getWorkBase() {
		return workBase;
	}

	public void setWorkBase(String workBase) {
		this.workBase = workBase;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}
	
	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
		this.isActiveStr = (isActive)? "Yes" : "No";
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getIsActiveStr() {
		return isActiveStr;
	}

	public void setIsActiveStr(String isActiveStr) {
		this.isActiveStr = isActiveStr;
	}
	
}
