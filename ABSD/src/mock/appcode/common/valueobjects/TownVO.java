package mock.appcode.common.valueobjects;

import java.io.Serializable;
import java.util.List;

/**
 *@author do duc trung
 *@version Aug 8, 2014
 */
public class TownVO implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private int id;
    private List<AddressVO> listAddress;
    private String name;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public List<AddressVO> getListAddress() {
        return listAddress;
    }
    public void setListAddress(List<AddressVO> listAddress) {
        this.listAddress = listAddress;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public TownVO() {
        super();
    }
	public TownVO(int id, List<AddressVO> listAddress, String name) {
		super();
		this.id = id;
		this.listAddress = listAddress;
		this.name = name;
	}
    
    
    
}
