package mock.appcode.common.valueobjects;

import java.io.Serializable;
import java.util.List;

public class CountryVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private List<CountyVO> listCounty;
	private List<TrustRegion> listTrustRegion;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<CountyVO> getListCounty() {
        return listCounty;
    }
    public void setListCounty(List<CountyVO> listCounty) {
        this.listCounty = listCounty;
    }
    public List<TrustRegion> getListTrustRegion() {
        return listTrustRegion;
    }
    public void setListTrustRegion(List<TrustRegion> listTrustRegion) {
        this.listTrustRegion = listTrustRegion;
    }
    public CountryVO() {
		super();
	}
	public CountryVO(int id, String name, List<CountyVO> listCounty,
			List<TrustRegion> listTrustRegion) {
		super();
		this.id = id;
		this.name = name;
		this.listCounty = listCounty;
		this.listTrustRegion = listTrustRegion;
	}
    
    
	

}
