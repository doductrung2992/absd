package mock.appcode.common.valueobjects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *@author do duc trung
 *@version Aug 5, 2014
 */
public class GovernmentOfficeRegionVO implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private int id;
    private String name;
    private String description;
    private int isActive;
    private String isActiveStr;
    private String countyName;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    
    
    public String getCountyName() {
		return countyName;
	}
	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}
	public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    
    public int getIsActive() {
        return isActive;
    }
    public void setIsActive(int isActive) {
        this.isActive = isActive;
        this.isActiveStr = (this.isActive == 1?"Yes":"No");
    }
    
    
   
    public String getIsActiveStr() {
        return isActiveStr;
    }
    public void setIsActiveStr(String isActiveStr) {
        this.isActiveStr = isActiveStr;
    }
    public GovernmentOfficeRegionVO() {
        super();
    }
    
    
    
}
