package mock.appcode.common.valueobjects;

import java.io.Serializable;
import java.util.List;

public class CountyVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private List<TownVO> listTown;
	private List<GovernmentOfficeRegionVO> listRegion;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public List<TownVO> getListTown() {
        return listTown;
    }
    public void setListTown(List<TownVO> listTown) {
        this.listTown = listTown;
    }
    
    public List<GovernmentOfficeRegionVO> getListRegion() {
        return listRegion;
    }
    public void setListRegion(List<GovernmentOfficeRegionVO> listRegion) {
        this.listRegion = listRegion;
    }
    public CountyVO() {
		super();
	}
	public CountyVO(int id, String name, List<TownVO> listTown,
			List<GovernmentOfficeRegionVO> listRegion) {
		super();
		this.id = id;
		this.name = name;
		this.listTown = listTown;
		this.listRegion = listRegion;
	}
	
    
    

}
