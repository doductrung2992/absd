package mock.appcode.common.utility;

import java.io.ObjectInputStream.GetField;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * @author do duc trung
 * @version Aug 5, 2014
 */
public class DBConnectionUtility {
    static String DBNAME = "IN.2014.LY.PTIT_G5_DB";
    static String USERNAME = "sa";
    static String PASSWORD = "hahaha";
    static String URL = "jdbc:sqlserver://localhost:1433; databaseName=";

    static DBConnectionUtility instance;

    public static DBConnectionUtility shareInstance() {
        if (instance == null) {
            instance = new DBConnectionUtility();
        }
        return instance;
    }

    public Connection getConnection() throws ClassNotFoundException,
            SQLException {
        Connection con = null;
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        con = DriverManager.getConnection(
                 URL + DBNAME,
                USERNAME, PASSWORD);
        System.out.println("Connected");
        return con;
    }
    public static void main(String[] args) {
		try {
			new DBConnectionUtility().getConnection();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
