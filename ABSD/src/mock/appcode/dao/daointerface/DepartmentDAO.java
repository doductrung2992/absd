package mock.appcode.dao.daointerface;

import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.DepartmentVO;
import mock.appcode.common.valueobjects.GovernmentOfficeRegionVO;

/**
 * @author do duc trung
 * @version Aug 5, 2014
 */
public interface DepartmentDAO {
    public ArrayList<DepartmentVO> listAllDepartment(int dirID)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<DepartmentVO> listAllDepartmentByFirstCharacter(int dirID,
            String s) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<DepartmentVO> listAllDepartmentIncludeInactive(int dirID,
            boolean b) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public DepartmentVO findDepartmentByID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public boolean createNewDepartment(int dirID, DepartmentVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public boolean updateDepartment(DepartmentVO d) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public void inActiveDepartmentWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public String[] copyAddressFromDirectorateWithDepartmentID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public String[] copyAddressFromOrganisationWithDepartmentID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public void activeDepartmentWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;
}
