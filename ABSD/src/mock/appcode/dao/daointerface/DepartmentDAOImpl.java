package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.DBConnectionUtility;
import mock.appcode.common.valueobjects.ContactVO;
import mock.appcode.common.valueobjects.CountyVO;
import mock.appcode.common.valueobjects.DepartmentVO;
import mock.appcode.common.valueobjects.TypeOfBusinessVO;

/**
 * @author do duc trung
 * @version Aug 5, 2014
 */
public class DepartmentDAOImpl implements DepartmentDAO {
    public ArrayList<DepartmentVO> listAllDepartment(int dirID)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<DepartmentVO> list = new ArrayList<DepartmentVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT DepartmentID" + ",DirectorateID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ",Name"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress" + ",CharityNumber" + ",CompanyNumber"
                + ",CountyID" + ",TownName" + ",d.IsActive" + ",d.Email"
                + " FROM dbo.Department AS d"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON d.ContactID = c.ContactID" 
                +" WHERE 1=1";
        if(dirID > 0) {
            sqlStr += " AND DirectorateID = "+dirID;
        }
        PreparedStatement pre = con.prepareStatement(sqlStr);
        ResultSet re = pre.executeQuery();
        DepartmentVO department = null;
        while (re.next()) {
            department = new DepartmentVO();
            department.setId(re.getInt("DepartmentID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            department.setContact(contact);
            department.setName(re.getString("Name"));
            department.setShortDescription(re.getString("ShortDescription"));
            department.setFullDescription(re.getString("FullDescription"));
            department.setAddressLine1(re.getString("AddressLine1"));
            department.setAddressLine2(re.getString("AddressLine2"));
            department.setAddressLine3(re.getString("AddressLine3"));
            department.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            department.setTypeOfBusiness(business);
            department.setPostcode(re.getInt("Postcode"));
            department.setPhoneNumber(re.getString("PhoneNumber"));
            department.setFax(re.getString("Fax"));
            department.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            department.setCounty(county);
            boolean b = (re.getInt("IsActive") == 1 ? true : false);
            department.setIsActive(b);
            department.setEmail(re.getString("Email"));
            department.setTown(re.getString("TownName"));
            list.add(department);
        }
        System.out.println("We got :" + list.size() + " departments");
        return list;
    }

    public ArrayList<DepartmentVO> listAllDepartmentByFirstCharacter(int dirID, String s)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<DepartmentVO> list = new ArrayList<DepartmentVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT DepartmentID" + ",DirectorateID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ",Name"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress" + ",CharityNumber" + ",CompanyNumber"
                + ",CountyID" + ",TownName" + ",d.IsActive" + ",d.Email"
                + " FROM dbo.Department AS d"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON d.ContactID = c.ContactID"
                + " WHERE d.Name LIKE ? OR d.Name LIKE ?";        
        if(dirID > 0) {
            sqlStr += " AND DirectorateID = "+dirID;
        }
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setString(1, "" + s + "%");
        pre.setString(2, "" + s.toLowerCase() + "%");
        ResultSet re = pre.executeQuery();
        DepartmentVO department = null;
        while (re.next()) {
            department = new DepartmentVO();
            department.setId(re.getInt("DepartmentID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            department.setContact(contact);
            department.setName(re.getString("Name"));
            department.setShortDescription(re.getString("ShortDescription"));
            department.setFullDescription(re.getString("FullDescription"));
            department.setAddressLine1(re.getString("AddressLine1"));
            department.setAddressLine2(re.getString("AddressLine2"));
            department.setAddressLine3(re.getString("AddressLine3"));
            department.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            department.setTypeOfBusiness(business);
            department.setPostcode(re.getInt("Postcode"));
            department.setPhoneNumber(re.getString("PhoneNumber"));
            department.setFax(re.getString("Fax"));
            department.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            department.setCounty(county);
            boolean b = (re.getInt("IsActive") == 1 ? true : false);
            department.setIsActive(b);
            department.setEmail(re.getString("Email"));
            department.setTown(re.getString("TownName"));
            list.add(department);
        }
        System.out.println("We got :" + list.size() + " departments ::");
        return list;
    }

    public ArrayList<DepartmentVO> listAllDepartmentIncludeInactive(int dirID, boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<DepartmentVO> list = new ArrayList<DepartmentVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT DepartmentID" + ",DirectorateID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ",Name"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress" + ",CharityNumber" + ",CompanyNumber"
                + ",CountyID" + ",TownName" + ",d.IsActive"
                + " FROM dbo.Department AS d"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON d.ContactID = c.ContactID"
                +" WHERE 1=1";
        if(dirID > 0) {
            sqlStr += " AND DirectorateID = "+dirID;
        }
        String appendStr = "WHERE IsActive = 1";
        if (!b) {
            sqlStr = sqlStr + appendStr;
        }
        PreparedStatement pre = con.prepareStatement(sqlStr);
        ResultSet re = pre.executeQuery();
        DepartmentVO department = null;
        while (re.next()) {
            department = new DepartmentVO();
            department.setId(re.getInt("DepartmentID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            department.setContact(contact);
            department.setName(re.getString("Name"));
            department.setShortDescription(re.getString("ShortDescription"));
            department.setFullDescription(re.getString("FullDescription"));
            department.setAddressLine1(re.getString("AddressLine1"));
            department.setAddressLine2(re.getString("AddressLine2"));
            department.setAddressLine3(re.getString("AddressLine3"));
            department.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            department.setTypeOfBusiness(business);
            department.setPostcode(re.getInt("Postcode"));
            department.setPhoneNumber(re.getString("PhoneNumber"));
            department.setFax(re.getString("Fax"));
            department.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            department.setCounty(county);
            boolean bo = (re.getInt("IsActive") == 1 ? true : false);
            department.setIsActive(bo);
            department.setEmail(re.getString("Email"));
            department.setTown(re.getString("TownName"));
            list.add(department);
        }
        System.out.println("We got :" + list.size() + " departments");
        return list;
    }

    public DepartmentVO findDepartmentByID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        DepartmentVO department = null;
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT DepartmentID" + ",DirectorateID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ",d.Name"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",d.BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress" + ",CharityNumber" + ",CompanyNumber"
                + ",CountyID" + ",TownName" + ",d.IsActive" + ",d.Email"
                +", b.Name AS BusinessTypeName, b.SIC"
                + " FROM dbo.Department AS d"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON d.ContactID = c.ContactID"
                + " LEFT JOIN dbo.BusinessType AS b"
                + " ON d.BusinessTypeID = b.BusinessTypeID"
                + " WHERE d.DepartmentID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            department = new DepartmentVO();
            department.setId(re.getInt("DepartmentID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            department.setContact(contact);
            department.setName(re.getString("Name"));
            department.setShortDescription(re.getString("ShortDescription"));
            department.setFullDescription(re.getString("FullDescription"));
            department.setAddressLine1(re.getString("AddressLine1"));
            department.setAddressLine2(re.getString("AddressLine2"));
            department.setAddressLine3(re.getString("AddressLine3"));
            department.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            business.setName(re.getString("BusinessTypeName"));
            business.setSicCode(re.getInt("SIC"));
            department.setTypeOfBusiness(business);
            department.setPostcode(re.getInt("Postcode"));
            department.setPhoneNumber(re.getString("PhoneNumber"));
            department.setFax(re.getString("Fax"));
            department.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            department.setCounty(county);
            boolean bo = (re.getInt("IsActive") == 1 ? true : false);
            department.setIsActive(bo);
            department.setEmail(re.getString("Email"));
            department.setTown(re.getString("TownName"));
        }
        return department;
    }

    public boolean createNewDepartment(int dirID, DepartmentVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "INSERT INTO dbo.Department " + "(DirectorateID"
                + ",ContactID" + ",Name" + ",ShortDescription"
                + ",FullDescription" + ",AddressLine1" + ",AddressLine2"
                + ",AddressLine3" + ",CountryName" + ",BusinessTypeID"
                + ",Postcode" + ",PhoneNumber" + ",Fax" + ",WebAddress"
                + ",CountyID" + ",TownName" + ",IsActive,Email)" + " VALUES"
                + " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        // simulate directorate
        pre.setInt(1, dirID);
        // simulate contact
        pre.setInt(2, 1);
        pre.setString(3, d.getName());
        pre.setString(4, d.getShortDescription());
        pre.setString(5, d.getFullDescription());
        pre.setString(6, d.getAddressLine1());
        pre.setString(7, d.getAddressLine2());
        pre.setString(8, d.getAddressLine3());
        pre.setString(9, d.getCountryName());
        // simulate type of business
        pre.setInt(10, d.getTypeOfBusiness().getId());
        pre.setInt(11, d.getPostcode());
        pre.setString(12, d.getPhoneNumber());
        pre.setString(13, d.getFax());
        pre.setString(14, d.getWebAddress());              
        pre.setInt(15, d.getCounty().getId());
        pre.setString(16, d.getTown());
        int isActive = (d.getIsActive() ? 1 : 0);
        pre.setInt(17, isActive);
        pre.setString(18, d.getEmail());

        int reInt = pre.executeUpdate();
        if (reInt == -1) {
            return false;
        }
        return true;
    }

    public boolean updateDepartment(DepartmentVO d) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Department" + " SET ContactID=?"
                + ",Name=?" + ",ShortDescription=?" + ",FullDescription=?"
                + ",AddressLine1=?" + ",AddressLine2=?" + ",AddressLine3=?"
                + ",CountryName=?" + ",BusinessTypeID=?" + ",Postcode=?"
                + ",PhoneNumber=?" + ",Fax=?" + ",WebAddress=?" + ",CountyID=?"
                + ",TownName=?" + ",IsActive=?,Email=?"
                + " WHERE DepartmentID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        // simulate directorate
        // simulate contact
        pre.setInt(1, 1);
        pre.setString(2, d.getName());
        pre.setString(3, d.getShortDescription());
        pre.setString(4, d.getFullDescription());
        pre.setString(5, d.getAddressLine1());
        pre.setString(6, d.getAddressLine2());
        pre.setString(7, d.getAddressLine3());
        pre.setString(8, d.getCountryName());
        // simulate type of business
        pre.setInt(9, 1);
        pre.setInt(10, d.getPostcode());
        pre.setString(11, d.getPhoneNumber());
        pre.setString(12, d.getFax());
        pre.setString(13, d.getWebAddress());
        CountyDAO countyDAO = new CountyDAOImpl();
        int countyID = -1;
        countyID = countyDAO.checkCountyNameForId(d.getCounty().getName());
        pre.setInt(14, countyID);
        pre.setString(15, d.getTown());
        int isActive = (d.getIsActive() ? 1 : 0);
        pre.setInt(16, isActive);
        pre.setString(17, d.getEmail());
        pre.setInt(18, d.getId());
        int reInt = pre.executeUpdate();
        if (reInt == -1) {
            return false;
        }
        return true;
    }

    public void inActiveDepartmentWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Department" + " SET IsActive = 0"
                + " WHERE DepartmentID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        pre.executeUpdate();
    }

    public void activeDepartmentWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Department" + " SET IsActive = 1"
                + " WHERE DepartmentID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        pre.executeUpdate();
    }

    public String[] copyAddressFromDirectorateWithDepartmentID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        String[] addresses = new String[3];
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT di.AddressLine1, di.AddressLine2, di.AddressLine3"
                + " FROM dbo.Department AS de JOIN dbo.Directorate AS di"
                + " ON de.DirectorateID = di.DirectorateID"
                + " WHERE de.DepartmentID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            addresses[0] = re.getString("AddressLine1");
            addresses[1] = re.getString("AddressLine2");
            addresses[2] = re.getString("AddressLine3");
        }
        return addresses;

    }

    public String[] copyAddressFromOrganisationWithDepartmentID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        String[] addresses = new String[3];
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT o.AddressLine1, o.AddressLine2, o.AddressLine3"
                + " FROM dbo.Department AS de JOIN dbo.Directorate AS di"
                + " ON de.DirectorateID = di.DirectorateID"
                + " JOIN dbo.Organisation AS o" + " ON di.OrgID = o.OrgID"
                + " WHERE de.DepartmentID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            addresses[0] = re.getString("AddressLine1");
            addresses[1] = re.getString("AddressLine2");
            addresses[2] = re.getString("AddressLine3");
        }
        return addresses;

    }
}
