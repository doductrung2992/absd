package mock.appcode.dao.daointerface;

import java.sql.SQLException;
import java.util.List;

import mock.appcode.common.valueobjects.ContactTypeVO;

public interface ContactTypeDAO {
	public List<ContactTypeVO> listContactType() throws ClassNotFoundException, SQLException;
}
