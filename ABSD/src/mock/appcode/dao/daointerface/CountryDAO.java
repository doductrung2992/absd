package mock.appcode.dao.daointerface;

import java.sql.SQLException;
import java.util.List;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;

/**
 *@author do duc trung
 *@version Aug 11, 2014
 */
public interface CountryDAO {
    public List<String> returnAllNation() throws DataAccessException,
    FunctionalException, ClassNotFoundException, SQLException;
}
