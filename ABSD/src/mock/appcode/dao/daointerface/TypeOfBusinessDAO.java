package mock.appcode.dao.daointerface;

import java.sql.SQLException;
import java.util.Vector;

import mock.appcode.common.valueobjects.TypeOfBusinessVO;

public interface TypeOfBusinessDAO {
	public Vector<TypeOfBusinessVO> searchTypeOfBusinessByOption(String name,String siccode) throws ClassNotFoundException, SQLException;
}
