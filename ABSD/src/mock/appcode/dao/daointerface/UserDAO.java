package mock.appcode.dao.daointerface;

import java.sql.SQLException;

import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.valueobjects.UserVO;

public interface UserDAO {
    public boolean checkLogin(UserVO user) throws ClassNotFoundException,
            SQLException, DataValidationException;

}
