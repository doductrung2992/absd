package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.DBConnectionUtility;
import mock.appcode.common.valueobjects.ProgrammeVO;
import mock.appcode.common.valueobjects.ProgrammeVO;

public class ProgrammeDAOImpl implements ProgrammeDAO {

    public ArrayList<ProgrammeVO> listAllProgramme()
            throws ClassNotFoundException, SQLException {
        ArrayList<ProgrammeVO> arr = new ArrayList<ProgrammeVO>();
        ProgrammeVO prog = null;
        String sql = "SELECT p.ProgrammeID, p.Name, p.Description, p.ContactID, p.IsActive, c.FirstName "
                + "FROM dbo.Programme AS p "
                + "JOIN dbo.Contact AS c ON p.ContactID = c.ContactID";
        Connection conn = DBConnectionUtility.shareInstance().getConnection();
        PreparedStatement prstm = conn.prepareStatement(sql);
        ResultSet rs = prstm.executeQuery();
        while (rs.next()) {
            prog = new ProgrammeVO();
            prog.setId(rs.getInt("ProgrammeID"));
            prog.setName(rs.getString("Name"));
            prog.setDescription(rs.getString("Description"));
            prog.setIsActive(rs.getInt("IsActive"));
            prog.setContactId(rs.getInt("ContactID"));
            prog.setContactName(rs.getString("FirstName"));
            arr.add(prog);
        }
        conn.close();
        return arr;
    }

    public ArrayList<ProgrammeVO> listAllProgrammeByFirstCharacter(String s)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<ProgrammeVO> list = new ArrayList<ProgrammeVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT p.ProgrammeID, p.Name, p.Description, p.ContactID, p.IsActive, c.FirstName "
                + "FROM dbo.Programme AS p "
                + "JOIN dbo.Contact AS c ON p.ContactID = c.ContactID "
                + "WHERE p.Name LIKE ? OR p.Name LIKE ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setString(1, "" + s + "%");
        pre.setString(2, "" + s.toLowerCase() + "%");
        ResultSet rs = pre.executeQuery();
        ProgrammeVO prog = null;
        while (rs.next()) {
            prog = new ProgrammeVO();
            prog.setId(rs.getInt("ProgrammeID"));
            prog.setName(rs.getString("Name"));
            prog.setDescription(rs.getString("Description"));
            prog.setIsActive(rs.getInt("IsActive"));
            prog.setContactId(rs.getInt("ContactID"));
            prog.setContactName(rs.getString("FirstName"));
            list.add(prog);
        }
        System.out.println("We got :" + list.size() + " progs ::");
        return list;
    }

    public ArrayList<ProgrammeVO> listAllProgrammeIncludeInactive(boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<ProgrammeVO> list = new ArrayList<ProgrammeVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT p.ProgrammeID, p.Name, p.Description, p.ContactID, p.IsActive, c.FirstName "
                + "FROM dbo.Programme AS p "
                + "JOIN dbo.Contact AS c ON p.ContactID = c.ContactID ";
        String appendStr = "WHERE IsActive = 1";
        if (!b) {
            sqlStr = sqlStr + appendStr;
        }
        PreparedStatement pre = con.prepareStatement(sqlStr);
        ResultSet rs = pre.executeQuery();
        ProgrammeVO prog = null;
        while (rs.next()) {
            prog = new ProgrammeVO();
            prog.setId(rs.getInt("ProgrammeID"));
            prog.setName(rs.getString("Name"));
            prog.setDescription(rs.getString("Description"));
            prog.setIsActive(rs.getInt("IsActive"));
            prog.setContactId(rs.getInt("ContactID"));
            prog.setContactName(rs.getString("FirstName"));
            list.add(prog);
        }
        System.out.println("We got :" + list.size() + " progs");
        return list;
    }

    public ProgrammeVO findProgrammeByID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        ProgrammeVO prog = null;
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT p.ProgrammeID, p.Name, p.Description, p.ContactID, p.IsActive, c.FirstName "
                + "FROM dbo.Programme AS p "
                + "JOIN dbo.Contact AS c ON p.ContactID = c.ContactID "
                + "WHERE p.ProgrammeID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet rs = pre.executeQuery();
        if (rs.next()) {
            prog = new ProgrammeVO();
            prog.setId(rs.getInt("ProgrammeID"));
            prog.setName(rs.getString("Name"));
            prog.setDescription(rs.getString("Description"));
            prog.setIsActive(rs.getInt("IsActive"));
            prog.setContactId(rs.getInt("ContactID"));
            prog.setContactName(rs.getString("FirstName"));
        }
        return prog;
    }

    public void AddProgramme(ProgrammeVO prog) throws ClassNotFoundException,
            SQLException {
        String sql = "INSERT INTO dbo.Programme(Name, Description, IsActive, ContactID) "
                + "VALUES (?, ?, ?, ?)";
        Connection conn = DBConnectionUtility.shareInstance().getConnection();
        PreparedStatement prstm = conn.prepareStatement(sql);
        prstm.setString(1, prog.getName());
        prstm.setString(2, prog.getDescription());
        prstm.setInt(3, prog.getIsActive());
        prstm.setInt(4, prog.getContactId());
        prstm.execute();
        conn.close();
    }

    public void updateProgramme(ProgrammeVO prog)
            throws ClassNotFoundException, SQLException {
        String sql = "UPDATE dbo.Programme SET Name=?, Description=?, IsActive=?, ContactID=? WHERE ProgrammeID=?";
        Connection conn = DBConnectionUtility.shareInstance().getConnection();
        PreparedStatement prstm = conn.prepareStatement(sql);
        prstm.setString(1, prog.getName());
        prstm.setString(2, prog.getDescription());
        prstm.setInt(3, prog.getIsActive());
        prstm.setInt(4, prog.getContactId());
        prstm.setInt(5, prog.getId());
        prstm.execute();
        conn.close();
    }

    public void deleteProgramme(int id) throws ClassNotFoundException,
            SQLException {
        String sql = "DELETE FROM dbo.Programme WHERE ProgrammeID=?";
        Connection conn = DBConnectionUtility.shareInstance().getConnection();
        PreparedStatement prstm = conn.prepareStatement(sql);
        prstm.setInt(1, id);
        prstm.execute();
        conn.close();
    }

    public void changeActiveStatus(int id, int status)
            throws ClassNotFoundException, SQLException {
        String sql = "UPDATE dbo.Programme SET IsActive=? WHERE ProgrammeID=?";
        Connection conn = DBConnectionUtility.shareInstance().getConnection();
        PreparedStatement prstm = conn.prepareStatement(sql);
        System.out.println("PROGRAM ID: " + id);
        prstm.setInt(1, status);
        prstm.setInt(2, id);
        prstm.execute();
        conn.close();
    }

    public void saveProgrammeDetail(ProgrammeVO prog)
            throws ClassNotFoundException, SQLException {
        String sql = "call SP_InsertOrUpdateProgramme(?, ?, ?, ?, ?)";
        Connection conn = DBConnectionUtility.shareInstance().getConnection();
        PreparedStatement prstm = conn.prepareStatement(sql);
        prstm.setInt(5, prog.getId());
        prstm.setString(1, prog.getName());
        prstm.setString(2, prog.getDescription());
        prstm.setInt(4, prog.getContactId());
        prstm.setInt(3, prog.getIsActive());
        prstm.execute();
        conn.close();
    }
}
