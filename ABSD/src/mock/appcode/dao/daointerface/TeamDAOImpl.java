package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.DBConnectionUtility;
import mock.appcode.common.valueobjects.ContactVO;
import mock.appcode.common.valueobjects.CountyVO;
import mock.appcode.common.valueobjects.TeamVO;
import mock.appcode.common.valueobjects.TypeOfBusinessVO;

public class TeamDAOImpl implements TeamDAO{
	public ArrayList<TeamVO> listAllTeam()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<TeamVO> list = new ArrayList<TeamVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT TeamID" + ", DepartmentID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ",Name"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress" 
                + ",CountyID" + ",TownName" + ",t.IsActive" + ",t.Email"
                + " FROM dbo.Team AS t"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON t.ContactID = c.ContactID";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        ResultSet re = pre.executeQuery();
        TeamVO team = null;
        while (re.next()) {
            team = new TeamVO();
            team.setId(re.getInt("TeamID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            team.setContact(contact);
            team.setName(re.getString("Name"));
            team.setShortDescription(re.getString("ShortDescription"));
            team.setFullDescription(re.getString("FullDescription"));
            team.setAddressLine1(re.getString("AddressLine1"));
            team.setAddressLine2(re.getString("AddressLine2"));
            team.setAddressLine3(re.getString("AddressLine3"));
            team.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            team.setTypeOfBusiness(business);
            team.setPostcode(re.getInt("Postcode"));
            team.setPhoneNumber(re.getString("PhoneNumber"));
            team.setFax(re.getString("Fax"));
            team.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            team.setCounty(county);
            boolean b = (re.getInt("IsActive") == 1 ? true : false);
            team.setIsActive(b);
            team.setEmail(re.getString("Email"));
            team.setTownName(re.getString("TownName"));
            list.add(team);
        }
        System.out.println("We got :" + list.size() + " teams");
        return list;
    }

    public ArrayList<TeamVO> listAllTeamByFirstCharacter(String s)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<TeamVO> list = new ArrayList<TeamVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT TeamID" + ",DepartmentID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ",Name"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress"
                + ",CountyID" + ",TownName" + ",t.IsActive" + ",t.Email"
                + " FROM dbo.Team AS t"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON t.ContactID = c.ContactID"
                + " WHERE t.Name LIKE ? OR t.Name LIKE ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setString(1, "" + s + "%");
        pre.setString(2, "" + s.toLowerCase() + "%");
        ResultSet re = pre.executeQuery();
        TeamVO team = null;
        while (re.next()) {
            team = new TeamVO();
            team.setId(re.getInt("TeamID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            team.setContact(contact);
            team.setName(re.getString("Name"));
            team.setShortDescription(re.getString("ShortDescription"));
            team.setFullDescription(re.getString("FullDescription"));
            team.setAddressLine1(re.getString("AddressLine1"));
            team.setAddressLine2(re.getString("AddressLine2"));
            team.setAddressLine3(re.getString("AddressLine3"));
            team.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            team.setTypeOfBusiness(business);
            team.setPostcode(re.getInt("Postcode"));
            team.setPhoneNumber(re.getString("PhoneNumber"));
            team.setFax(re.getString("Fax"));
            team.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            team.setCounty(county);
            boolean b = (re.getInt("IsActive") == 1 ? true : false);
            team.setIsActive(b);
            team.setEmail(re.getString("Email"));
            team.setTownName(re.getString("TownName"));
            list.add(team);
        }
        System.out.println("We got :" + list.size() + " teams ::");
        return list;
    }

    public ArrayList<TeamVO> listAllTeamIncludeInactive(boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<TeamVO> list = new ArrayList<TeamVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT TeamID" + ",DepartmentID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ",Name"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress"
                + ",CountyID" + ",TownName" + ",t.IsActive" +",t.Email"
                + " FROM dbo.Team AS t"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON t.ContactID = c.ContactID";
        String appendStr = "WHERE IsActive = 1";
        if (!b) {
            sqlStr = sqlStr + appendStr;
        }
        PreparedStatement pre = con.prepareStatement(sqlStr);
        ResultSet re = pre.executeQuery();
        TeamVO team = null;
        while (re.next()) {
            team = new TeamVO();
            team.setId(re.getInt("TeamID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            team.setContact(contact);
            team.setName(re.getString("Name"));
            team.setShortDescription(re.getString("ShortDescription"));
            team.setFullDescription(re.getString("FullDescription"));
            team.setAddressLine1(re.getString("AddressLine1"));
            team.setAddressLine2(re.getString("AddressLine2"));
            team.setAddressLine3(re.getString("AddressLine3"));
            team.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            team.setTypeOfBusiness(business);
            team.setPostcode(re.getInt("Postcode"));
            team.setPhoneNumber(re.getString("PhoneNumber"));
            team.setFax(re.getString("Fax"));
            team.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            team.setCounty(county);
            boolean bo = (re.getInt("IsActive") == 1 ? true : false);
            team.setIsActive(bo);
            team.setEmail(re.getString("Email"));
            team.setTownName(re.getString("TownName"));
            list.add(team);
        }
        System.out.println("We got :" + list.size() + " teams");
        return list;
    }

    public TeamVO findTeamByID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        TeamVO team = null;
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT TeamID" + ",DepartmentID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ",t.Name"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",t.BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress"
                + ",CountyID" + ",TownName" + ",t.IsActive" + ",t.Email"
                +", b.Name AS BusinessTypeName, b.SIC"
                + " FROM dbo.Team AS t"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON t.ContactID = c.ContactID"
                + " LEFT JOIN dbo.BusinessType AS b"
                + " ON t.BusinessTypeID = b.BusinessTypeID"
                + " WHERE t.TeamID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            team = new TeamVO();
            team.setId(re.getInt("TeamID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            team.setContact(contact);
            team.setName(re.getString("Name"));
            team.setShortDescription(re.getString("ShortDescription"));
            team.setFullDescription(re.getString("FullDescription"));
            team.setAddressLine1(re.getString("AddressLine1"));
            team.setAddressLine2(re.getString("AddressLine2"));
            team.setAddressLine3(re.getString("AddressLine3"));
            team.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            business.setName(re.getString("BusinessTypeName"));
            business.setSicCode(re.getInt("SIC"));
            team.setTypeOfBusiness(business);
            team.setPostcode(re.getInt("Postcode"));
            team.setPhoneNumber(re.getString("PhoneNumber"));
            team.setFax(re.getString("Fax"));
            team.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            team.setCounty(county);
            boolean bo = (re.getInt("IsActive") == 1 ? true : false);
            team.setIsActive(bo);
            team.setEmail(re.getString("Email"));
            team.setTownName(re.getString("TownName"));
        }
        return team;
    }

    public boolean createNewTeam(TeamVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "INSERT INTO dbo.Team " + "(DepartmentID"
                + ",ContactID" + ",Name" + ",ShortDescription"
                + ",FullDescription" + ",AddressLine1" + ",AddressLine2"
                + ",AddressLine3" + ",CountryName" + ",BusinessTypeID"
                + ",Postcode" + ",PhoneNumber" + ",Fax" + ",WebAddress"
                + ",CountyID" + ",TownName" + ",IsActive, Email)" + " VALUES"
                + " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        // simulate department
        pre.setInt(1, 1);
        // simulate contact
        pre.setInt(2, 1);
        pre.setString(3, d.getName());
        pre.setString(4, d.getShortDescription());
        pre.setString(5, d.getFullDescription());
        pre.setString(6, d.getAddressLine1());
        pre.setString(7, d.getAddressLine2());
        pre.setString(8, d.getAddressLine3());
        pre.setString(9, d.getCountryName());
        // simulate type of business
        pre.setInt(10, 1);
        pre.setInt(11, d.getPostcode());
        pre.setString(12, d.getPhoneNumber());
        pre.setString(13, d.getFax());
        pre.setString(14, d.getWebAddress());
        CountyDAO countyDAO = new CountyDAOImpl();
        int countyID = -1;
        countyID = countyDAO.checkCountyNameForId(d.getCounty().getName());
        pre.setInt(15, countyID);
        pre.setString(16, d.getTownName());
        int isActive = (d.getIsActive() ? 1 : 0);
        pre.setInt(17, isActive);
        pre.setString(18, d.getEmail());

        int reInt = pre.executeUpdate();
        if (reInt == -1) {
            return false;
        }
        return true;
    }

    public boolean updateTeam(TeamVO d) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Team" + " SET ContactID=?"
                + ",Name=?" + ",ShortDescription=?" + ",FullDescription=?"
                + ",AddressLine1=?" + ",AddressLine2=?" + ",AddressLine3=?"
                + ",CountryName=?" + ",BusinessTypeID=?" + ",Postcode=?"
                + ",PhoneNumber=?" + ",Fax=?" + ",WebAddress=?" + ",CountyID=?"
                + ",TownName=?" + ",IsActive=?,Email=?"
                + " WHERE TeamID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        // simulate directorate
        // simulate contact
        pre.setInt(1, 1);
        pre.setString(2, d.getName());
        pre.setString(3, d.getShortDescription());
        pre.setString(4, d.getFullDescription());
        pre.setString(5, d.getAddressLine1());
        pre.setString(6, d.getAddressLine2());
        pre.setString(7, d.getAddressLine3());
        pre.setString(8, d.getCountryName());
        // simulate type of business
        pre.setInt(9, 1);
        pre.setInt(10, d.getPostcode());
        pre.setString(11, d.getPhoneNumber());
        pre.setString(12, d.getFax());
        pre.setString(13, d.getWebAddress());
        CountyDAO countyDAO = new CountyDAOImpl();
        int countyID = -1;
        countyID = countyDAO.checkCountyNameForId(d.getCounty().getName());
        pre.setInt(14, countyID);
        pre.setString(15, d.getTownName());
        int isActive = (d.getIsActive() ? 1 : 0);
        pre.setInt(16, isActive);
        pre.setString(17, d.getEmail());
        pre.setInt(18, d.getId());
        int reInt = pre.executeUpdate();
        if (reInt == -1) {
            return false;
        }
        return true;
    }

    public void inActiveTeamWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Team" + " SET IsActive = 0"
                + " WHERE TeamID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        pre.executeUpdate();
    }

    public void activeTeamWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Team" + " SET IsActive = 1"
                + " WHERE TeamID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        pre.executeUpdate();
    }

    public String[] copyAddressFromDepartmentWithTeamID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        String[] addresses = new String[3];
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT di.AddressLine1, di.AddressLine2, di.AddressLine3"
                + " FROM dbo.Team AS de JOIN dbo.Department AS di"
                + " ON de.DepartmentID = di.DepartmentID"
                + " WHERE de.TeamID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            addresses[0] = re.getString("AddressLine1");
            addresses[1] = re.getString("AddressLine2");
            addresses[2] = re.getString("AddressLine3");
        }
        return addresses;

    }

    public String[] copyAddressFromOrganisationWithTeamID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        String[] addresses = new String[3];
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT o.AddressLine1, o.AddressLine2, o.AddressLine3"
                + " FROM dbo.Team AS de JOIN dbo.Department AS di"
                + " ON de.DepartmentID = di.DepartmentID"
                + " JOIN dbo.Organisation AS o" + " ON di.OrgID = o.OrgID"
                + " WHERE de.TeamID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            addresses[0] = re.getString("AddressLine1");
            addresses[1] = re.getString("AddressLine2");
            addresses[2] = re.getString("AddressLine3");
        }
        return addresses;

    }

}
