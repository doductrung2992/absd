package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.DBConnectionUtility;

/**
 *@author do duc trung
 *@version Aug 11, 2014
 */
public class CountryDAOImpl implements CountryDAO{
    public List<String> returnAllNation() throws DataAccessException,
    FunctionalException, ClassNotFoundException, SQLException {
        List<String> countryName = new ArrayList<String>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sql = "SELECT CountryName FROM dbo.Country";
        PreparedStatement pre = con.prepareStatement(sql);        
        ResultSet re = pre.executeQuery();
        String name = "";
        while(re.next()) {
            name = re.getString("CountryName");
            countryName.add(name);
        }
        return countryName;
    }
}
