package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.DBConnectionUtility;
import mock.appcode.common.valueobjects.GovernmentOfficeRegionVO;

/**
 * @author do duc trung
 * @version Aug 5, 2014
 */
public class GovernmentOfficeRegionDAOImpl implements GovernmentOfficeRegionDAO{
    public ArrayList<GovernmentOfficeRegionVO> listAllRegion()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<GovernmentOfficeRegionVO> list = new ArrayList<GovernmentOfficeRegionVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT GovOfficeRegionID, c.CountyName"
                + ",Desctiption, Name, IsActive "
                + "FROM dbo.GovOfficeRegion AS g LEFT JOIN dbo.County AS c "
                + "ON g.CountyID = c.CountyID";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        ResultSet re = pre.executeQuery();
        GovernmentOfficeRegionVO region = null;
        while (re.next()) {
            region = new GovernmentOfficeRegionVO();
            region.setId(re.getInt("GovOfficeRegionID"));
            region.setName(re.getString("Name"));
            region.setDescription(re.getString("Desctiption"));
            region.setIsActive(re.getInt("IsActive"));
            region.setCountyName(re.getString("CountyName"));
            list.add(region);
        }
        System.out.println("We got :" + list.size() + " regions");
        return list;
    }

    public ArrayList<GovernmentOfficeRegionVO> listAllRegionByFirstCharacter(
            String s) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<GovernmentOfficeRegionVO> list = new ArrayList<GovernmentOfficeRegionVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT GovOfficeRegionID, c.CountyName"
                + ",Desctiption, Name, IsActive "
                + "FROM dbo.GovOfficeRegion AS g LEFT JOIN dbo.County AS c "
                + "ON g.CountyID = c.CountyID "
                + "WHERE g.Name LIKE ? OR Name LIKE ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setString(1, "" + s + "%");
        pre.setString(2, "" + s.toLowerCase() + "%");
        ResultSet re = pre.executeQuery();
        GovernmentOfficeRegionVO region = null;
        while (re.next()) {
            region = new GovernmentOfficeRegionVO();
            region.setId(re.getInt("GovOfficeRegionID"));
            region.setName(re.getString("Name"));
            region.setDescription(re.getString("Desctiption"));
            region.setIsActive(re.getInt("IsActive"));
            region.setCountyName(re.getString("CountyName"));
            list.add(region);
        }
        System.out.println("We got :" + list.size() + " regions ::");
        return list;
    }

    public ArrayList<GovernmentOfficeRegionVO> listAllRegionIncludeInactive(
            boolean b) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<GovernmentOfficeRegionVO> list = new ArrayList<GovernmentOfficeRegionVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT GovOfficeRegionID, c.CountyName"
                + ",Desctiption, Name, IsActive "
                + "FROM dbo.GovOfficeRegion AS g LEFT JOIN dbo.County AS c "
                + "ON g.CountyID = c.CountyID ";
        String appendStr = "WHERE IsActive = 1";
        if (!b) {
            sqlStr = sqlStr + appendStr;
        }
        PreparedStatement pre = con.prepareStatement(sqlStr);
        ResultSet re = pre.executeQuery();
        GovernmentOfficeRegionVO region = null;
        while (re.next()) {
            region = new GovernmentOfficeRegionVO();
            region.setId(re.getInt("GovOfficeRegionID"));
            region.setName(re.getString("Name"));
            region.setDescription(re.getString("Desctiption"));
            region.setIsActive(re.getInt("IsActive"));
            region.setCountyName(re.getString("CountyName"));
            list.add(region);
        }
        System.out.println("We got :" + list.size() + " regions");
        return list;
    }

    public GovernmentOfficeRegionVO findRegionByID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        GovernmentOfficeRegionVO region = null;
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT GovOfficeRegionID ,c.CountyName, Desctiption, "
                + "Name, IsActive "
                + "FROM dbo.GovOfficeRegion AS g "
                + "INNER JOIN dbo.County AS c "
                + "ON g.CountyID = c.CountyID "
                + "WHERE g.GovOfficeRegionID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            region = new GovernmentOfficeRegionVO();
            region.setId(re.getInt("GovOfficeRegionID"));
            region.setDescription(re.getString("Desctiption"));
            region.setName(re.getString("Name"));
            region.setIsActive(re.getInt("IsActive"));
            region.setCountyName(re.getString("CountyName"));
        }
        return region;
    }

}
