package mock.appcode.dao.daointerface;

import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.OrganisationVO;
import mock.appcode.common.valueobjects.GovernmentOfficeRegionVO;

/**
 * @author do duc trung
 * @version Aug 5, 2014
 */
public interface OrganisationDAO {
    public ArrayList<OrganisationVO> listAllOrganisation()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<OrganisationVO> listAllOrganisationByFirstCharacter(
            String s) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<OrganisationVO> listAllOrganisationIncludeInactive(
            boolean b) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public OrganisationVO findorganisationByID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public boolean createNeworganisation(OrganisationVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public boolean updateorganisation(OrganisationVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public void inActiveorganisationWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public String[] copyAddressFromOrganisationWithorganisationID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public void changeActiveStatus(int id, int status)
            throws ClassNotFoundException, SQLException;

    public void activeorganisationWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;
}
