package mock.appcode.dao.daointerface;

import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.ProgrammeVO;

public interface ProgrammeDAO {
    public ArrayList<ProgrammeVO> listAllProgramme()
            throws ClassNotFoundException, SQLException;

    public ArrayList<ProgrammeVO> listAllProgrammeByFirstCharacter(String s)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<ProgrammeVO> listAllProgrammeIncludeInactive(boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ProgrammeVO findProgrammeByID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public void AddProgramme(ProgrammeVO prog) throws ClassNotFoundException,
            SQLException;

    public void updateProgramme(ProgrammeVO prog)
            throws ClassNotFoundException, SQLException;

    public void deleteProgramme(int id) throws ClassNotFoundException,
            SQLException;

    public void changeActiveStatus(int id, int status)
            throws ClassNotFoundException, SQLException;

    public void saveProgrammeDetail(ProgrammeVO prog)
            throws ClassNotFoundException, SQLException;
}
