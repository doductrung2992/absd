
package mock.appcode.dao.daointerface;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import mock.appcode.common.valueobjects.BestContactMethodVO;

public interface BestContactMethodDAO {
	public List<BestContactMethodVO> listBestContactMethod() throws ClassNotFoundException, SQLException;
}
