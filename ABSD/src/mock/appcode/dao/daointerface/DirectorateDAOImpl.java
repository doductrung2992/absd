package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.DBConnectionUtility;
import mock.appcode.common.valueobjects.ContactVO;
import mock.appcode.common.valueobjects.CountyVO;
import mock.appcode.common.valueobjects.DirectorateVO;
import mock.appcode.common.valueobjects.TypeOfBusinessVO;

public class DirectorateDAOImpl implements DirectorateDAO {
	public ArrayList<DirectorateVO> listAllDirectorate()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<DirectorateVO> list = new ArrayList<DirectorateVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT DirectorateID" + ", OrgID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ",Name"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress" 
                + ",CountyID" + ",TownName" + ", d.IsActive" + ", d.Email"
                + " FROM dbo.Directorate AS d"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON d.ContactID = c.ContactID";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        ResultSet re = pre.executeQuery();
        DirectorateVO directorate = null;
        while (re.next()) {
            directorate = new DirectorateVO();
            directorate.setId(re.getInt("DirectorateID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            directorate.setContact(contact);
            directorate.setName(re.getString("Name"));
            directorate.setShortDescription(re.getString("ShortDescription"));
            directorate.setFullDescription(re.getString("FullDescription"));
            directorate.setAddressLine1(re.getString("AddressLine1"));
            directorate.setAddressLine2(re.getString("AddressLine2"));
            directorate.setAddressLine3(re.getString("AddressLine3"));
            directorate.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            directorate.setTypeOfBusiness(business);
            directorate.setPostcode(re.getInt("Postcode"));
            directorate.setPhoneNumber(re.getString("PhoneNumber"));
            directorate.setFax(re.getString("Fax"));
            directorate.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            directorate.setCounty(county);
            boolean b = (re.getInt("IsActive") == 1 ? true : false);
            directorate.setIsActive(b);
            directorate.setEmail(re.getString("Email"));
            directorate.setTownName(re.getString("TownName"));
            list.add(directorate);
        }
        System.out.println("We got :" + list.size() + " directorates");
        return list;
    }

    public ArrayList<DirectorateVO> listAllDirectorateByFirstCharacter(String s)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<DirectorateVO> list = new ArrayList<DirectorateVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT DirectorateID" + ",OrgID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ",Name"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress"
                + ",CountyID" + ",TownName" + ",d.IsActive" + ",d.Email"
                + " FROM dbo.Directorate AS d"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON d.ContactID = c.ContactID"
                + " WHERE d.Name LIKE ? OR d.Name LIKE ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setString(1, "" + s + "%");
        pre.setString(2, "" + s.toLowerCase() + "%");
        ResultSet re = pre.executeQuery();
        DirectorateVO directorate = null;
        while (re.next()) {
            directorate = new DirectorateVO();
            directorate.setId(re.getInt("DirectorateID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            directorate.setContact(contact);
            directorate.setName(re.getString("Name"));
            directorate.setShortDescription(re.getString("ShortDescription"));
            directorate.setFullDescription(re.getString("FullDescription"));
            directorate.setAddressLine1(re.getString("AddressLine1"));
            directorate.setAddressLine2(re.getString("AddressLine2"));
            directorate.setAddressLine3(re.getString("AddressLine3"));
            directorate.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            directorate.setTypeOfBusiness(business);
            directorate.setPostcode(re.getInt("Postcode"));
            directorate.setPhoneNumber(re.getString("PhoneNumber"));
            directorate.setFax(re.getString("Fax"));
            directorate.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            directorate.setCounty(county);
            boolean b = (re.getInt("IsActive") == 1 ? true : false);
            directorate.setIsActive(b);
            directorate.setEmail(re.getString("Email"));
            directorate.setTownName(re.getString("TownName"));
            list.add(directorate);
        }
        System.out.println("We got :" + list.size() + " directorates ::");
        return list;
    }

    public ArrayList<DirectorateVO> listAllDirectorateIncludeInactive(boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<DirectorateVO> list = new ArrayList<DirectorateVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT DirectorateID" + ",OrgID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ",Name"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress"
                + ",CountyID" + ",TownName" + ",d.IsActive" +",d.Email"
                + " FROM dbo.Directorate AS d"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON d.ContactID = c.ContactID";
        String appendStr = "WHERE IsActive = 1";
        if (!b) {
            sqlStr = sqlStr + appendStr;
        }
        PreparedStatement pre = con.prepareStatement(sqlStr);
        ResultSet re = pre.executeQuery();
        DirectorateVO directorate = null;
        while (re.next()) {
            directorate = new DirectorateVO();
            directorate.setId(re.getInt("DirectorateID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            directorate.setContact(contact);
            directorate.setName(re.getString("Name"));
            directorate.setShortDescription(re.getString("ShortDescription"));
            directorate.setFullDescription(re.getString("FullDescription"));
            directorate.setAddressLine1(re.getString("AddressLine1"));
            directorate.setAddressLine2(re.getString("AddressLine2"));
            directorate.setAddressLine3(re.getString("AddressLine3"));
            directorate.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            directorate.setTypeOfBusiness(business);
            directorate.setPostcode(re.getInt("Postcode"));
            directorate.setPhoneNumber(re.getString("PhoneNumber"));
            directorate.setFax(re.getString("Fax"));
            directorate.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            directorate.setCounty(county);
            boolean boo = (re.getInt("IsActive") == 1 ? true : false);
            directorate.setIsActive(boo);
            directorate.setEmail(re.getString("Email"));
            directorate.setTownName(re.getString("TownName"));
            list.add(directorate);
        }
        System.out.println("We got :" + list.size() + " directorates");
        return list;
    }

    public DirectorateVO findDirectorateByID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        DirectorateVO directorate = null;
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT DirectorateID" + ",OrgID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ", d.Name"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",d.BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress"
                + ",CountyID" + ",TownName" + ",d.IsActive" + ",d.Email"
                +", b.Name AS BusinessTypeName, b.SIC"
                + " FROM dbo.Directorate AS d"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON d.ContactID = c.ContactID"
                + " LEFT JOIN dbo.BusinessType AS b"
                + " ON d.BusinessTypeID = b.BusinessTypeID"
                + " WHERE d.DirectorateID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            directorate = new DirectorateVO();
            directorate.setId(re.getInt("DirectorateID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            directorate.setContact(contact);
            directorate.setName(re.getString("Name"));
            directorate.setShortDescription(re.getString("ShortDescription"));
            directorate.setFullDescription(re.getString("FullDescription"));
            directorate.setAddressLine1(re.getString("AddressLine1"));
            directorate.setAddressLine2(re.getString("AddressLine2"));
            directorate.setAddressLine3(re.getString("AddressLine3"));
            directorate.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            business.setName(re.getString("BusinessTypeName"));
            business.setSicCode(re.getInt("SIC"));
            directorate.setTypeOfBusiness(business);
            directorate.setPostcode(re.getInt("Postcode"));
            directorate.setPhoneNumber(re.getString("PhoneNumber"));
            directorate.setFax(re.getString("Fax"));
            directorate.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            directorate.setCounty(county);
            boolean b = (re.getInt("IsActive") == 1 ? true : false);
            directorate.setIsActive(b);
            directorate.setEmail(re.getString("Email"));
            directorate.setTownName(re.getString("TownName"));
        }
        return directorate;
    }

    public boolean createNewDirectorate(DirectorateVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "INSERT INTO dbo.Directorate " + "(OrgID"
                + ",ContactID" + ",Name" + ",ShortDescription"
                + ",FullDescription" + ",AddressLine1" + ",AddressLine2"
                + ",AddressLine3" + ",CountryName" + ",BusinessTypeID"
                + ",Postcode" + ",PhoneNumber" + ",Fax" + ",WebAddress"
                + ",CountyID" + ",TownName" + ",IsActive, Email)" + " VALUES"
                + " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        // simulate organization
        pre.setInt(1, 1);
        // simulate contact
        pre.setInt(2, 1);
        pre.setString(3, d.getName());
        pre.setString(4, d.getShortDescription());
        pre.setString(5, d.getFullDescription());
        pre.setString(6, d.getAddressLine1());
        pre.setString(7, d.getAddressLine2());
        pre.setString(8, d.getAddressLine3());
        pre.setString(9, d.getCountryName());
        // simulate type of business
        pre.setInt(10, 1);
        pre.setInt(11, d.getPostcode());
        pre.setString(12, d.getPhoneNumber());
        pre.setString(13, d.getFax());
        pre.setString(14, d.getWebAddress());
        CountyDAO countyDAO = new CountyDAOImpl();
        int countyID = -1;
        countyID = countyDAO.checkCountyNameForId(d.getCounty().getName());
        pre.setInt(15, countyID);
        pre.setString(16, d.getTownName());
        int isActive = (d.getIsActive() ? 1 : 0);
        pre.setInt(17, isActive);
        pre.setString(18, d.getEmail());

        int reInt = pre.executeUpdate();
        if (reInt == -1) {
            return false;
        }
        return true;
    }

    public boolean updateDirectorate(DirectorateVO d) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Directorate" + " SET ContactID=?"
                + ",Name=?" + ",ShortDescription=?" + ",FullDescription=?"
                + ",AddressLine1=?" + ",AddressLine2=?" + ",AddressLine3=?"
                + ",CountryName=?" + ",BusinessTypeID=?" + ",Postcode=?"
                + ",PhoneNumber=?" + ",Fax=?" + ",WebAddress=?" + ",CountyID=?"
                + ",TownName=?" + ",IsActive=?,Email=?"
                + " WHERE DirectorateID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        // simulate directorate
        // simulate contact
        pre.setInt(1, 1);
        pre.setString(2, d.getName());
        pre.setString(3, d.getShortDescription());
        pre.setString(4, d.getFullDescription());
        pre.setString(5, d.getAddressLine1());
        pre.setString(6, d.getAddressLine2());
        pre.setString(7, d.getAddressLine3());
        pre.setString(8, d.getCountryName());
        // simulate type of business
        pre.setInt(9, 1);
        pre.setInt(10, d.getPostcode());
        pre.setString(11, d.getPhoneNumber());
        pre.setString(12, d.getFax());
        pre.setString(13, d.getWebAddress());
        CountyDAO countyDAO = new CountyDAOImpl();
        int countyID = -1;
        countyID = countyDAO.checkCountyNameForId(d.getCounty().getName());
        pre.setInt(14, countyID);
        pre.setString(15, d.getTownName());
        int isActive = (d.getIsActive() ? 1 : 0);
        pre.setInt(16, isActive);
        pre.setString(17, d.getEmail());
        pre.setInt(18, d.getId());
        int reInt = pre.executeUpdate();
        if (reInt == -1) {
            return false;
        }
        return true;
    }

    public void inActiveDirectorateWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Directorate" + " SET IsActive = 0"
                + " WHERE DirectorateID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        pre.executeUpdate();
    }

    public void activeDirectorateWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Directorate" + " SET IsActive = 1"
                + " WHERE DirectorateID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        pre.executeUpdate();
    }

    public String[] copyAddressFromOrganizationWithDirectorateID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        String[] addresses = new String[3];
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT di.AddressLine1, di.AddressLine2, di.AddressLine3"
                + " FROM dbo.Directorate AS de JOIN dbo.Organization AS di"
                + " ON de.OrgID = di.OrgID"
                + " WHERE de.DirectorateID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            addresses[0] = re.getString("AddressLine1");
            addresses[1] = re.getString("AddressLine2");
            addresses[2] = re.getString("AddressLine3");
        }
        return addresses;

    }

    public String[] copyAddressFromOrganisationWithDirectorateID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        String[] addresses = new String[3];
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT o.AddressLine1, o.AddressLine2, o.AddressLine3"
                + " FROM dbo.Directorate AS de JOIN dbo.Organization AS di"
                + " ON de.OrgID = di.OrgID"
                + " JOIN dbo.Organisation AS o" + " ON di.OrgID = o.OrgID"
                + " WHERE de.DirectorateID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            addresses[0] = re.getString("AddressLine1");
            addresses[1] = re.getString("AddressLine2");
            addresses[2] = re.getString("AddressLine3");
        }
        return addresses;

    }

}
