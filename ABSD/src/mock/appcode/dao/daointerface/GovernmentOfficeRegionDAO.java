package mock.appcode.dao.daointerface;

import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.GovernmentOfficeRegionVO;

/**
 * @author do duc trung
 * @version Aug 5, 2014
 */
public interface GovernmentOfficeRegionDAO {
	public ArrayList<GovernmentOfficeRegionVO> listAllRegion()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;
	public ArrayList<GovernmentOfficeRegionVO> listAllRegionByFirstCharacter(
            String s) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;
	public ArrayList<GovernmentOfficeRegionVO> listAllRegionIncludeInactive(
            boolean b) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;
	public GovernmentOfficeRegionVO findRegionByID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;
}
