package mock.appcode.dao.daointerface;

import java.sql.SQLException;
import java.util.Vector;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.AddressVO;

/**
 *@author do duc trung
 *@version Aug 9, 2014
 */
public interface AddressDAO {
    public boolean checkPostcode(int postcode)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, DataValidationException,SQLException;
    
	public Vector<AddressVO> getAddressByOption(String postcode, String street,
			String town) throws ClassNotFoundException, SQLException;
}
