package mock.appcode.dao.daointerface;

import java.sql.SQLException;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;

/**
 *@author do duc trung
 *@version Aug 9, 2014
 */
public interface CountyDAO {
    public int checkCountyNameForId(String name) throws DataAccessException,
    FunctionalException, ClassNotFoundException, SQLException;
    public String returnCountyNameWithId(int id) throws DataAccessException,
    FunctionalException, ClassNotFoundException, SQLException;
}
