package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mock.appcode.common.utility.DBConnectionUtility;
import mock.appcode.common.valueobjects.ContactTypeVO;

public class ContactTypeDAOImpl implements ContactTypeDAO {

	@Override
	public List<ContactTypeVO> listContactType() throws ClassNotFoundException, SQLException {
	    List<ContactTypeVO> map = new ArrayList<ContactTypeVO>();
		Connection con = DBConnectionUtility.shareInstance().getConnection();
		String sql = "SELECT ContactTypeID, Value FROM dbo.ContactType";
		PreparedStatement prstm = con.prepareStatement(sql);
		ResultSet rs = prstm.executeQuery();
		ContactTypeVO contacType;
		while(rs.next()){
		    contacType = new ContactTypeVO();			
			contacType.setId(rs.getInt("ContactTypeID"));
			contacType.setValue(rs.getString("Value"));
			map.add(contacType);
		}
		return map;
	}

}
