package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.DBConnectionUtility;

/**
 * @author do duc trung
 * @version Aug 9, 2014
 */
public class CountyDAOImpl implements CountyDAO {
    public int checkCountyNameForId(String name) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        int countyId = -1;
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sql = "SELECT CountyID FROM dbo.County WHERE CountyName = ?";
        PreparedStatement pre = con.prepareStatement(sql);
        pre.setString(1, name);
        ResultSet re = pre.executeQuery();
        if(re.next()) {
            countyId = re.getInt("CountyID");            
        }
        return countyId;
    }
    
    public String returnCountyNameWithId(int id) throws DataAccessException,
    FunctionalException, ClassNotFoundException, SQLException {
        String name = null;
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sql = "SELECT CountyName FROM dbo.County WHERE CountyID = ?";
        PreparedStatement pre = con.prepareStatement(sql);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if(re.next()) {
             name = re.getString("CountyName");           
        }
        return name;
    }
}
