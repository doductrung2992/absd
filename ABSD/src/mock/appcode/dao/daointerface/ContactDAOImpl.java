package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.DBConnectionUtility;
import mock.appcode.common.valueobjects.ContactVO;

public class ContactDAOImpl implements ContactDAO {
    @Override
    public ArrayList<ContactVO> listAllContact() throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        ArrayList<ContactVO> list = new ArrayList<ContactVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();

        String sql = "SELECT c.ContactID, c.FirstName, c.SurName, c.KnownAs, c.OfficePhone, c.MobilePhone, c.STHomePhone, c.Email, "
                + " c.ManagerID, c.ContactTypeID, c2.Value AS ContactType, c.BestContactMethodID, c.JobRole, c.WorkBase, c.JobTitle, c.IsActive "
                + " FROM dbo.Contact AS c "
                + " LEFT JOIN dbo.ContactType AS c2 ON c.ContactTypeID = c2.ContactTypeID ";
        PreparedStatement pre = con.prepareStatement(sql);
        ResultSet re = pre.executeQuery();
        ContactVO contact;
        while (re.next()) {
            contact = new ContactVO();
            contact.setId(re.getInt("ContactID"));
            contact.setFirstName(re.getString("FirstName"));
            contact.setSurname(re.getString("SurName"));
            contact.setKnownAs(re.getString("KnownAs"));
            contact.setOfficePhone(re.getString("OfficePhone"));
            contact.setMobilePhone(re.getString("MobilePhone"));
            contact.setStHomePhone(re.getString("STHomePhone"));
            contact.setEmail(re.getString("Email"));
            contact.setManagerID(re.getInt("ManagerID"));
            contact.setContactTypeID(re.getInt("ContactTypeID"));
            contact.setContactTypeStr(re.getString("ContactType"));
            contact.setBestContactMethodID(re.getInt("BestContactMethodID"));
            contact.setJobRole(re.getString("JobRole"));
            contact.setWorkBase(re.getString("WorkBase"));
            contact.setJobTitle(re.getString("JobTitle"));
            boolean isActive = (re.getInt("IsActive") == 1) ? true : false;
            contact.setIsActive(isActive);
            contact.setFullName(contact.getFirstName() + " "
                    + contact.getSurname());
            list.add(contact);
        }
        System.out.println("We got :" + list.size() + " contact");
        return list;
    }

    @Override
    public ArrayList<ContactVO> listAllContactIncludeInactive(boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {

        ArrayList<ContactVO> list = new ArrayList<ContactVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();

        String sql = "SELECT c.ContactID, c.FirstName, c.SurName, c.KnownAs, c.OfficePhone, c.MobilePhone, c.STHomePhone, c.Email, "
                + "c.ManagerID, (c1.FirstName + ' ' + c1.SurName) AS ManagerName, c.ContactTypeID, c2.Value AS ContactType, c.BestContactMethodID, c.JobRole, c.WorkBase, c.JobTitle, c.IsActive "
                + "FROM dbo.Contact AS c "
                + "LEFT JOIN dbo.Contact AS c1 ON "
                + "LEFT JOIN dbo.ContactType AS c2 ON c.ContactTypeID = c2.ContactTypeID ";
        if (!b)
            sql += " WHERE c.IsActive = 1";
        PreparedStatement pre = con.prepareStatement(sql);
        ResultSet re = pre.executeQuery();
        ContactVO contact;
        while (re.next()) {
            contact = new ContactVO();
            contact.setId(re.getInt("ContactID"));
            contact.setFirstName(re.getString("FirstName"));
            contact.setSurname(re.getString("SurName"));
            contact.setKnownAs(re.getString("KnownAs"));
            contact.setOfficePhone(re.getString("OfficePhone"));
            contact.setMobilePhone(re.getString("MobilePhone"));
            contact.setStHomePhone(re.getString("STHomePhone"));
            contact.setEmail(re.getString("Email"));
            contact.setManagerID(re.getInt("ManagerID"));
            contact.setManagerName(re.getString("ManagerName"));
            contact.setContactTypeID(re.getInt("ContactTypeID"));
            contact.setContactTypeStr(re.getString("ContactType"));
            contact.setBestContactMethodID(re.getInt("BestContactMethodID"));
            contact.setJobRole(re.getString("JobRole"));
            contact.setWorkBase(re.getString("WorkBase"));
            contact.setJobTitle(re.getString("JobTitle"));
            boolean isActive = (re.getInt("IsActive") == 1) ? true : false;
            contact.setIsActive(isActive);
            contact.setFullName(contact.getFirstName() + " "
                    + contact.getSurname());
            list.add(contact);
        }
        System.out.println("We got :" + list.size() + " contact");
        return list;
    }

    @Override
    public ContactVO findContactById(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();

        String sql = "SELECT c.ContactID, c.FirstName, c.SurName, c.KnownAs, c.OfficePhone, c.MobilePhone, c.STHomePhone, c.Email, "
                + " c.ManagerID, (c1.FirstName + ' ' + c1.SurName) AS ManagerName, c.ContactTypeID, c2.Value AS ContactType, c.BestContactMethodID, c.JobRole, c.WorkBase, c.JobTitle, c.IsActive "
                + " FROM dbo.Contact AS c "
                + " LEFT JOIN dbo.Contact AS c1 ON c.ManagerID = c1.ContactID"
                + " LEFT JOIN dbo.ContactType AS c2 ON c.ContactTypeID = c2.ContactTypeID "
                + " WHERE c.ContactID = ?";

        PreparedStatement pre = con.prepareStatement(sql);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        ContactVO contact = null;
        if (re.next()) {
            contact = new ContactVO();
            contact.setId(re.getInt("ContactID"));
            contact.setFirstName(re.getString("FirstName"));
            contact.setSurname(re.getString("SurName"));
            contact.setKnownAs(re.getString("KnownAs"));
            contact.setOfficePhone(re.getString("OfficePhone"));
            contact.setMobilePhone(re.getString("MobilePhone"));
            contact.setStHomePhone(re.getString("STHomePhone"));
            contact.setEmail(re.getString("Email"));
            contact.setManagerID(re.getInt("ManagerID"));
            contact.setManagerName(re.getString("ManagerName"));
            contact.setContactTypeID(re.getInt("ContactTypeID"));
            contact.setContactTypeStr(re.getString("ContactType"));
            contact.setBestContactMethodID(re.getInt("BestContactMethodID"));
            contact.setJobRole(re.getString("JobRole"));
            contact.setWorkBase(re.getString("WorkBase"));
            contact.setJobTitle(re.getString("JobTitle"));
            boolean isActive = (re.getInt("IsActive") == 1) ? true : false;
            contact.setIsActive(isActive);
            contact.setFullName(contact.getFirstName() + " "
                    + contact.getSurname());
        }

        return contact;
    }

    @Override
    public boolean addNewContact(ContactVO c) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "INSERT INTO dbo.Contact (FirstName, ManagerID, SurName, KnownAs, OfficePhone, MobilePhone, STHomePhone, Email, JobRole, WorkBase, JobTitle, IsActive, ContactTypeID, BestContactMethodID)"
                + " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setString(1, c.getFirstName());
        pre.setInt(2, c.getManagerID());
        pre.setString(3, c.getSurname());
        pre.setString(4, c.getKnownAs());
        pre.setString(5, c.getOfficePhone());
        pre.setString(6, c.getMobilePhone());
        pre.setString(7, c.getStHomePhone());
        pre.setString(8, c.getEmail());
        pre.setString(9, c.getJobRole());
        pre.setString(10, c.getWorkBase());
        pre.setString(11, c.getJobTitle());
        int isActive = (c.getIsActive() ? 1 : 0);
        pre.setInt(12, isActive);
        pre.setInt(13, c.getContactTypeID());
        pre.setInt(14, c.getBestContactMethodID());
        int reInt = pre.executeUpdate();
        if (reInt == -1) {
            return false;
        }
        return true;
    }

    @Override
    public ArrayList<ContactVO> findContactByName(String firstName,
            String surName) throws ClassNotFoundException, SQLException {
        ArrayList<ContactVO> list = new ArrayList<ContactVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();

        String sql = "SELECT c.ContactID, c.FirstName, c.SurName, c.KnownAs, c.OfficePhone, c.MobilePhone, c.STHomePhone, c.Email, "
                + "c.ManagerID, (c1.FirstName + ' ' + c1.SurName) AS ManagerName, c.ContactTypeID, c2.Value AS ContactType, c.BestContactMethodID, c.JobRole, c.WorkBase, c.JobTitle, c.IsActive "
                + "FROM dbo.Contact AS c "
                + "LEFT JOIN dbo.Contact AS c1 ON "
                + "LEFT JOIN dbo.ContactType AS c2 ON c.ContactTypeID = c2.ContactTypeID "
                + "WHERE 1=1";

        if (firstName.length() > 0) {
            sql += " AND FirstName LIKE " + "'%" + firstName + "%'";
        }

        if (surName.length() > 0) {
            sql += " AND SurName LIKE " + "'%" + surName + "%'";
        }
        Statement pre = con.createStatement();
        ResultSet re = pre.executeQuery(sql);

        ContactVO contact;
        while (re.next()) {
            contact = new ContactVO();
            contact.setId(re.getInt("ContactID"));
            contact.setFirstName(re.getString("FirstName"));
            contact.setSurname(re.getString("SurName"));
            contact.setKnownAs(re.getString("KnownAs"));
            contact.setOfficePhone(re.getString("OfficePhone"));
            contact.setMobilePhone(re.getString("MobilePhone"));
            contact.setStHomePhone(re.getString("STHomePhone"));
            contact.setEmail(re.getString("Email"));
            contact.setManagerID(re.getInt("ManagerID"));
            contact.setManagerName(re.getString("ManagerName"));
            contact.setContactTypeID(re.getInt("ContactTypeID"));
            contact.setContactTypeStr(re.getString("ContactType"));
            contact.setBestContactMethodID(re.getInt("BestContactMethodID"));
            contact.setJobRole(re.getString("JobRole"));
            contact.setWorkBase(re.getString("WorkBase"));
            contact.setJobTitle(re.getString("JobTitle"));
            boolean isActive = (re.getInt("IsActive") == 1) ? true : false;
            contact.setIsActive(isActive);
            contact.setFullName(contact.getFirstName() + " "
                    + contact.getSurname());
            list.add(contact);

        }
        System.out.println("GOT: " + list.size());
        return list;
    }

    @Override
    public void changeActiveStatus(int id, int status)
            throws ClassNotFoundException, SQLException {
        String sql = "UPDATE dbo.Contact SET IsActive=? WHERE ContactID=?";
        Connection conn = DBConnectionUtility.shareInstance().getConnection();
        PreparedStatement prstm = conn.prepareStatement(sql);
        System.out.println("CONTACT ID: " + id);
        prstm.setInt(1, status);
        prstm.setInt(2, id);
        prstm.execute();
        conn.close();
    }

    @Override
    public boolean amendContact(ContactVO c) throws ClassNotFoundException,
            SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Contact SET FirstName = ?, ManagerID = ?, SurName = ?, KnownAs = ?, OfficePhone = ?, "
                + "MobilePhone = ?, STHomePhone = ?, Email = ?, JobRole = ?, WorkBase = ?, JobTitle = ?, "
                + "IsActive = ?, ContactTypeID = ?, BestContactMethodID = ? "
                + "WHERE ContactID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setString(1, c.getFirstName());
        pre.setInt(2, c.getManagerID());
        pre.setString(3, c.getSurname());
        pre.setString(4, c.getKnownAs());
        pre.setString(5, c.getOfficePhone());
        pre.setString(6, c.getMobilePhone());
        pre.setString(7, c.getStHomePhone());
        pre.setString(8, c.getEmail());
        pre.setString(9, c.getJobRole());
        pre.setString(10, c.getWorkBase());
        pre.setString(11, c.getJobTitle());
        int isActive = (c.getIsActive() ? 1 : 0);
        pre.setInt(12, isActive);
        pre.setInt(13, c.getContactTypeID());
        pre.setInt(14, c.getBestContactMethodID());
        pre.setInt(15, c.getId());
        int reInt = pre.executeUpdate();
        if (reInt == -1) {
            return false;
        }
        return true;
    }

}
