package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import mock.appcode.common.utility.DBConnectionUtility;
import mock.appcode.common.valueobjects.TypeOfBusinessVO;

public class TypeOfBusinessDAOImplm implements TypeOfBusinessDAO{

	@Override
	public Vector<TypeOfBusinessVO> searchTypeOfBusinessByOption(String name,String siccode) throws ClassNotFoundException, SQLException {
		String sql = "Select * from dbo.BusinessType where 1=1 ";
		
		if(name!=null && name.length()>0){
			sql = sql+" AND Name like '%"+name+"%' ";
		}
		
		if(siccode!=null && siccode.length()>0){
			sql = sql+" AND SIC like '%"+siccode+"%' ";
		}

		Connection con = DBConnectionUtility.shareInstance().getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sql);

		Vector<TypeOfBusinessVO> res = new Vector<TypeOfBusinessVO>();

		while (rs.next()) {
			TypeOfBusinessVO tob  = new TypeOfBusinessVO(rs.getString("Name"), rs.getInt("SIC"), rs.getInt("BusinessTypeID"));
			res.add(tob);
		}
		return res;
		
	}
	
}
