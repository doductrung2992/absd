package mock.appcode.dao.daointerface;

import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.TeamVO;

public interface TeamDAO {
	public ArrayList<TeamVO> listAllTeam()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<TeamVO> listAllTeamByFirstCharacter(String s)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<TeamVO> listAllTeamIncludeInactive(boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public TeamVO findTeamByID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public boolean createNewTeam(TeamVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public boolean updateTeam(TeamVO d) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public void inActiveTeamWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public String[] copyAddressFromDepartmentWithTeamID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public String[] copyAddressFromOrganisationWithTeamID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public void activeTeamWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

}
