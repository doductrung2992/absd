package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.utility.DBConnectionUtility;
import mock.appcode.common.valueobjects.UserVO;

public class UserDAOImpl implements UserDAO {

    @Override
	public boolean checkLogin(UserVO user)
			throws ClassNotFoundException, SQLException, DataValidationException {
		
		Connection con = DBConnectionUtility.shareInstance().getConnection();
		String sql = "SELECT * FROM dbo.[User] WHERE Account=? AND Password=?";
		PreparedStatement pre = con.prepareStatement(sql);
		pre.setString(1, user.getAccount());
		pre.setString(2, user.getPassword());
		ResultSet rs = pre.executeQuery();
		if(rs.next()){		    
			return true;			
		}
		return false;
	}
}
