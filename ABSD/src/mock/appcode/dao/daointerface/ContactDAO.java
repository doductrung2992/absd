package mock.appcode.dao.daointerface;

import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.ContactVO;

public interface ContactDAO {
	public ArrayList<ContactVO> listAllContact() throws DataAccessException,
			FunctionalException, ClassNotFoundException, SQLException;

	public ArrayList<ContactVO> listAllContactIncludeInactive(boolean b)
			throws DataAccessException, FunctionalException,
			ClassNotFoundException, SQLException;

	public ContactVO findContactById(int id) throws DataAccessException,
			FunctionalException, ClassNotFoundException, SQLException;

	public boolean addNewContact(ContactVO c) throws DataAccessException,
			FunctionalException, ClassNotFoundException, SQLException;

	public ArrayList<ContactVO> findContactByName(String firstName,
			String surName) throws ClassNotFoundException, SQLException;

	public void changeActiveStatus(int id, int i) throws ClassNotFoundException, SQLException;

	public boolean amendContact(ContactVO contact) throws ClassNotFoundException, SQLException;
}
