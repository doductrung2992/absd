package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import mock.appcode.common.utility.DBConnectionUtility;
import mock.appcode.common.valueobjects.BestContactMethodVO;

public class BestContactMethodDAOImpl implements BestContactMethodDAO {

	@Override
	public List<BestContactMethodVO> listBestContactMethod()
			throws ClassNotFoundException, SQLException {
	    List<BestContactMethodVO> map = new ArrayList<BestContactMethodVO>();
		Connection con = DBConnectionUtility.shareInstance().getConnection();
		String sql = "SELECT BestContactMethodID, Value FROM dbo.BestContactMethod";
		PreparedStatement prstm = con.prepareStatement(sql);
		ResultSet rs = prstm.executeQuery();
		BestContactMethodVO bestContactMethod;
		while(rs.next()){
		    bestContactMethod = new BestContactMethodVO();			
			bestContactMethod.setId(rs.getInt("BestContactMethodID"));
			bestContactMethod.setValue(rs.getString("Value"));
			map.add(bestContactMethod);
		}
		return map;
	}

}

