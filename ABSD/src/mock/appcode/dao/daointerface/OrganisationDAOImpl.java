package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.DBConnectionUtility;
import mock.appcode.common.valueobjects.ContactVO;
import mock.appcode.common.valueobjects.CountyVO;
import mock.appcode.common.valueobjects.OrganisationVO;
import mock.appcode.common.valueobjects.TypeOfBusinessVO;

/**
 * @author do duc trung
 * @version Aug 5, 2014
 */
public class OrganisationDAOImpl implements OrganisationDAO {
    public ArrayList<OrganisationVO> listAllOrganisation()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<OrganisationVO> list = new ArrayList<OrganisationVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT OrgID" + ",OrgName" + ",o.ContactID"
                + ",(c.FirstName+' '+c.SurName) AS FullName" + ",AddressLine1"
                + ",Postcode" + ",o.IsActive" + " FROM dbo.Organisation AS o"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON o.ContactID = c.ContactID";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        ResultSet re = pre.executeQuery();
        OrganisationVO organisation = null;
        while (re.next()) {
            organisation = new OrganisationVO();
            organisation.setId(re.getInt("OrgID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("FullName"));
            organisation.setContact(contact);
            organisation.setName(re.getString("OrgName"));
            organisation.setAddressLine1(re.getString("AddressLine1"));
            organisation.setPostcode(re.getInt("Postcode"));
            boolean b = (re.getInt("IsActive") == 1 ? true : false);
            organisation.setIsActive(b);
            list.add(organisation);
        }
        System.out.println("We got :" + list.size() + " organisations");
        return list;
    }

    public ArrayList<OrganisationVO> listAllOrganisationByFirstCharacter(
            String s) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<OrganisationVO> list = new ArrayList<OrganisationVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT OrgID" + ",OrgName" + ",o.ContactID"
                + ",(c.FirstName+' '+c.SurName) AS FullName" + ",AddressLine1"
                + ",Postcode" + ",o.IsActive" + " FROM dbo.Organisation AS o"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON o.ContactID = c.ContactID"
                + " WHERE o.OrgName LIKE ? OR o.OrgName LIKE ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setString(1, "" + s + "%");
        pre.setString(2, "" + s.toLowerCase() + "%");
        ResultSet re = pre.executeQuery();
        OrganisationVO organisation = null;
        while (re.next()) {
            organisation = new OrganisationVO();
            organisation.setId(re.getInt("OrgID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("FullName"));
            organisation.setContact(contact);
            organisation.setName(re.getString("OrgName"));
            organisation.setAddressLine1(re.getString("AddressLine1"));
            organisation.setPostcode(re.getInt("Postcode"));
            boolean b = (re.getInt("IsActive") == 1 ? true : false);
            organisation.setIsActive(b);
            list.add(organisation);
        }
        System.out.println("We got :" + list.size() + " organisations ::");
        return list;
    }

    public ArrayList<OrganisationVO> listAllOrganisationIncludeInactive(
            boolean b) throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        ArrayList<OrganisationVO> list = new ArrayList<OrganisationVO>();
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT OrgID" + ",OrgName" + ",o.ContactID"
                + ",(c.FirstName+' '+c.SurName) AS FullName" + ",AddressLine1"
                + ",Postcode" + ",o.IsActive" + " FROM dbo.Organisation AS o"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON o.ContactID = c.ContactID";
        String appendStr = " WHERE IsActive = 1";
        if (!b) {
            sqlStr = sqlStr + appendStr;
        }
        PreparedStatement pre = con.prepareStatement(sqlStr);
        ResultSet re = pre.executeQuery();
        OrganisationVO organisation = null;
        while (re.next()) {
            organisation = new OrganisationVO();
            organisation.setId(re.getInt("OrgID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("FullName"));
            organisation.setContact(contact);
            organisation.setName(re.getString("OrgName"));
            organisation.setAddressLine1(re.getString("AddressLine1"));
            organisation.setPostcode(re.getInt("Postcode"));
            boolean bo = (re.getInt("IsActive") == 1 ? true : false);
            organisation.setIsActive(bo);
            list.add(organisation);
        }
        System.out.println("We got :" + list.size() + " organisations");
        return list;
    }

    public OrganisationVO findorganisationByID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        OrganisationVO organisation = null;
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT OrgID"
                + ",(c.FirstName+' '+c.SurName) AS ContactName" + ",OrgName"
                + ",ShortDescription" + ",FullDescription" + ",AddressLine1"
                + ",AddressLine2" + ",AddressLine3" + ",CountryName"
                + ",BusinessTypeID" + ",Postcode" + ",PhoneNumber" + ",Fax"
                + ",WebAddress" + ",CharityNumber" + ",CompanyNumber"
                + ",CountyID" + ",TownName" + ",o.IsActive" + ",o.Email"
                + ",o.ContactID" + " FROM dbo.Organisation AS o"
                + " LEFT JOIN dbo.Contact AS c"
                + " ON o.ContactID = c.ContactID" + " WHERE o.OrgID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            organisation = new OrganisationVO();
            organisation.setId(re.getInt("OrgID"));
            ContactVO contact = new ContactVO();
            contact.setFullName(re.getString("ContactName"));
            contact.setId(re.getInt("ContactID"));
            organisation.setContact(contact);
            organisation.setName(re.getString("OrgName"));
            organisation.setShortDescription(re.getString("ShortDescription"));
            organisation.setFullDescription(re.getString("FullDescription"));
            organisation.setAddressLine1(re.getString("AddressLine1"));
            organisation.setAddressLine2(re.getString("AddressLine2"));
            organisation.setAddressLine3(re.getString("AddressLine3"));
            organisation.setCountryName(re.getString("CountryName"));
            TypeOfBusinessVO business = new TypeOfBusinessVO();
            business.setId(re.getInt("BusinessTypeID"));
            organisation.setTypeOfBusiness(business);
            organisation.setPostcode(re.getInt("Postcode"));
            organisation.setPhoneNumber(re.getString("PhoneNumber"));
            organisation.setFax(re.getString("Fax"));
            organisation.setWebAddress(re.getString("WebAddress"));
            CountyVO county = new CountyVO();
            county.setId(re.getInt("CountyID"));
            CountyDAO ctDAO = new CountyDAOImpl();
            String ctName = ctDAO.returnCountyNameWithId(county.getId());
            county.setName(ctName);
            organisation.setCounty(county);
            boolean bo = (re.getInt("IsActive") == 1 ? true : false);
            organisation.setIsActive(bo);
            organisation.setEmail(re.getString("Email"));
            organisation.setTown(re.getString("TownName"));
        }
        return organisation;
    }

    public boolean createNeworganisation(OrganisationVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "INSERT INTO dbo.Organisation " + "(DirectorateID"
                + ",ContactID" + ",Name" + ",ShortDescription"
                + ",FullDescription" + ",AddressLine1" + ",AddressLine2"
                + ",AddressLine3" + ",CountryName" + ",BusinessTypeID"
                + ",Postcode" + ",PhoneNumber" + ",Fax" + ",WebAddress"
                + ",CountyID" + ",TownName" + ",IsActive,Email)" + " VALUES"
                + " (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        // simulate directorate
        pre.setInt(1, 1);
        // simulate contact
        pre.setInt(2, 1);
        pre.setString(3, d.getName());
        pre.setString(4, d.getShortDescription());
        pre.setString(5, d.getFullDescription());
        pre.setString(6, d.getAddressLine1());
        pre.setString(7, d.getAddressLine2());
        pre.setString(8, d.getAddressLine3());
        pre.setString(9, d.getCountryName());
        // simulate type of business
        pre.setInt(10, 1);
        pre.setInt(11, d.getPostcode());
        pre.setString(12, d.getPhoneNumber());
        pre.setString(13, d.getFax());
        pre.setString(14, d.getWebAddress());
        CountyDAO countyDAO = new CountyDAOImpl();
        int countyID = -1;
        countyID = countyDAO.checkCountyNameForId(d.getCounty().getName());
        pre.setInt(15, countyID);
        pre.setString(16, d.getTown());
        int isActive = (d.getIsActive() ? 1 : 0);
        pre.setInt(17, isActive);
        pre.setString(18, d.getEmail());

        int reInt = pre.executeUpdate();
        if (reInt == -1) {
            return false;
        }
        return true;
    }

    public boolean updateorganisation(OrganisationVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Organisation" + " SET ContactID=?"
                + ",Name=?" + ",ShortDescription=?" + ",FullDescription=?"
                + ",AddressLine1=?" + ",AddressLine2=?" + ",AddressLine3=?"
                + ",CountryName=?" + ",BusinessTypeID=?" + ",Postcode=?"
                + ",PhoneNumber=?" + ",Fax=?" + ",WebAddress=?" + ",CountyID=?"
                + ",TownName=?" + ",IsActive=?,Email=?"
                + " WHERE organisationID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        // simulate directorate
        // simulate contact
        pre.setInt(1, 1);
        pre.setString(2, d.getName());
        pre.setString(3, d.getShortDescription());
        pre.setString(4, d.getFullDescription());
        pre.setString(5, d.getAddressLine1());
        pre.setString(6, d.getAddressLine2());
        pre.setString(7, d.getAddressLine3());
        pre.setString(8, d.getCountryName());
        // simulate type of business
        pre.setInt(9, 1);
        pre.setInt(10, d.getPostcode());
        pre.setString(11, d.getPhoneNumber());
        pre.setString(12, d.getFax());
        pre.setString(13, d.getWebAddress());
        CountyDAO countyDAO = new CountyDAOImpl();
        int countyID = -1;
        countyID = countyDAO.checkCountyNameForId(d.getCounty().getName());
        pre.setInt(14, countyID);
        pre.setString(15, d.getTown());
        int isActive = (d.getIsActive() ? 1 : 0);
        pre.setInt(16, isActive);
        pre.setString(17, d.getEmail());
        pre.setInt(18, d.getId());
        int reInt = pre.executeUpdate();
        if (reInt == -1) {
            return false;
        }
        return true;
    }

    public void inActiveorganisationWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Organisation" + " SET IsActive = 0"
                + " WHERE OrgID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        pre.executeUpdate();
    }

    public void activeorganisationWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException {
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "UPDATE dbo.Organisation" + " SET IsActive = 1"
                + " WHERE OrgID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        pre.executeUpdate();
    }

    public String[] copyAddressFromDirectorateWithorganisationID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        String[] addresses = new String[3];
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT di.AddressLine1, di.AddressLine2, di.AddressLine3"
                + " FROM dbo.Organisation AS de JOIN dbo.Directorate AS di"
                + " ON de.DirectorateID = di.DirectorateID"
                + " WHERE de.organisationID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            addresses[0] = re.getString("AddressLine1");
            addresses[1] = re.getString("AddressLine2");
            addresses[2] = re.getString("AddressLine3");
        }
        return addresses;

    }

    public String[] copyAddressFromOrganisationWithorganisationID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException {
        String[] addresses = new String[3];
        Connection con = DBConnectionUtility.shareInstance().getConnection();
        String sqlStr = "SELECT o.AddressLine1, o.AddressLine2, o.AddressLine3"
                + " FROM dbo.Organisation AS de JOIN dbo.Directorate AS di"
                + " ON de.DirectorateID = di.DirectorateID"
                + " JOIN dbo.Organisation AS o" + " ON di.OrgID = o.OrgID"
                + " WHERE de.organisationID = ?";
        PreparedStatement pre = con.prepareStatement(sqlStr);
        pre.setInt(1, id);
        ResultSet re = pre.executeQuery();
        if (re.next()) {
            addresses[0] = re.getString("AddressLine1");
            addresses[1] = re.getString("AddressLine2");
            addresses[2] = re.getString("AddressLine3");
        }
        return addresses;

    }

    public void changeActiveStatus(int id, int status)
            throws ClassNotFoundException, SQLException {
        String sql = "UPDATE dbo.Organisation SET IsActive=? WHERE OrgID=?";
        Connection conn = DBConnectionUtility.shareInstance().getConnection();
        PreparedStatement prstm = conn.prepareStatement(sql);
        System.out.println("PROGRAM ID: " + id);
        prstm.setInt(1, status);
        prstm.setInt(2, id);
        prstm.execute();
        conn.close();
    }
}
