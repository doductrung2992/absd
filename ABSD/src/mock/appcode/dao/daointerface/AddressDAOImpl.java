package mock.appcode.dao.daointerface;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.DataValidationException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.utility.DBConnectionUtility;
import mock.appcode.common.valueobjects.AddressVO;
import mock.appcode.common.valueobjects.CountryVO;
import mock.appcode.common.valueobjects.CountyVO;
import mock.appcode.common.valueobjects.TownVO;

/**
 * @author do duc trung
 * @version Aug 9, 2014
 */
public class AddressDAOImpl implements AddressDAO {
	public boolean checkPostcode(int postcode) throws DataAccessException,
			FunctionalException, ClassNotFoundException,
			DataValidationException, SQLException {
		boolean b = false;
		Connection con = DBConnectionUtility.shareInstance().getConnection();
		String sqlStr = "SELECT * FROM dbo.Address WHERE PostCode = ?";
		PreparedStatement pre = con.prepareStatement(sqlStr);
		pre.setInt(1, postcode);
		ResultSet re = pre.executeQuery();
		if (re.next()) {
			b = true;
		}
		return b;
	}

	@Override
	public Vector<AddressVO> getAddressByOption(String postcode, String street,
			String town) throws ClassNotFoundException, SQLException {
		Connection con = DBConnectionUtility.shareInstance().getConnection();

		String sqlstr = "SELECT * FROM dbo.Address as a JOIN dbo.Town as t On a.TownID =t.TownID"
				+ " JOIN dbo.Country as c On a.CountryID = c.CountryID"
				+ " JOIN dbo.County as c2 On a.CountyID = c2.CountyID "
				+ "  WHERE 1=1 ";

		if (postcode != null && postcode.length() > 0) {
			sqlstr = sqlstr + " AND Postcode like '%" + postcode + "%' ";
		}

		if (street != null && street.length() > 0) {
			sqlstr = sqlstr + " AND Street like '%" + street + "%' ";
		}

		if (town != null && town.length() > 0) {
			sqlstr = sqlstr + " AND t.TownName like '%" + town + "%' ";
		}

		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(sqlstr);

		Vector<AddressVO> res = new Vector<AddressVO>();

		while (rs.next()) {
			TownVO tvo = new TownVO(rs.getInt("TownID"), null,
					rs.getString("TownName"));
			CountryVO country = new CountryVO(rs.getInt("CountryID"),
					rs.getString("CountryName"), null, null);
			CountyVO county = new CountyVO(rs.getInt("CountyID"),
					rs.getString("CountyName"), null, null);

			AddressVO avo = new AddressVO(rs.getInt("AddressID"),
					rs.getInt("PostCode"), tvo, rs.getString("Street"),
					country, county);
			res.add(avo);
		}
		return res;
	}
}
