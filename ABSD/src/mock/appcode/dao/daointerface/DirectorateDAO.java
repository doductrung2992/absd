package mock.appcode.dao.daointerface;

import java.sql.SQLException;
import java.util.ArrayList;

import mock.appcode.common.exception.DataAccessException;
import mock.appcode.common.exception.FunctionalException;
import mock.appcode.common.valueobjects.DirectorateVO;

public interface DirectorateDAO {
	public ArrayList<DirectorateVO> listAllDirectorate()
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<DirectorateVO> listAllDirectorateByFirstCharacter(String s)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public ArrayList<DirectorateVO> listAllDirectorateIncludeInactive(boolean b)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public DirectorateVO findDirectorateByID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public boolean createNewDirectorate(DirectorateVO d)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public boolean updateDirectorate(DirectorateVO d) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public void inActiveDirectorateWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

    public String[] copyAddressFromOrganisationWithDirectorateID(int id)
            throws DataAccessException, FunctionalException,
            ClassNotFoundException, SQLException;

    public void activeDirectorateWithID(int id) throws DataAccessException,
            FunctionalException, ClassNotFoundException, SQLException;

}
