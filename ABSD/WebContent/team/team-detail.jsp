<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Team Details</title>
<link rel="stylesheet" type="text/css" href="../css/government-office-region.css">
<%@ include file="../Head.jsp" %>
<%@ include file="../checkLogin.jsp" %>
<script>
var child;
var timer;
var lookupType;
function goBack() {
    window.history.back()
}
function radioChoose() {
	var a = document.getElementsByName("selectedOption")[0].checked;
	var b = document.getElementsByName("selectedOption")[1].checked;
	if(a == true) {
		
	}
	if(b == true) {
		
	}
	
}
function openLookup(type) {
	lookupType = type;
	if(lookupType == "postcode") {
		child = window.open('${pageContext.request.contextPath}/lookup/searchAddress?postcode=&street=&town=','','toolbar=0,status=0');
		timer = setInterval(checkChild, 500);
	} else if(lookupType == "typebusiness") {
		child = window.open('${pageContext.request.contextPath}/lookup/BusinessType.jsp','','toolbar=0,status=0');
		timer = setInterval(checkChild, 500);
	} else if(lookupType == "contact") {
		child = window.open('${pageContext.request.contextPath}/contact/contact.jsp','','toolbar=0,status=0');
		timer = setInterval(checkChild, 500);
	}
}
function checkChild() {
    if (child.closed) {
    	var a = checkCookie();    	
    	var x = document.getElementById("detail-form");
    	if(lookupType == "postcode") {
    		for (var i=0; i<x.length; i++)
    	  	{
    			if(x.elements[i].name == "selectedTeam.postcode") {
    	  			x.elements[i].value = a;
    	  			break;
    			}
    	  	}    	
    	} else if(lookupType == "typebusiness") {
    		var b = new Array();
    		var index = 0;
    		for(var j = 0; j< a.length; j++) {
    			if(a[j] != null) {
    				b[index] = a[j];
    				index++;
    			}
    		}
    		for (var i=0; i<x.length; i++)
    	  	{
    			if(x.elements[i].name == "selectedTeam.typeOfBusiness.id") {
    	  			x.elements[i].value = b[0];    	  			
    			} else
    			if(x.elements[i].name == "selectedTeam.typeOfBusiness.name") {
    	  			x.elements[i].value = b[1];    	  			
    			} else
    			if(x.elements[i].name == "selectedTeam.typeOfBusiness.sicCode") {
    	  			x.elements[i].value = b[2];    	  			
    			}
    	  	}   
    	} else if(lookupType == "contact") {
    		var b = new Array();
    		var index = 0;
    		for(var j = 0; j< a.length; j++) {
    			if(a[j] != null) {
    				b[index] = a[j];
    				index++;
    			}
    		}
    		for (var i=0; i<x.length; i++)
    	  	{
    			if(x.elements[i].name == "selectedTeam.contact.id") {
    	  			x.elements[i].value = b[0];    	  			
    			} else
    			if(x.elements[i].name == "selectedTeam.contact.fullName") {
    	  			x.elements[i].value = b[1];    	  			
    			}
    	  	}
    	}
    	//alert(a); 
        clearInterval(timer);
    }
}
function checkCookie() {
	var name = "postcode=";
	var typeId="typeID=";
	var typeName="typeName=";
	var sicCode="sic=";
	var contactID = "contactID=";
	var contactName = "contactName=";
	var ca = document.cookie.split(';');
	//alert(ca);	
	var type = new Array();
	var index = 0;
	for(var i=0; i<ca.length; i++) {
        var c = ca[i];        
        while (c.charAt(0)==' ') {
        	c = c.substring(1);
        }
        if(lookupType == "postcode") {
        	if (c.indexOf(name) != -1) {        		        
        		return c.substring(name.length, c.length);
        	}
        } else if(lookupType == "typebusiness") {        	
        	if (c.indexOf(typeId) != -1) {        		
        		type[index] = c.substring(typeId.length, c.length);        		
        	} else if (c.indexOf(typeName) != -1) {        		
        		type[index] = c.substring(typeName.length, c.length);        		
        	} else if (c.indexOf(sicCode) != -1) {        		
        		type[index] = c.substring(sicCode.length, c.length);        		
        	}        	
        	index++;
        } else if(lookupType == "contact") {        	
        	if (c.indexOf(contactID) != -1) {        		
        		type[index] = c.substring(contactID.length, c.length);        		
        	} else if (c.indexOf(contactName) != -1) {        		
        		type[index] = c.substring(contactName.length, c.length);        		
        	}       	
        	index++;
        }         
    }	
	return type;
}
</script>
</head>
<body>
<div class="container">
<%@ include file="../Menu.jsp" %>
<h2>Team Detail</h2>
<button onclick="goBack()">Go Back</button>	
<s:actionerror />
<s:form id="detail-form" action="createTeamAction" method="post" theme="css_xhtml">
	<s:submit value="In-Active" name="inactive" action="inactiveTeamAction"></s:submit>
	<div id="form-container">
	<div id="form-part1">	
	<s:hidden name="selectedTeam.id" value="%{selectedTeam.id}"></s:hidden>
	<s:textfield  label="Team Name" name="selectedTeam.name" value="%{selectedTeam.name}"></s:textfield>	
	<s:textarea label="Team Short Description" name="selectedTeam.shortDescription" value="%{selectedTeam.shortDescription}"></s:textarea>
	<s:textfield label="Lead Contact" name="selectedTeam.contact.fullName" value="%{selectedTeam.contact.fullName}"></s:textfield>
	<s:hidden name="selectedTeam.contact.id" value="%{selectedTeam.contact.id}"></s:hidden>
	<a href="javascript:openLookup('contact');">Lookup</a>
	
	<s:set var="myList" value="{'Organisation','Parent'}"/>
	<s:radio id="radio" label="Copy Address from" name="selectedOption" list="#myList" onclick="radioChoose()"/>		
	<s:textfield label="Address Line 1" name="selectedTeam.addressLine1" value="%{selectedTeam.addressLine1}"></s:textfield>
	<s:textfield label="Address Line 2" name="selectedTeam.addressLine2" value="%{selectedTeam.addressLine2}"></s:textfield>
	<s:textfield label="Address Line 3" name="selectedTeam.addressLine3" value="%{selectedTeam.addressLine3}"></s:textfield>
	<s:textfield id="tfpostcode" label="Postcode" name="selectedTeam.postcode" value="%{selectedTeam.postcode}"></s:textfield>
	<a href="javascript:openLookup('postcode');">Lookup</a>
	<s:textfield label="Town/Village/City" name="selectedTeam.town" value="%{selectedTeam.town}"></s:textfield>
	<s:textfield label="County" name="selectedTeam.county.name" value="%{selectedTeam.county.name}"></s:textfield>	
	<s:select list="countryName" label="Nation/Country" name="selectedDepartment.countryName" value="%{selectedTeam.countryName}"></s:select>
	</div>
	
	<div id="form-part2">
		<s:textfield label="Type of Business" name="selectedTeam.typeOfBusiness.name" value="%{selectedTeam.typeOfBusiness.name}"></s:textfield>
		<s:a href="javascript:openLookup('typebusiness');" label="Lookup">Lookup</s:a>		
		<s:textfield label="SIC Code" name="selectedTeam.typeOfBusiness.sicCode" value="%{selectedTeam.typeOfBusiness.sicCode}"></s:textfield>
		<s:textarea label="Team Full Description" name="selectedTeam.fullDescription" value="%{selectedTeam.fullDescription}"></s:textarea>
		<s:textfield label="Phone Number" name="selectedTeam.phoneNumber" value="%{selectedTeam.phoneNumber}"></s:textfield>	
		<s:textfield label="Fax" name="selectedTeam.fax" value="%{selectedTeam.fax}"></s:textfield>
		<s:textfield label="Email" name="selectedTeam.email" value="%{selectedTeam.email}"></s:textfield>
		<s:textfield label="Web address" name="selectedTeam.webAddress" value="%{selectedTeam.webAddress}"></s:textfield>
		<s:submit value="Save" name="isSaving"></s:submit>		
	</div>
	</div>
	
</s:form>
<%@ include file="../Footer.jsp" %>
</div>
</body>
</html>