<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<%@taglib prefix="s" uri="/struts-tags" %>
	<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Team</title>
<link rel="stylesheet" type="text/css" href="../css/government-office-region.css">
<%@ include file="../Head.jsp" %>
<%@ include file="../checkLogin.jsp" %>
<script type="text/javascript">
function checkBoxTicked() 
{
	var x = document.getElementById("show-inactive-checkbox").checked;
	var currentLocation = window.location;
	window.location = "${pageContext.request.contextPath}/team/doShowInactiveTeamAction!doShowInactive.action?includeInactive="+x;
}
function itemSelect(id, isActive) {
	if(isActive == true) {
		window.location = "${pageContext.request.contextPath}/team/showDetailTeamAction!showTeamDetail.action?selectedId="+id;
	} else {
		var r = confirm("Do you want to active this team?");
		if (r == true) {
			window.location = "${pageContext.request.contextPath}/team/activeTeamAction!activeTeam.action?selectedId="+id;
		} else {
			window.location = "${pageContext.request.contextPath}/team/showDetailTeamAction!showTeamDetail.action?selectedId="+id;
		}
	}
}
</script>
</head>
<body>
<s:if test="%{listTeam == null}">	
	<s:action name="doFilterTeamAction" namespace="/team" executeResult="true">
		<s:param name="filterStr" value="%{'all'}"/>
	</s:action>
</s:if>
<s:else>
<div class="container">
<%@ include file="../Menu.jsp" %>
<h1>List Team</h1>
	<ul class="region-table clearfix">		
	<s:url action="doFilterTeamAction" method="doFilter" var="filter" />
		<li >			
			<s:url id="filterURL"  namespace="/team" action="doFilterTeamAction" method="doFilter">
                  <s:param name="filterStr" value="%{'all'}"/>
            </s:url>
            <s:a href="%{filterURL}">All</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/team" action="doFilterTeamAction" method="doFilter">
                  <s:param name="filterStr" value="%{'0-9'}"/>
            </s:url>
            <s:a href="%{filterURL}">0-9</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/team" action="doFilterTeamAction" method="doFilter">
                  <s:param name="filterStr" value="%{'A-E'}"/>
            </s:url>
            <s:a href="%{filterURL}">A-E</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/team" action="doFilterTeamAction" method="doFilter">
                  <s:param name="filterStr" value="%{'F-J'}"/>
            </s:url>
            <s:a href="%{filterURL}">F-J</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/team" action="doFilterTeamAction" method="doFilter">
                  <s:param name="filterStr" value="%{'K-N'}"/>
            </s:url>
            <s:a href="%{filterURL}">K-N</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/team" action="doFilterTeamAction" method="doFilter">
                  <s:param name="filterStr" value="%{'O-R'}"/>
            </s:url>
            <s:a href="%{filterURL}">O-R</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/team" action="doFilterTeamAction" method="doFilter">
                  <s:param name="filterStr" value="%{'S-V'}"/>
            </s:url>
            <s:a href="%{filterURL}">S-V</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/team" action="doFilterTeamAction" method="doFilter">
                  <s:param name="filterStr" value="%{'W-Z'}"/>
            </s:url>
            <s:a href="%{filterURL}">W-Z</s:a>
		</li>
		<li>
			<s:a href="doFilterTeamAction!doFilter.action?filterStr=all" >All</s:a>
		</li>
		<li><input type="button" id="btnCreate" value="Create" onClick="location.href='/ABSD/team/showCreatePageAction!showCreatePage.action'"></li>
		<li><s:checkbox id="show-inactive-checkbox" name="includeInactive" value="%{includeInactive}" label="Include-Inactive" onclick="checkBoxTicked()"></s:checkbox>
		</li>
	</ul>            

<div>
<display:table export="false" name="listTeam" pagesize="2" cellpadding="5px;"
cellspacing="5px;" style="margin-left:50px;margin-top:20px;" id="row" requestURI="">
<display:column class="thClass"  title="Team Name"    sortable="true">
<a href="javascript:itemSelect(${row.id}, ${row.isActive})">${row.name}</a>
</display:column>
<display:column class="thClass" property="addressLine1" title="Address line1" sortable="true"></display:column>
<display:column class="thClass" property="postcode" title="Postcode" sortable="true"></display:column>
<display:column class="thClass" property="contact.fullName" title="Contact" sortable="true"></display:column>
<display:column class="thClass" property="isActive" title="Is Active?" sortable="true"></display:column>
<display:setProperty name="paging.banner.placement" value="bottom" />
</display:table>
</div>
<%@ include file="../Footer.jsp" %>
</div>
</s:else>
</body>
</html>