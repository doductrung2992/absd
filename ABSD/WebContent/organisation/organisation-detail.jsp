<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Department Details</title>
<link rel="stylesheet" type="text/css" href="../css/government-office-region.css">
<%@ include file="../Head.jsp" %>
<%@ include file="../checkLogin.jsp" %>
<script>
var child;
var timer;
var lookupType;
function goBack() {
    window.history.back()
}
function radioChoose() {
	var a = document.getElementsByName("selectedOption")[0].checked;
	var b = document.getElementsByName("selectedOption")[1].checked;
	if(a == true) {
		
	}
	if(b == true) {
		
	}
	
}
function openLookup(type) {
	lookupType = type;
	if(lookupType == "postcode") {
		child = window.open('${pageContext.request.contextPath}/lookup/searchAddress?postcode=&street=&town=','','toolbar=0,status=0');
		timer = setInterval(checkChild, 500);
	} else if(lookupType == "typebusiness") {
		child = window.open('${pageContext.request.contextPath}/lookup/BusinessType.jsp','','toolbar=0,status=0');
		timer = setInterval(checkChild, 500);
	} else if(lookupType == "contact") {
		child = window.open('${pageContext.request.contextPath}/contact/contact.jsp','','toolbar=0,status=0');
		timer = setInterval(checkChild, 500);
	}
}
function checkChild() {
    if (child.closed) {
    	var a = checkCookie();    	
    	var x = document.getElementById("detail-form");
    	if(lookupType == "postcode") {
    		for (var i=0; i<x.length; i++)
    	  	{
    			if(x.elements[i].name == "selectedOrganisation.postcode") {
    	  			x.elements[i].value = a;
    	  			break;
    			}
    	  	}    	
    	} else if(lookupType == "typebusiness") {
    		var b = new Array();
    		var index = 0;
    		for(var j = 0; j< a.length; j++) {
    			if(a[j] != null) {
    				b[index] = a[j];
    				index++;
    			}
    		}
    		for (var i=0; i<x.length; i++)
    	  	{
    			if(x.elements[i].name == "selectedOrganisation.typeOfBusiness.id") {
    	  			x.elements[i].value = b[0];    	  			
    			} else
    			if(x.elements[i].name == "selectedOrganisation.typeOfBusiness.name") {
    	  			x.elements[i].value = b[1];    	  			
    			} else
    			if(x.elements[i].name == "selectedOrganisation.typeOfBusiness.sicCode") {
    	  			x.elements[i].value = b[2];    	  			
    			}
    	  	}   
    	} else if(lookupType == "contact") {
    		var b = new Array();
    		var index = 0;
    		for(var j = 0; j< a.length; j++) {
    			if(a[j] != null) {
    				b[index] = a[j];
    				index++;
    			}
    		}
    		for (var i=0; i<x.length; i++)
    	  	{
    			if(x.elements[i].name == "selectedOrganisation.contact.id") {
    	  			x.elements[i].value = b[0];    	  			
    			} else
    			if(x.elements[i].name == "selectedOrganisation.contact.fullName") {
    	  			x.elements[i].value = b[1];    	  			
    			}
    	  	}
    	}
    	//alert(a); 
        clearInterval(timer);
    }
}
function checkCookie() {
	var name = "postcode=";
	var typeId="typeID=";
	var typeName="typeName=";
	var sicCode="sic=";
	var contactID = "contactID=";
	var contactName = "contactName=";
	var ca = document.cookie.split(';');
	//alert(ca);	
	var type = new Array();
	var index = 0;
	for(var i=0; i<ca.length; i++) {
        var c = ca[i];        
        while (c.charAt(0)==' ') {
        	c = c.substring(1);
        }
        if(lookupType == "postcode") {
        	if (c.indexOf(name) != -1) {        		        
        		return c.substring(name.length, c.length);
        	}
        } else if(lookupType == "typebusiness") {        	
        	if (c.indexOf(typeId) != -1) {        		
        		type[index] = c.substring(typeId.length, c.length);        		
        	} else if (c.indexOf(typeName) != -1) {        		
        		type[index] = c.substring(typeName.length, c.length);        		
        	} else if (c.indexOf(sicCode) != -1) {        		
        		type[index] = c.substring(sicCode.length, c.length);        		
        	}        	
        	index++;
        } else if(lookupType == "contact") {        	
        	if (c.indexOf(contactID) != -1) {        		
        		type[index] = c.substring(contactID.length, c.length);        		
        	} else if (c.indexOf(contactName) != -1) {        		
        		type[index] = c.substring(contactName.length, c.length);        		
        	}       	
        	index++;
        }           
    }	
	return type;
}
</script>
</head>
<body>
<div class="container">
<%@ include file="../Menu.jsp" %>
<h2>Department Detail</h2>
<button onclick="goBack()">Go Back</button>	
<s:actionerror />
<s:form id="detail-form" action="createDepartmentAction" method="post" theme="css_xhtml">
	<s:submit value="In-Active" name="inactive" action="inactiveDepartmentAction"></s:submit>
	<div id="form-container">
	<div id="form-part1">	
	<s:hidden name="selectedOrganisation.id" value="%{selectedOrganisation.id}"></s:hidden>
	<s:textfield  label="Organisation Name" name="selectedOrganisation.name" value="%{selectedOrganisation.name}"></s:textfield>	
	<s:textarea label="Organisation Short Description" name="selectedOrganisation.shortDescription" value="%{selectedOrganisation.shortDescription}"></s:textarea>
	<s:textfield label="Lead Contact" name="selectedOrganisation.contact.fullName" value="%{selectedOrganisation.contact.fullName}"></s:textfield>
	<s:hidden name="selectedOrganisation.contact.id" value="%{selectedOrganisation.contact.id}"></s:hidden>
	<a href="javascript:openLookup('contact');">Lookup</a>
		
	<s:textfield label="Address Line 1" name="selectedOrganisation.addressLine1" value="%{selectedOrganisation.addressLine1}"></s:textfield>
	<s:textfield label="Address Line 2" name="selectedOrganisation.addressLine2" value="%{selectedOrganisation.addressLine2}"></s:textfield>
	<s:textfield label="Address Line 3" name="selectedOrganisation.addressLine3" value="%{selectedOrganisation.addressLine3}"></s:textfield>
	<s:textfield id="tfpostcode" label="Postcode" name="selectedOrganisation.postcode" value="%{selectedOrganisation.postcode}"></s:textfield>
	<a href="javascript:openLookup('postcode');">Lookup</a>
	<s:textfield label="Town/Village/City" name="selectedOrganisation.town" value="%{selectedOrganisation.town}"></s:textfield>
	<s:textfield label="County" name="selectedOrganisation.county.name" value="%{selectedOrganisation.county.name}"></s:textfield>	
	<s:select list="countryName" label="Nation/Country" name="selectedOrganisation.countryName" value="%{selectedOrganisation.countryName}"></s:select>
	</div>
	
	<div id="form-part2">		
		<input type="checkbox" >Preffered Organisation <br/>		
		<input type="checkbox" >Expression of Interest <br/>
		<s:textfield label="Type of Business" name="selectedOrganisation.typeOfBusiness.name" value="%{selectedOrganisation.typeOfBusiness.name}"></s:textfield>
		<s:a href="javascript:openLookup('typebusiness');" label="Lookup">Lookup</s:a>		
		<s:hidden name="selectedOrganisation.typeOfBusiness.id" value="%{selectedOrganisation.typeOfBusiness.id}"></s:hidden>
		<s:textfield label="SIC Code" name="selectedOrganisation.typeOfBusiness.sicCode" value="%{selectedOrganisation.typeOfBusiness.sicCode}"></s:textfield>		
		<s:textarea label="Organisation Full Description" name="selectedOrganisation.fullDescription" value="%{selectedOrganisation.fullDescription}"></s:textarea>
		<s:textfield label="Phone Number" name="selectedOrganisation.phoneNumber" value="%{selectedOrganisation.phoneNumber}"></s:textfield>	
		<s:textfield label="Fax" name="selectedOrganisation.fax" value="%{selectedOrganisation.fax}"></s:textfield>
		<s:textfield label="Email" name="selectedOrganisation.email" value="%{selectedOrganisation.email}"></s:textfield>
		<s:textfield label="Web address" name="selectedOrganisation.webAddress" value="%{selectedOrganisation.webAddress}"></s:textfield>
		<s:submit value="Save" name="isSaving"></s:submit>		
	</div>
	</div>
	
</s:form>
<%@ include file="../Footer.jsp" %>
</div>
</body>
</html>