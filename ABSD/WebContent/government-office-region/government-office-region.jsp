<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="s" uri="/struts-tags" %>
	<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Government Office Region</title>
<link rel="stylesheet" type="text/css" href="../css/government-office-region.css">
<%@ include file="../Head.jsp" %>
<%@ include file="../checkLogin.jsp" %>
<script type="text/javascript">
function checkBoxTicked() 
{
	var x = document.getElementById("show-inactive-checkbox").checked;	
	window.location = "${pageContext.request.contextPath}/government-office-region/doShowInactiveAction!doShowInactive.action?includeInactive="+x;
}
</script>
</head>
<body>
<s:if test="%{listRegion == null}">
	<s:action name="doFilterAction" namespace="/government-office-region" executeResult="true">
		<s:param name="filterStr" value="%{'all'}"/>
	</s:action>
</s:if>
<s:else>
<div class="container">
<%@ include file="../Menu.jsp" %>
<h1>List Government Office Region</h1>
	<ul class="region-table clearfix">		
	<s:url action="doFilterAction" method="doFilter" var="filter" />
		<li >			
			<s:url id="filterURL"  namespace="/government-office-region" action="doFilterAction" method="doFilter">
                  <s:param name="filterStr" value="%{'all'}"/>
            </s:url>
            <s:a href="%{filterURL}">All</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/government-office-region" action="doFilterAction" method="doFilter">
                  <s:param name="filterStr" value="%{'0-9'}"/>
            </s:url>
            <s:a href="%{filterURL}">0-9</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/government-office-region" action="doFilterAction" method="doFilter">
                  <s:param name="filterStr" value="%{'A-E'}"/>
            </s:url>
            <s:a href="%{filterURL}">A-E</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/government-office-region" action="doFilterAction" method="doFilter">
                  <s:param name="filterStr" value="%{'F-J'}"/>
            </s:url>
            <s:a href="%{filterURL}">F-J</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/government-office-region" action="doFilterAction" method="doFilter">
                  <s:param name="filterStr" value="%{'K-N'}"/>
            </s:url>
            <s:a href="%{filterURL}">K-N</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/government-office-region" action="doFilterAction" method="doFilter">
                  <s:param name="filterStr" value="%{'O-R'}"/>
            </s:url>
            <s:a href="%{filterURL}">O-R</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/government-office-region" action="doFilterAction" method="doFilter">
                  <s:param name="filterStr" value="%{'S-V'}"/>
            </s:url>
            <s:a href="%{filterURL}">S-V</s:a>
		</li>
		<li >
			<s:url id="filterURL"  namespace="/government-office-region" action="doFilterAction" method="doFilter">
                  <s:param name="filterStr" value="%{'W-Z'}"/>
            </s:url>
            <s:a href="%{filterURL}">W-Z</s:a>
		</li>
		<li>
			<s:a href="doFilterAction!doFilter.action?filterStr=all" >All</s:a>
		</li>
		<li><s:checkbox id="show-inactive-checkbox" name="includeInactive" value="%{includeInactive}" label="Include-Inactive" onclick="checkBoxTicked()"></s:checkbox>
		</li>
	</ul>            

<div>
<display:table export="false" id="regionTable" name="listRegion" pagesize="2" cellpadding="5px;"
cellspacing="5px;" style="margin-left:50px;margin-top:20px;" requestURI="">
<display:column class="thClass" property="name" title="Gov Office Region Name" sortable="true" href="/ABSD/government-office-region/showRegionDetailAction!showRegionDetail.action" paramId="selectedId" paramProperty="id"></display:column>
<display:column class="thClass" property="description" title="Description" sortable="true"></display:column>
<display:column class="thClass" property="countyName" title="County" sortable="true"></display:column>
<display:column class="thClass" property="isActiveStr" title="Is Active?" sortable="true"></display:column>
<display:setProperty name="paging.banner.placement" value="bottom" />
</display:table>
</div>
<%@ include file="../Footer.jsp" %>
</div>
</s:else>
</body>
</html>