<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Government office region detail</title>
<link rel="stylesheet" type="text/css" href="../css/government-office-region.css">
<%@ include file="../Head.jsp" %>
<%@ include file="../checkLogin.jsp" %>
<script>
function goBack() {
    window.history.back()
}
</script>
</head>
<body>
<div class="container">
<%@ include file="../Menu.jsp" %>
<h2>Government Office Region Detail</h2>
<button onclick="goBack()">Go Back</button>
<s:form>
	<s:textfield label="Government Office Region Name" value="%{selectedRegion.name}"></s:textfield>
	<s:textarea label="Description" value="%{selectedRegion.description}"></s:textarea>
	<s:textfield label="County" value="%{selectedRegion.countyName}"></s:textfield>	
</s:form>
<%@ include file="../Footer.jsp" %>
</div>
</body>
</html>