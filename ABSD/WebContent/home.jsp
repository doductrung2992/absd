<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@page import="mock.appcode.common.valueobjects.UserVO"%>
<%@page import="mock.appcode.web.action.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link rel="stylesheet" type="text/css"
	href="css/government-office-region.css">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/menu-styles.css">
<script src="http://code.jquery.com/jquery-latest.min.js"
	type="text/javascript"></script>
<script src="script.js"></script>
</head>
<body>	
	<%
	    UserVO user = (UserVO) session
	            .getAttribute(LoginLogoutAction.USER_SESSION);
	    if (user == null) {
	        response.sendRedirect("/ABSD/user/Login.jsp");
	    }
	%>
	<div class="container">
		<%@ include file="Menu.jsp"%>
		<div>
			<h2>
				Welcome back to AB Services Directory
				</h2>
			<img alt="Logo" src="img/logo.jpg">
		</div>

		<div>AB currently does not have a central repository for the
			services that they (and associated) organisations provide. This
			system requirements specification is part of the project to deliver a
			central repository called the Services Directory.</div>
		<%@ include file="Footer.jsp"%>
	</div>

</body>
</html>