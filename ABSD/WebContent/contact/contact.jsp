<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
	<%@taglib prefix="s" uri="/struts-tags" %>
	<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Contact</title>
<link rel="stylesheet" type="text/css" href="../css/government-office-region.css">
<%@ include file="../Head.jsp" %>
<%@ include file="../checkLogin.jsp" %>
<style>
	input[type="button"],input[type="submit"]{
		cursor: pointer;
		background: white;
		color: black;
		font-weight: bold;
		text-align: center;
		padding: 5px 10px;
		font-family: Arial;
		border: none;
		border: 2px solid black;
	}
	input[type="button"]:hover, input[type="submit"]:hover{
		background: black;
		border: 2px solid white;
		color: white;
	}
</style>
<script type="text/javascript">
function checkBoxTicked()
{
	var x = document.getElementById("show-inactive-checkbox").checked;	
	window.location = "${pageContext.request.contextPath}/contact/doShowInactiveAction!doShowInactive.action?includeInactive="+x;
}
var contactId = -1;
var fullName;
function itemSelect() {
	if(contactId>0) {
		var v = confirm("Do you want to select this item?");
		if(v == true) {		
			document.cookie = "contactID="+contactId+"; path=/";
			document.cookie = "contactName="+fullName+"; path=/";
			window.close();
			target="";
		}    
	}
}
function radioSelect(id,name) {
	contactId = id;
	fullName = name;
}
</script>

</head>
<body>
<s:if test="%{listContact == null}">
			<s:action name="doListAction" namespace="/contact" executeResult="true">
			</s:action>
		</s:if>
		<s:else>
<%@ include file="../Menu.jsp"%>
	<form action="searchContactAction">
		First Name: <input name="firstName" value="" type="text">
		Surname: <input name="surName" value="" type="text"> 
 		<input type="submit" value="Search">
	</form>
	<div class="container">		
		<h1>List Contact</h1>
		
			<div>
				<ul>
					<li><input type="button" id="btnCreate" value="Create"
					onClick="location.href='contact-detail.jsp'"></li>
					<li><s:checkbox id="show-inactive-checkbox"
						name="includeInactive" value="%{includeInactive}"
						label="Include-Inactive" onclick="checkBoxTicked()"></s:checkbox>
					</li>
				</ul>
				
				<display:table export="false" id="row" name="listContact"
					pagesize="2" cellpadding="5px;" cellspacing="5px;"
					style="margin-left:50px;margin-top:20px;" requestURI="">
					<display:column title="Select"  class="thClass"  sortable="false">
						<input type="radio" name="itemSelect" onclick="radioSelect(${row.id}, '${row.fullName}')"> Select
					</display:column>
					<display:column class="thClass" property="fullName" title="Contact Name"
						sortable="true"
						href="/ABSD/contact/showContactDetailAction!showContactDetail.action"
						paramId="selectedId" paramProperty="id"></display:column>
					<display:column class="thClass" property="mobilePhone" title="Mobile Phone"
						sortable="true"></display:column>
					<display:column class="thClass" property="email" title="Email"
						sortable="true"></display:column>
					<display:column class="thClass" property="contactTypeStr"
						title="Contact Type" sortable="true"></display:column>
					<display:column class="thClass" property="isActiveStr"
						title="Is Active?" sortable="true"></display:column>

				</display:table>
			</div>
			<input type="button" value="Select" onclick="itemSelect()"/>
		<%@ include file="../Footer.jsp"%>
	</div>
	</s:else>
</body>
</html>