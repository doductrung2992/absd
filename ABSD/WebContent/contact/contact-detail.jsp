<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Contact Details</title>
<link rel="stylesheet" type="text/css" href="../css/government-office-region.css">
<%@ include file="../Head.jsp" %>
<%@ include file="../checkLogin.jsp" %>
</head>
<body>
<s:if test="%{(listContactType == null) || (listBestContactMethod == null)}">
	<s:action name="doGetContactTypeAndMethodAction" namespace="/contact" executeResult="true">
	</s:action>
</s:if>
<s:else>
<div class="container">
<%@ include file="../Menu.jsp" %>
<h2>Contact Detail</h2>
<s:form action="saveContactDetailAction" method="post">
	<table width="1200" align="center" border="0" cellpadding="5" cellspacing="0">
		<tr>
			<td><s:hidden name="selectedContact.id" value="%{selectedContact.id}"></s:hidden></td>
			<td><s:hidden name="selectedContact.managerID" value="%{selectedContact.managerID}"></s:hidden></td>
		</tr>
		<tr>
			<td><s:textfield id="selectedContact.firstName" label="First Name" value="%{selectedContact.firstName}" name="selectedContact.firstName"></s:textfield></td>
			<td><s:textfield id="selectedContact.managerName" label="Manager Name" value="%{selectedContact.managerName}" name="selectedContact.managerName"></s:textfield>
				<a href="" style="text-decoration: none; color: blue;">Lookup</a></td>
		</tr>
		<tr>
			<td><s:textfield id="surName" label="Surname" value="%{selectedContact.surname}" name="selectedContact.surname"></s:textfield></td>
			<td><s:select list="listContactType" listKey="id" listValue="value" name="selectedType" label="Contact Type" value="selectedType"></s:select> </td>
		</tr>
		<tr>
			<td><s:textfield id="knownAs" label="Known As" value="%{selectedContact.knownAs}" name="selectedContact.knownAs"></s:textfield></td>
			<td><s:select list="listBestContactMethod" listKey="id" listValue="value" name="selectedBestMethod" label="BestContactMethod" value="selectedBestMethod"></s:select> </td>
		</tr>
		<tr>
			<td><s:textfield id="officePhone" label="Office Phone" value="%{selectedContact.officePhone}" name="selectedContact.officePhone"></s:textfield></td>
			<td><s:textfield id="jobRole" label="Job Role" value="%{selectedContact.jobRole}" name="selectedContact.jobRole"></s:textfield></td>
		</tr>
		<tr>
			<td><s:textfield id="mobilePhone" label="Mobile Phone" value="%{selectedContact.mobilePhone}" name="selectedContact.mobilePhone"></s:textfield></td>
			<td><s:textfield id="workBase" label="Workbase" value="%{selectedContact.workBase}" name="selectedContact.workBase"></s:textfield></td>
		</tr>
		<tr>
			<td><s:textfield id="stHomePhone" label="ST Home Phone" value="%{selectedContact.stHomePhone}" name="selectedContact.stHomePhone"></s:textfield></td>
			<td><s:textfield id="jobTitle" label="Job Title" value="%{selectedContact.jobTitle}" name="selectedContact.jobTitle"></s:textfield></td>
		</tr>
		<tr>
			<td><s:textfield id="email" label="Email Address" value="%{selectedContact.email}" name="selectedContact.email"></s:textfield></td>
			<td><s:checkbox id="isActive-checkbox" name="selectedContact.isActive" value="%{selectedContact.isActive}" label="Is Active?"></s:checkbox></td>
		</tr>
		<tr>
			<td></td>
			<td>
				<s:submit value="Save" name="isSaving"></s:submit>
				<button onclick="location.href='/ABSD/contact/contact.jsp">Go Back</button>
			</td>
		</tr>
	</table>	
	
</s:form>
<%@ include file="../Footer.jsp" %>
</div>
</s:else>
</body>
</html>