<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="../css/government-office-region.css">
<%@ include file="../Head.jsp"%>
<title>Address Lookup</title>
<script>
var postcode = -1;
function itemSelect() {
	if(postcode>0) {
		var v = confirm("Do you want to select this item?");
		if(v == true) {		
			document.cookie = "postcode="+postcode+"; path=/";
			window.close();
			target="";
		}    
	}
}
function radioSelect(i) {
	postcode = i;
}
</script>
</head>
<body>
	<%@ include file="../Menu.jsp"%>
	<form action="searchAddress">
		post code: <input name="postcode" value="" type="text">
		Street: <input name="street" value="" type="text"> Town: <input
			name="town" value="" type="text"> <input type="submit"
			value="Search">
	</form>



	<div class="container">		
		<h1>List Address</h1>
		<s:if test="%{addList == null}">

		</s:if>
		<s:else>
			<div>
				<display:table export="false" id="row" name="addList"
					pagesize="2" cellpadding="5px;" cellspacing="5px;"
					style="margin-left:50px;margin-top:20px;" requestURI="">
					<display:column class="thClass" property="id" title="ID"
						sortable="true"						
						paramId="selectedId" paramProperty="id"></display:column>
					<display:column class="thClass" property="street" title="Street"
						sortable="true"></display:column>
					<display:column class="thClass" property="town.name" title="Town"
						sortable="true"></display:column>
					<display:column class="thClass" property="county.name"
						title="County" sortable="true"></display:column>
					<display:column class="thClass" property="postcode"
						title="Postcode" sortable="true"></display:column>
					<display:column class="thClass" title="Select" sortable="true">
					<input type="radio" name="itemSelect" onclick="javascript:radioSelect(${row.postcode});"> Select
					</display:column>

				</display:table>
			</div>
			<input type="button" value="Select" onclick="javascript:itemSelect()">
		</s:else>
		<%@ include file="../Footer.jsp"%>
	</div>

</body>
</html>