<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags"%>
<%@taglib prefix="display" uri="http://displaytag.sf.net"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css"
	href="../css/government-office-region.css">
<%@ include file="../Head.jsp"%>
<title>Business Type Lookup</title>
<script>
var typeID = -1;
var typeName;
var sic;
function itemSelect() {
	if(typeID>0) {
		var v = confirm("Do you want to select this item?");
		if(v == true) {		
			document.cookie = "typeID="+typeID+"; path=/";
			document.cookie = "typeName="+typeName+"; path=/";
			document.cookie = "sic="+sic+"; path=/";
			window.close();
			target="";
		}    
	}
}
function radioSelect(id,name,siccode) {
	typeID = id;
	typeName = name;
	sic = siccode;
}
</script>
</head>
<body>
	<form action="searchTypeOfBusiness">
		Name: <input name="name" value="" type="text"> SIC Code: <input
			name="sicCode" value="" type="text"> <input type="submit"
			value="Search">
	</form>



	<div class="container">
		<%@ include file="../Menu.jsp"%>
		<h1>Business Type Lookup</h1>
		<s:if test="%{typeOfBusinessList == null}">

		</s:if>
		<s:else>
			<div>
				<display:table export="false" id="row" name="typeOfBusinessList"
					pagesize="2" cellpadding="5px;" cellspacing="5px;"
					style="margin-left:50px;margin-top:20px;" requestURI="">
					<display:column class="thClass" property="id" title="ID"
						sortable="true" paramId="selectedId" paramProperty="id"></display:column>
					<display:column class="thClass" property="name" title="Name"
						sortable="true"></display:column>
					<display:column class="thClass" property="sicCode" title="SIC Code"
						sortable="true"></display:column>
					<display:column class="thClass" title="Select" sortable="true">
					<input type="radio" name="itemSelect" onclick="javascript:radioSelect(${row.id},'${row.name}',${row.sicCode});"> Select
					</display:column>
				</display:table>
			</div>
			<input type="button" value="Select" onclick="javascript:itemSelect();">
		</s:else>
		<%@ include file="../Footer.jsp"%>
	</div>

</body>
</html>