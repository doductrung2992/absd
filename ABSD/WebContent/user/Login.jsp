<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login</title>
<link rel="stylesheet" type="text/css" href="../css/government-office-region.css">
<%@ include file="../Head.jsp" %>
<style type="text/css">
.login-form {
	margin-left: 100px;	
}
.wwgrp {
	display: table;
	padding-top:0px;
}
.wwlbl {
	display: block;
	text-align:center;	
}
.wwctrl {
	width: 100%;
	display: block;
	margin:0 auto;
	text-align: center;
	margin-top:-23px;
}
#wwctrl_submitBtn {
	padding-top:20px;
}
h2 {
	display: block;
	text-align: center;
}
label {
	text-align:center;
}
#login-form-wrapper{
	position: absolute;
	top: 50%;
	left:50%;
	transform: translate(-50%,-50%);
	-ms-transform: translate(-50%,-50%);
	-moz-transform: translate(-50%,-50%);
	-webkit-transform: translate(-50%,-50%);
	background: rgba(0,0,0,0.4);
	border-radius: 50%;
	width: 300px;
	height: 300px;
}
.forgot-password-link{
	display: block;
	text-align:center;
}
#submitBtn{
	border: none;
	background: white;
	padding: 10px 15px;
	text-transform: uppercase;
	font-weight: bold;
	display: block;
	margin: 0 auto;
	margin-bottom: 20px;
	cursor: pointer;
	transition: 1s all;
	-moz-transition: 1s all;
	-ms-transition: 1s all;
	-webkit-transition: 1s all;
}
#submitBtn:hover{
	background:#FF553F;
	color: white;
}
.wwctrl input{
	width: 70%;
	height: 27px;
}
h1{
	text-align: center;
	font-size: 50px;
	font-weight: bold;
	font-family: Arial;
	color: white;
	text-shadow: 0 0 5px black;
}
h1 span{
	color: gray;
}
</style>
</head>
<body>

<div class="container">
	<%@ include file="../Menu.jsp" %>
	<h1>Service <span>Directory</span></h1>
	<div id="login-form-wrapper">
		<h2>Log in</h2>
		<s:form id="login-form" theme="css_xhtml" action="doLogin" method="post">
			<s:textfield label="Username" name="user.account"></s:textfield>
			<s:password label="Password" name="user.password"></s:password>
			<s:submit id="submitBtn" value="Log in"></s:submit>
		</s:form>
		<a href="ForgotPassword.jsp" class="forgot-password-link">Forgot password</a>
	</div>
	<s:actionerror/>
	<%@ include file="../Footer.jsp" %>	
</div>
</body>
</html>
