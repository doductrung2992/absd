<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="s" uri="/struts-tags" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Forgot password</title>
<link rel="stylesheet" type="text/css" href="../css/government-office-region.css">
<%@ include file="../Head.jsp" %>
<style type="text/css">
.login-form {
	margin-left: 100px;	
}
.wwgrp {
	display: table;
    width:20%;
	padding-top:20px;
	padding-left: 400px;
}
.wwlbl {
	display: table-cell;	
}
.wwctrl {
	display: table-cell;	
        
}
#wwctrl_submitBtn {
	width:20%;
	padding-top:20px;
	padding-left: 400px;
}
h2 {
	padding-left: 400px;
}
input {
	float:right;
}
label {
	float:left;
}
</style>
</head>
<body>

<div class="container">
	<%@ include file="../Menu.jsp" %>
	<h2>Forgot password</h2>
	<a href="Login.jsp">Back to login</a>
	<s:form id="login-form" theme="css_xhtml" action="sendToEmail" method="post">
		<s:textfield label="Email" name="email"></s:textfield>		
		<s:submit id="submitBtn" value="Send to email"></s:submit>
	</s:form>
	<s:actionerror/>
	<%@ include file="../Footer.jsp" %>	
</div>
</body>
</html>
