<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Programme Details</title>
<link rel="stylesheet" type="text/css" href="../css/government-office-region.css">
<%@ include file="../Head.jsp" %>
<%@ include file="../checkLogin.jsp" %>
<script>
function goBack() {
    window.history.back()
}
</script>
</head>
<body>
<div class="container">
<%@ include file="../Menu.jsp" %>
<h2>Programme Detail</h2>
<button onclick="goBack()">Go Back</button>
<s:form action="inactiveProgrammeAction" method="post">
	<s:submit value="In-active"></s:submit>
	<s:submit value="Save" name="isSaving" action="saveProgrammeDetailAction"></s:submit>
	
	<s:hidden name="selectedId" value="%{selectedId}"></s:hidden>
	<s:textfield id="programmeName" label="Programme Name" value="%{selectedProgramme.name}" name="programmeName"></s:textfield>
	<s:textarea label="Description" value="%{selectedProgramme.description}" name="description"></s:textarea>
	<s:textfield label="Contact" value="%{selectedProgramme.contactName}" name="contactName"></s:textfield>
	<s:hidden name="isActive" value="%{selectedProgramme.isActive}"></s:hidden>	
</s:form>
<%@ include file="../Footer.jsp" %>
</div>
</body>
</html>