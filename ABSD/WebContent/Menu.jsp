<div id='cssmenu'>
	<ul>
		<li class='active'><a href='/ABSD/home.jsp'><span>Home</span></a></li>
		<li><a href='/ABSD/organisation/organisation.jsp'><span>Organisation</span></a>
			<ul>
				<li><a href="/ABSD/directorate/directorate.jsp">Directorate Maintenance</a></li>
				<li><a href="/ABSD/department/department.jsp">Department Maintenance</a></li>
				<li><a href="/ABSD/team/team.jsp">Team Maintenance</a></li>
			</ul></li>
		</li>
		<li><a href='#'><span>Geography</span></a>
			<ul>
				<li><a href="#">Trust Regions/Trust Districts Maintenance</a></li>
				<li><a
					href="/ABSD/government-office-region/government-office-region.jsp">Government
						Office Region Maintenance</a></li>
			</ul></li>
		<li><a href='#'><span>Service</span></a>
			<ul>
				<li><a href="/ABSD/programme/programme.jsp">Programme
						Maintenance</a></li>
				<li><a href="#">Service Maintenance</a></li>
			</ul></li>
		<li><a href='/ABSD/contact/contact.jsp'><span>Contact</span></a></li>

		<li class='last'><a href='#'><span>Premises</span></a></li>
		<%@page import="mock.appcode.common.valueobjects.UserVO"%>
		<%@page import="mock.appcode.web.action.*"%>
		<%
		    UserVO user1 = (UserVO) session
		            .getAttribute(LoginLogoutAction.USER_SESSION);
		    if (user1 != null) {%>
		    <li class='last'><a href='/ABSD/logout.jsp'><span>Logout</span></a></li>
	<%		        
		    }
		%>
	</ul>
</div>