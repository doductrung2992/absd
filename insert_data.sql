USE [IN.2014.LY.PTIT_G5_DB]
GO
INSERT INTO dbo.Country (CountryName) VALUES ('VietNam')
GO
INSERT INTO dbo.Country (CountryName) VALUES ('United State')
GO
INSERT INTO dbo.Country (CountryName) VALUES ('United Kingdom')
GO

INSERT INTO dbo.County ([CountryID], [CountyName]) VALUES (1,'County1-1')
GO
INSERT INTO dbo.County ([CountryID], [CountyName]) VALUES (1,'County1-2')
GO
INSERT INTO dbo.County ([CountryID], [CountyName]) VALUES (2,'County2-1')
GO
INSERT INTO dbo.County ([CountryID], [CountyName]) VALUES (3,'County3-1')
GO

INSERT INTO dbo.GovOfficeRegion ([CountyID]
      ,[Desctiption]
      ,[Name]
      ,[IsActive]) VALUES (1, 'Some thing about region1','Region 1', 1)
INSERT INTO dbo.GovOfficeRegion ([CountyID]
      ,[Desctiption]
      ,[Name]
      ,[IsActive]) VALUES (2, 'Some thing about region2','Region 2', 0)
INSERT INTO dbo.GovOfficeRegion ([CountyID]
      ,[Desctiption]
      ,[Name]
      ,[IsActive]) VALUES (2, 'Some thing about region3','Region 3', 1) 
INSERT INTO dbo.GovOfficeRegion ([CountyID]
      ,[Desctiption]
      ,[Name]
      ,[IsActive]) VALUES (2, 'Some thing about region3','Begion 3', 1)         
INSERT INTO dbo.GovOfficeRegion ([CountyID]
      ,[Desctiption]
      ,[Name]
      ,[IsActive]) VALUES (3, 'Some thing about region3','Cegion 3', 0)               
          
/*SELECT GovOfficeRegionID
      ,c.CountyName
      ,[Desctiption]
      ,[Name]
      ,[IsActive]
FROM dbo.GovOfficeRegion AS g
LEFT JOIN dbo.County AS c
ON g.CountyID = c.CountyID
WHERE g.Name LIKE 'R%'
GO*/

INSERT INTO [dbo].[Contact]
           ([FirstName]
           ,[ManagerID]
           ,[SurName]
           ,[KnownAs]
           ,[OfficePhone]
           ,[MobilePhone]
           ,[STHomePhone]
           ,[Email]
           ,[JobRole]
           ,[WorkBase]
           ,[JobTitle]
           ,[IsActive]
           ,[ContactTypeID]
           ,[BestContactMethodID])
     VALUES
     ('do',NULL,'trung','hahaha','132132','12314234','21311234','trung@gmail.com'
     ,'manager','work base 1', 'job title 1', 1, 1,1)
GO

INSERT INTO [dbo].[Contact]
           ([FirstName]
           ,[ManagerID]
           ,[SurName]
           ,[KnownAs]
           ,[OfficePhone]
           ,[MobilePhone]
           ,[STHomePhone]
           ,[Email]
           ,[JobRole]
           ,[WorkBase]
           ,[JobTitle]
           ,[IsActive]
           ,[ContactTypeID]
           ,[BestContactMethodID])
     VALUES
     ('adfa',1,'asd','21344','12314','23455','46757','aa@gmail.com'
     ,'manager','work base 1', 'job title 1', 0, 2,1)
GO

INSERT INTO [dbo].[Contact]
           ([FirstName]
           ,[ManagerID]
           ,[SurName]
           ,[KnownAs]
           ,[OfficePhone]
           ,[MobilePhone]
           ,[STHomePhone]
           ,[Email]
           ,[JobRole]
           ,[WorkBase]
           ,[JobTitle]
           ,[IsActive]
           ,[ContactTypeID]
           ,[BestContactMethodID])
     VALUES
     ('aaaa',2,'asdfsf','hahaha','12314','12323414234','2131231234','aaa@gmail.com'
     ,'manager','work base 1', 'job title 2', 1, 2,2)
GO

INSERT INTO [dbo].[Contact]
           ([FirstName]
           ,[ManagerID]
           ,[SurName]
           ,[KnownAs]
           ,[OfficePhone]
           ,[MobilePhone]
           ,[STHomePhone]
           ,[Email]
           ,[JobRole]
           ,[WorkBase]
           ,[JobTitle]
           ,[IsActive]
           ,[ContactTypeID]
           ,[BestContactMethodID])
     VALUES
     ('afdf',2,'bbbb','hahaha','13424','64567','21311234','trung@gmail.com'
     ,'manager','work base 1', 'job title 1', 0, 1,2)
GO

/*Programme data*/
INSERT INTO [dbo].[Programme]
           ([ContactID]
           ,[Name]
           ,[Description]
           ,[IsActive])
     VALUES
           (1, 'Programme 1', 'Description about programme1', 1)
GO

INSERT INTO [dbo].[Programme]
           ([ContactID]
           ,[Name]
           ,[Description]
           ,[IsActive])
     VALUES
           (3, 'Programme 2', 'Description about programme2', 0)
GO

INSERT INTO [dbo].[Programme]
           ([ContactID]
           ,[Name]
           ,[Description]
           ,[IsActive])
     VALUES
           (2, 'Programme 3', 'Description about programme1', 1)
GO

INSERT INTO [dbo].[Programme]
           ([ContactID]
           ,[Name]
           ,[Description]
           ,[IsActive])
     VALUES
           (1, 'Programme 4', 'Description about programme1', 0)
GO
INSERT INTO [dbo].[Programme]
           ([ContactID]
           ,[Name]
           ,[Description]
           ,[IsActive])
     VALUES
           (2, 'Programme 5', 'Description about programme5', 1)
GO

/*Insert business type*/
INSERT INTO [dbo].[BusinessType]
           ([SIC]
           ,[Name])
     VALUES
           (1231,'Business type 1')
GO

INSERT INTO [dbo].[BusinessType]
           ([SIC]
           ,[Name])
     VALUES
           (12222,'Business type 2')
GO

INSERT INTO [dbo].[BusinessType]
           ([SIC]
           ,[Name])
     VALUES
           (33535,'Business type 3')
GO

INSERT INTO [dbo].[BusinessType]
           ([SIC]
           ,[Name])
     VALUES
           (66666,'Business type 4')
GO

INSERT INTO [dbo].[BusinessType]
           ([SIC]
           ,[Name])
     VALUES
           (1123,'Business type 5')
GO

/*Insert address*/
INSERT INTO [dbo].[Country]
           ([CountryName])
     VALUES
           ('Viet Nam')
GO

INSERT INTO [dbo].[Country]
           ([CountryName])
     VALUES
           ('United State')
GO

INSERT INTO [dbo].[Country]
           ([CountryName])
     VALUES
           ('United Kingdom')
GO

INSERT INTO [dbo].[Country]
           ([CountryName])
     VALUES
           ('Holand')
GO

INSERT INTO [dbo].[Country]
           ([CountryName])
     VALUES
           ('Philipins')
GO

/*Insert to county*/
INSERT INTO [dbo].[County]
           ([CountryID]
           ,[CountyName])
     VALUES
           (1,'County1')
GO

INSERT INTO [dbo].[County]
           ([CountryID]
           ,[CountyName])
     VALUES
           (1,'County2')
GO

INSERT INTO [dbo].[County]
           ([CountryID]
           ,[CountyName])
     VALUES
           (2,'County3')
GO

INSERT INTO [dbo].[County]
           ([CountryID]
           ,[CountyName])
     VALUES
           (2,'County4')
GO

INSERT INTO [dbo].[County]
           ([CountryID]
           ,[CountyName])
     VALUES
           (3,'County5')
GO

INSERT INTO [dbo].[County]
           ([CountryID]
           ,[CountyName])
     VALUES
           (4,'County6')
GO

INSERT INTO [dbo].[County]
           ([CountryID]
           ,[CountyName])
     VALUES
           (3,'County7')
GO

/*Insert to Town*/
INSERT INTO [dbo].[Town]
           ([CountyID]
           ,[TownName])
     VALUES
           (1,'Town1')
GO

INSERT INTO [dbo].[Town]
           ([CountyID]
           ,[TownName])
     VALUES
           (1,'Town2')
GO

INSERT INTO [dbo].[Town]
           ([CountyID]
           ,[TownName])
     VALUES
           (2,'Town2')
GO

INSERT INTO [dbo].[Town]
           ([CountyID]
           ,[TownName])
     VALUES
           (2,'Town3')
GO

INSERT INTO [dbo].[Town]
           ([CountyID]
           ,[TownName])
     VALUES
           (3,'Town4')
GO

INSERT INTO [dbo].[Town]
           ([CountyID]
           ,[TownName])
     VALUES
           (3,'Town5')
GO

INSERT INTO [dbo].[Town]
           ([CountyID]
           ,[TownName])
     VALUES
           (4,'Town6')
GO

INSERT INTO [dbo].[Town]
           ([CountyID]
           ,[TownName])
     VALUES
           (4,'Town7')
GO

/*Insert to address*/
INSERT INTO [dbo].[Address]
           ([TownID]
           ,[PostCode]
           ,[Street]
           ,[CountryID]
           ,[CountyID])
     VALUES
           (1,234232,'Street1', 1, 1)
GO
INSERT INTO [dbo].[Address]
           ([TownID]
           ,[PostCode]
           ,[Street]
           ,[CountryID]
           ,[CountyID])
     VALUES
           (2,23563,'Street2', 1, 2)
GO

INSERT INTO [dbo].[Address]
           ([TownID]
           ,[PostCode]
           ,[Street]
           ,[CountryID]
           ,[CountyID])
     VALUES
           (1,66775,'Street3', 2, 2)
GO
INSERT INTO [dbo].[Address]
           ([TownID]
           ,[PostCode]
           ,[Street]
           ,[CountryID]
           ,[CountyID])
     VALUES
           (1,234555,'Street4', 2, 1)
GO
INSERT INTO [dbo].[Address]
           ([TownID]
           ,[PostCode]
           ,[Street]
           ,[CountryID]
           ,[CountyID])
     VALUES
           (2,11111,'Street5', 2, 3)
GO
/*Insert trust region*/
INSERT INTO [dbo].[TrustRegion]
           ([Name]
           ,[Description]
           ,[CountryID], IsActive)
     VALUES
           ('TrustRegion1','Description1',1,1)
GO

INSERT INTO [dbo].[TrustRegion]
           ([Name]
           ,[Description]
           ,[CountryID], IsActive)
     VALUES
           ('TrustRegion2','Description2',1,0)
GO
INSERT INTO [dbo].[TrustRegion]
           ([Name]
           ,[Description]
           ,[CountryID], IsActive)
     VALUES
           ('TrustRegion3','Description3',2,1)
GO
INSERT INTO [dbo].[TrustRegion]
           ([Name]
           ,[Description]
           ,[CountryID], IsActive)
     VALUES
           ('TrustRegion4','Description4',2,0)
GO
/*Insert to trustdistrict*/
INSERT INTO [dbo].[TrustDistrict]
           ([TrustRegionID]
           ,[Name]
           ,[Description])
     VALUES
           (1,'Trustdistrict1','Description1')
GO
INSERT INTO [dbo].[TrustDistrict]
           ([TrustRegionID]
           ,[Name]
           ,[Description])
     VALUES
           (1,'Trustdistrict2','Description2')
GO
INSERT INTO [dbo].[TrustDistrict]
           ([TrustRegionID]
           ,[Name]
           ,[Description])
     VALUES
           (2,'Trustdistrict3','Description3')
GO
INSERT INTO [dbo].[TrustDistrict]
           ([TrustRegionID]
           ,[Name]
           ,[Description])
     VALUES
           (3,'Trustdistrict4','Description4')
GO
INSERT INTO [dbo].[TrustDistrict]
           ([TrustRegionID]
           ,[Name]
           ,[Description])
     VALUES
           (2,'Trustdistrict5','Description5')
GO
/*Insert to organosation*/
INSERT INTO [dbo].[Organisation]
           ([OrgName]
           ,[ContactID]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[WebAddress]
           ,[CharityNumber]
           ,[CompanyNumber]
           ,[CountyID]
           ,[TownName]
           ,[TrustRegionID]
           ,[TrustDistrictID]
           ,[IsActive], [Email])
     VALUES
           ('Org1',1,'Short description 1','Full description 1','Address line1'
           ,'Address line2','Address line3','VietNam',1,123,'12314','asffa'
           ,'http://asd.com','123','321',1,'Town1',1,1,1,'abc@gmail.com')
GO

INSERT INTO [dbo].[Organisation]
           ([OrgName]
           ,[ContactID]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[WebAddress]
           ,[CharityNumber]
           ,[CompanyNumber]
           ,[CountyID]
           ,[TownName]
           ,[TrustRegionID]
           ,[TrustDistrictID]
           ,[IsActive],[Email])
     VALUES
           ('Org2',2,'Short description 2','Full description 2','Address line1'
           ,'Address line2','Address line3','United Kingdom',2,12314,'3456','fghc'
           ,'http://asd1.com','123','321',1,'Town1',2,1,1,'abc@gmail.com')
GO

INSERT INTO [dbo].[Organisation]
           ([OrgName]
           ,[ContactID]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[WebAddress]
           ,[CharityNumber]
           ,[CompanyNumber]
           ,[CountyID]
           ,[TownName]
           ,[TrustRegionID]
           ,[TrustDistrictID]
           ,[IsActive],[Email])
     VALUES
           ('Org3',3,'Short description 3','Full description 3','Address line1'
           ,'Address line2','Address line3','VietNam',3,123324,'66666','asfasdfa'
           ,'http://asd.com','123324','33421',1,'Town1',3,2,1,'abc@yahoo.com')
GO

/*Insert to directorate*/
INSERT INTO [dbo].[Directorate]
           ([OrgID]
           ,[ContactID]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[WebAddress]
           ,[CharityNumber]
           ,[CompanyNumber]
           ,[CountyID]
           ,[TownName]
           ,[IsActive],[Email])
     VALUES
           (1,1,'Directorate 1','Sort description 1','Full description 1','Addressline1'
           ,'Addressline2','Addressline3','VietName',1,123,'2131244','dfsfg'
           ,'http"//abc.com','2314','234234',1,'Town1',1,'anbc@gmail.com')
GO

INSERT INTO [dbo].[Directorate]
           ([OrgID]
           ,[ContactID]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[WebAddress]
           ,[CharityNumber]
           ,[CompanyNumber]
           ,[CountyID]
           ,[TownName]
           ,[IsActive],[Email])
     VALUES
           (2,3,'Directorate 2','Sort description 2','Full description 2','Addressline1'
           ,'Addressline2','Addressline3','VietName',2,23,'324325','dfasdsfg'
           ,'http"//abc1.com','2355','3333',1,'Town2',0,'abc@gmail.com')
GO

INSERT INTO [dbo].[Directorate]
           ([OrgID]
           ,[ContactID]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[WebAddress]
           ,[CharityNumber]
           ,[CompanyNumber]
           ,[CountyID]
           ,[TownName]
           ,[IsActive],[Email])
     VALUES
           (2,4,'Directorate 3','Sort description 3','Full description 3','Addressline1'
           ,'Addressline2','Addressline3','VietName',1,123,'2131124','dfsasdfg'
           ,'http"//abc1.com','2234','234234',2,'Town3',1,'abc@gmail.com')
GO
/*Insert to department*/
INSERT INTO [dbo].[Department]
           ([DirectorateID]
           ,[ContactID]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[WebAddress]
           ,[CharityNumber]
           ,[CompanyNumber]
           ,[CountyID]
           ,[TownName]
           ,[IsActive],[Email])
     VALUES
           (2,4,'Department1 3','Sort description 3','Full description 3','Addressline1'
           ,'Addressline2','Addressline3','VietName',1,123,'2131124','dfsasdfg'
           ,'http"//abc1.com','2234','234234',2,'Town3',1,'abc@gmail.com')
GO


INSERT INTO [dbo].[Department]
           ([DirectorateID]
           ,[ContactID]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[WebAddress]
           ,[CharityNumber]
           ,[CompanyNumber]
           ,[CountyID]
           ,[TownName]
           ,[IsActive],[Email])
     VALUES
           (2,3,'Department2','Sort description 2','Full description 2','Addressline1'
           ,'Addressline2','Addressline3','VietName',2,23,'324325','dfasdsfg'
           ,'http"//abc1.com','2355','3333',1,'Town2',0,'abc@gmail.com')
GO

INSERT INTO [dbo].[Department]
           ([DirectorateID]
           ,[ContactID]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[WebAddress]
           ,[CharityNumber]
           ,[CompanyNumber]
           ,[CountyID]
           ,[TownName]
           ,[IsActive],[Email])
     VALUES
           (1,1,'Department3','Sort description 3','Full description 3','Addressline1'
           ,'Addressline2','Addressline3','VietName',2,2123,'325','dfasdsfg'
           ,'http"//abc1.com','2355','3333',1,'Town3',1,'abc@gmail.com')
GO

INSERT INTO [dbo].[Department]
           ([DirectorateID]
           ,[ContactID]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[WebAddress]
           ,[CharityNumber]
           ,[CompanyNumber]
           ,[CountyID]
           ,[TownName]
           ,[IsActive],[Email])
     VALUES
           (2,4,'Department4','Sort description 4','Full description 4','Addressline1'
           ,'Addressline2','Addressline3','VietName',3,123,'325','dfasdsfg'
           ,'http"//abc1.com','2355','3333',1,'Town3',0,'abc@gmail.com')
GO

/*Insert to user*/
INSERT INTO [dbo].[User]
           ([Account]
           ,[Email]
           ,[Role]
           ,[Password])
     VALUES
           ('trunghahaha','abc@gmail.com',1,'123')
GO

INSERT INTO [dbo].[User]
           ([Account]
           ,[Email]
           ,[Role]
           ,[Password])
     VALUES
           ('trunghehehe','abc@gmail.com',1,'123')
GO
INSERT INTO [dbo].[User]
           ([Account]
           ,[Email]
           ,[Role]
           ,[Password])
     VALUES
           ('trunghihihi','abc@gmail.com',1,'123')
GO
-- Insert into dbo.Team

INSERT INTO [dbo].[Team]
           ([DepartmentID]
           ,[ContactID]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[Email]
           ,[WebAddress]
           ,[CountyID]
           ,[TownName]
           ,[IsActive])
     VALUES
           (1, 1 ,N'Team name 1' ,N'Short description 1' ,N'Full description 1' ,N'Address 1' 
		   ,N'Address 2' ,N'Address 3' ,N'Country 1' ,1 ,233 ,'09753456546' ,'3645646775'
		   ,N'abc@gmail.com' ,N'https://fsoft.com.vn' ,1 ,N'Town name 1' ,1)
GO

INSERT INTO [dbo].[Team]
           ([DepartmentID]
           ,[ContactID]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[Email]
           ,[WebAddress]
           ,[CountyID]
           ,[TownName]
           ,[IsActive])
     VALUES
           (2, 2 ,N'Team name 2' ,N'Short description' ,N'Full description' ,N'Address 1' 
		   ,N'Address 2' ,N'Address 3' ,N'Country 2' ,2 ,244 ,'09753456546' ,'3645646775'
		   ,N'abc@gmail.com' ,N'https://fsoft.com.vn' ,2 ,N'Town name' ,0)
GO

INSERT INTO [dbo].[Team]
           ([DepartmentID]
           ,[ContactID]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[Email]
           ,[WebAddress]
           ,[CountyID]
           ,[TownName]
           ,[IsActive])
     VALUES
           (1, 3 ,N'Team name 3' ,N'Short description' ,N'Full description' ,N'Address 1' 
		   ,N'Address 2' ,N'Address 3' ,N'Country 2' ,2 ,244 ,'09753456546' ,'3645646775'
		   ,N'abc@gmail.com' ,N'https://fsoft.com.vn' ,2 ,N'Town name' ,1)
GO

INSERT INTO [dbo].[Team]
           ([DepartmentID]
           ,[ContactID]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[Email]
           ,[WebAddress]
           ,[CountyID]
           ,[TownName]
           ,[IsActive])
     VALUES
           (3, 3 ,N'Team name 4' ,N'Short description' ,N'Full description' ,N'Address 1' 
		   ,N'Address 2' ,N'Address 3' ,N'Country 4' ,2 ,24 ,'09753456546' ,'3645646775'
		   ,N'abc@gmail.com' ,N'https://fsoft.com.vn' ,2 ,N'Town name' ,0)
GO

INSERT INTO [dbo].[Team]
           ([DepartmentID]
           ,[ContactID]
           ,[Name]
           ,[ShortDescription]
           ,[FullDescription]
           ,[AddressLine1]
           ,[AddressLine2]
           ,[AddressLine3]
           ,[CountryName]
           ,[BusinessTypeID]
           ,[Postcode]
           ,[PhoneNumber]
           ,[Fax]
           ,[Email]
           ,[WebAddress]
           ,[CountyID]
           ,[TownName]
           ,[IsActive])
     VALUES
           (4, 4 ,N'Team name 5' ,N'Short description' ,N'Full description' ,N'Address 1' 
		   ,N'Address 2' ,N'Address 3' ,N'Country 3' ,2 ,44 ,'09753456546' ,'3645646775'
		   ,N'abc@gmail.com' ,N'https://fsoft.com.vn' ,2 ,N'Town name' ,1)
GO